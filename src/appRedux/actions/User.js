import {
  EDIT_USER,
  DELETE_USER,
  CANCEL_EDITING,
  SUBMIT_EDITED_DATA
} from "../../constants/ActionTypes";
import { useDispatch } from "react-redux";

export const editUser = (id, data) => {
  return {
    type: EDIT_USER,
    payload: { id, data }
  };
};

export const deleteUser = id => {
  return {
    type: DELETE_USER,
    payload: { id: id }
  };
};

export const onSubmit = (id, data) => {
  return {
    type: SUBMIT_EDITED_DATA,
    payload: { id, data }
  };
  //payload:{id,userName}
};

export const onCancel = () => {
  return {
    type: CANCEL_EDITING
  };
};
