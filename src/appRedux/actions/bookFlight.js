export const BOOK_FLIGHT_FORM = data => {
  return {
    type: "BOOK_FLIGHT_FORM",
    payload: data
  };
};

export const SEARCH_FILTER = data => {
  console.log("action called", data);
  return {
    type: "SEARCH_FILTER_FORM",
    payload: data
  };
};
