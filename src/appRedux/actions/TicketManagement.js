export const TICKET_MANAGEMENT_FORM_DATA = data => {
  return {
    type: "TICKET_POST_DATA",
    payload: data
  };
};
