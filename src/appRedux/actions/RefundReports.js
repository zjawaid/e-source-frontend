export const REFUND_REPORT_FILTER_DATA = data => {
  return {
    type: "REFUND_REPORT_FILTER_DATA",
    payload: data
  };
};
