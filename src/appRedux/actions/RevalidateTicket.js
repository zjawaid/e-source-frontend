export const REVALIDATE_SUBMIT_FORM = data => {
  return {
    type: "REVALIDATE_FORM_DATA",
    payload: data
  };
};

export const REVALIDATE_POST_DATA = data => {
  return {
    type: "REVALIDATE_POST_DATA",
    payload: data
  };
};

export const SET_FlAG = () => {
  return {
    type: "SET_FlAG"
  };
};
