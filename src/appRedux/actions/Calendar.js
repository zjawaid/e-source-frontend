import {
  EDIT_CALENDAR,
  DELETE_CALENDAR,
  ADD_CALENDAR,
  ADD_PERIOD,
  GENERATE_PERIOD,
  CANCEL
} from "constants/ActionTypes";

export const addCalendar = (title, description) => {
  return { type: ADD_CALENDAR, calendar: { title, description } };
};

export const deleteCalendar = calendar_id => {
  return {
    type: DELETE_CALENDAR,
    calendar_id: calendar_id
  };
};

export const editCalendar = (id, title, description) => {
  return {
    type: EDIT_CALENDAR,
    calendar: { id: id, title: title, description: description }
  };
};

export const addPeriod = (
  title,
  spStartDate,
  spEndDate,
  numOfDays,
  stStartDate,
  stEndDate
) => {
  return {
    type: ADD_PERIOD,
    period: { title, spStartDate, spEndDate, numOfDays, stStartDate, stEndDate }
  };
};

export const generatePeriod = (timeSpan, startDate, numOfRecords) => {
  return {
    type: GENERATE_PERIOD,
    period: { timeSpan, startDate, numOfRecords }
  };
};

export const cancel = () => {
  return { type: CANCEL };
};

export const fetchNextPeriodStartDate = () => {};
