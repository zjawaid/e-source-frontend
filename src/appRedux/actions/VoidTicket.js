export const VOID_TICKET_FORM_DATA = data => {
  return {
    type: "POST_DATA",
    payload: data
  };
};

export const IS_VOIDED = data => {
  return {
    type: "IS_VOIDED",
    payload: data
  };
};
