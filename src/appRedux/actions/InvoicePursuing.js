export const INVOICE_PURSUING_DATA_FETCH_DATA = () => {
  return {
    type: "INVOICE_PURSUING_DATA_FETCH_DATA"
  };
};

export const INVOICE_PURSUING_ACTION_DELETE = data => {
  return {
    type: "INVOICE_PURSUING_ACTION_DELETE",
    payload: data
  };
};
