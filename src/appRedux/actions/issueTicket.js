export const ISSUE_TICKET_FORM_DATA = data => {
  return {
    type: "SUBMIT_DATA",
    payload: data
  };
};
