export const MY_PNR_FORM_DATA = data => {
  return {
    type: "PNR_POST_DATA",
    payload: data
  };
};

// export const testApi = () => dispatch => {
//   fetch("https://jsonplaceholder.typicode.com/todos")
//     .then(response => response.json())
//     .then(json => dispatch({ type: "TEST_API", payload: json }));
// };
