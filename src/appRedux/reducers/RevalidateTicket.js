const INIT_STATE = {
  user_id: null,
  confirmVoidTicket: false,
  revalidateTicketFormData: null,
  showUserData: false,
  successfullMessage: "success",
  revalidatePostData: null,
  errorMessage: "error",
  revalidateDetailsData: [
    {
      PNR: "PK123-1345-1",
      TKT_NO: "3231312",
      Passenger: "zohaib",
      BASE_FARE: "500",
      TAXES: "3123",
      OTHER_CHARGES: "4234",
      PSF_COMM: "12",
      TOTAL_FARE: "4000"
    }
  ]
};

const revalidateTicket = (state = INIT_STATE, action) => {
  switch (action.type) {
    case "REVALIDATE_FORM_DATA":
      return {
        ...state,
        showUserData: true,
        revalidateTicketFormData: action.payload
      };
    case "REVALIDATE_POST_DATA":
      return {
        ...state,
        showUserData: true,
        revalidatePostData: action.payload
      };
    case "SET_FlAG":
      return {
        ...state,
        showUserData: false,
        revalidatePostData: null,
        revalidateTicketFormData: null
      };
    default:
      return state;
  }
};

export default revalidateTicket;
