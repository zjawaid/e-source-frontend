const INIT_STATE = {
  user_id: null,
  selectedPnrFormData: null,
  showUserData: false,
  selectedUser: null,
  testApiData: null,
  myPnrData: [
    {
      key: "1",
      date: new Date().toISOString().slice(0, 10),
      pnr: "LM21BG",
      contract: "V8V-UATAG2",
      sector: "BWN-SIN",
      TOTAL_FARE: "2860",
      PNR_STATUS: "Created"
    }
  ]
};

const myPnr = (state = INIT_STATE, action) => {
  switch (action.type) {
    case "PNR_POST_DATA":
      return {
        ...state,
        showUserData: true,
        selectedPnrFormData: action.payload
      };
    case "TEST_API":
      return {
        ...state,
        showUserData: true,
        testApiData: action.payload
      };
    default:
      return state;
  }
};

export default myPnr;
