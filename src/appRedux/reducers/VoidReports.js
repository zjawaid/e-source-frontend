const INIT_STATE = {
  user_id: null,
  searchFilter: [],
  successfullMessage: "success",
  errorMessage: "error",
  selectedUser: null,
  ChildRefundReportFilterData: []
};

const voidReports = (state = INIT_STATE, action) => {
  switch (action.type) {
    case "SEARCH_VOID_REPORTS_POST":
      return {
        ...state,
        showSearchFlightsResponse: true,
        BookfligtFilterData: action.payload
      };
    default:
      return state;
  }
};

export default voidReports;
