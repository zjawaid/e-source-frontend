const INIT_STATE = {
  user_id: null,
  confirmVoidTicket: false,
  searchFilterData: null,
  showUserData: false,
  successfullMessage: "success",
  errorMessage: "error",
  selectedUser: null,
  refundReportFilterData: [
    {
      key: "1",
      DOI: "INVOICE",
      date_of_refund: "LM21BG",
      Agency: "5de8c379ee01813d88955b24",
      User: new Date().toISOString().slice(0, 10),
      Pax_Name: "3",
      PNR: "4234",
      Ticket_Number: "UAT AGENCY 2",
      A_L: "Supplier or Customer mapping is missing",
      Fare: "4234",
      EQU_to: "4234",
      Fare_used: "4234",
      Fare_Refundable: "4234",
      Taxes_Refundable: "4234",
      Other_Charges: "4234",
      DIS_R: "4234",
      DIS_P: "4234",
      PSF_R: "4234",
      PSF_P: "4234",
      Al_Refund_Charges: "4234",
      CONS_Refund_Charges_R: "4234",
      CONS_Refund_Charges_P: "4234",
      Refund_R: "4234",
      Refund_P: "4234",
      Status: "4234"
    }
  ]
};

const refundReports = (state = INIT_STATE, action) => {
  switch (action.type) {
    case "REFUND_REPORT_FILTER_DATA":
      return {
        ...state,
        showUserData: true,
        searchFilterData: action.payload
      };
    default:
      return state;
  }
};

export default refundReports;
