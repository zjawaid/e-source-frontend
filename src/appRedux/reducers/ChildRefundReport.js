const INIT_STATE = {
  user_id: null,
  confirmVoidTicket: false,
  searchFilterData: null,
  showUserData: false,
  successfullMessage: "success",
  errorMessage: "error",
  selectedUser: null,
  ChildRefundReportFilterData: [
    {
      key: "1",
      refund_date: new Date().toISOString().slice(0, 10),
      issue_date: new Date().toISOString().slice(0, 10),
      Description: "Supplier or Customer mapping is missing",
      agency: new Date().toISOString().slice(0, 10),
      user: "3",
      PNR: "4234",
      PAX: "UAT AGENCY 2",
      ticket_number: "52dddQssQfDaA",
      Fare: "4234",
      fare_equ: "4234",
      Fare_used: "4234",
      total_tax: "4234",
      airline_charges: "4234",
      total_refunded: "4234",
      Rate: "4234",
      Commission: "4234",
      back_to_account: "4234"
    }
  ]
};

const childRefundReports = (state = INIT_STATE, action) => {
  switch (action.type) {
    case "CHILD_REFUND_REPORT_FILTER_DATA":
      return {
        ...state,
        showUserData: true,
        searchFilterData: action.payload
      };
    default:
      return state;
  }
};

export default childRefundReports;
