import {
  DELETE_USER,
  EDIT_USER,
  SUBMIT_EDITED_DATA,
  CANCEL_EDITING
} from "constants/ActionTypes";

const INIT_STATE = {
  user_id: null,
  showEditWindow: false,
  confirmDelete: false,
  selectedUser: null,
  userData: [
    {
      key: 1,
      name: "vishal",
      agency: "Agency",
      loginEmail: "vishal.das@gmail.com",
      status: "active"
    }
  ]
};

const user = (state = INIT_STATE, action) => {
  switch (action.type) {
    case EDIT_USER:
      return {
        ...state,

        showEditWindow: true,
        selectedUser: action.payload
      };
    case DELETE_USER:
      return {
        ...state,
        confirmDelete: true
      };

    case CANCEL_EDITING:
      return {
        ...state,
        showEditWindow: false
      };

    case SUBMIT_EDITED_DATA:
      console.log("from the store", action.payload);
      return {
        ...state,
        showEditWindow: false,
        userData: update(action.payload.id, action.payload.data)
      };

    default:
      return state;
  }
};

const update = (id, data) => {
  console.log("from update", INIT_STATE.userData[0]);
  INIT_STATE.userData[id - 1].name = data.name;
  return [...INIT_STATE.userData];
};

export default user;
