const INIT_STATE = {
  user_id: null,
  selectedUser: null,
  InvoicePursuingData: null,
  InvoicePursuingResponse: [
    {
      key: "1",
      ref_type: "INVOICE",
      pnr: "LM21BG",
      ref_no: "5de8c379ee01813d88955b24",
      CREATION_DATE: new Date().toISOString().slice(0, 10),
      TOTAL_BOOKINGS: "2",
      BOOKINGS_TOTAL: "4423",
      AGENCY: "UAT AGENCY 2",
      DESCRIPTION: "Supplier or Customer mapping is missing"
    },
    {
      key: "2",
      ref_type: "INVOICE",
      pnr: "LM21BG",
      ref_no: "5de8c379ee01813d88955b24",
      CREATION_DATE: new Date().toISOString().slice(0, 10),
      TOTAL_BOOKINGS: "3",
      BOOKINGS_TOTAL: "4234",
      AGENCY: "UAT AGENCY 2",
      DESCRIPTION: "Supplier or Customer mapping is missing"
    }
  ]
};

const invoicePursuing = (state = INIT_STATE, action) => {
  console.log(action);
  switch (action.type) {
    case "INVOICE_PURSUING_DATA_FETCH_DATA":
      return {
        ...state
      };
    case "INVOICE_PURSUING_ACTION_DELETE":
      return {
        InvoicePursuingResponse: deleteActionData(
          action.payload.id,
          action.payload
        ),
        ...state
      };
    default:
      return state;
  }
};

const deleteActionData = (id, data) => {
  console.log("functionh response", data);
  // const updatedRecord = data[data.key];
  // INIT_STATE.InvoicePursuingResponse.splice(updatedRecord["key"]);
  // return { ...INIT_STATE, updatedRecord };
};

export default invoicePursuing;
