const INIT_STATE = {
  user_id: null,
  confirmVoidTicket: false,
  BookfligtFilterData: null,
  searchFilterForm: null,
  showSearchFlightsResponse: false,
  searchFilter: [],
  successfullMessage: "success",
  errorMessage: "error",
  selectedUser: null,
  ChildRefundReportFilterData: []
};

const bookFlights = (state = INIT_STATE, action) => {
  switch (action.type) {
    case "BOOK_FLIGHT_FORM":
      return {
        ...state,
        showSearchFlightsResponse: true,
        BookfligtFilterData: action.payload
      };
    case "SEARCH_FILTER_FORM":
      return {
        ...state,
        searchFilterForm: action.payload
      };
    default:
      return state;
  }
};

export default bookFlights;
