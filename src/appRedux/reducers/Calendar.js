import {
  EDIT_CALENDAR,
  DELETE_CALENDAR,
  ADD_CALENDAR,
  ADD_PERIOD,
  GENERATE_PERIOD,
  CANCEL
} from "constants/ActionTypes";

const INIT_STATE = {
  addCalendar: false,
  showEditWindow: false,
  calendars: [],
  timePeriods: []
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case ADD_CALENDAR: {
      return {
        ...state,
        addCalendar: true,
        calendars: addCalendar(
          action.calendar.title,
          action.calendar.description
        )
      };
    }
    case DELETE_CALENDAR: {
      return state;
    }
    case EDIT_CALENDAR: {
      return {
        ...state,
        showEditWindow: false,
        calendars: update(
          action.calendar.id,
          action.calendar.title,
          action.calendar.description
        )
      };
    }

    case ADD_PERIOD: {
      return {
        ...state,
        timePeriods: addPeriod(
          action.period.title,
          action.period.spStartDate,
          action.period.spEndDate,
          action.period.numOfDays,
          action.period.stStartDate,
          action.period.stEndDate
        )
      };
    }
    case CANCEL: {
      return {
        ...state,
        addCalendar: true
      };
    }
    default: {
      return state;
    }
  }

  const update = (id, title, description) => {
    console.log("from update", INIT_STATE.userData[0]);
    INIT_STATE.calendars[id - 1].title = title;
    INIT_STATE.calendars[id - 1].description = description;
    return [...INIT_STATE.calendars];
  };
};
const addCalendar = (title, description) => {
  INIT_STATE.calendars.push({ title, description });
  return [...INIT_STATE.calendars];
};

const addPeriod = (
  title,
  spStartDate,
  spEndDate,
  numOfDays,
  stStartDate,
  stEndDate
) => {
  INIT_STATE.timePeriods.push({
    title,
    spStartDate,
    spEndDate,
    numOfDays,
    stStartDate,
    stEndDate
  });
  return [...INIT_STATE.timePeriods];
};
