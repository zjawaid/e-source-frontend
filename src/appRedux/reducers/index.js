import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import Settings from "./Settings";
import Auth from "./Auth";
import Notes from "./Notes";
import Contact from "./Contact";
import Common from "./Common";
import User from "./User";
import Calendar from "./Calendar";
import issueTicket from "./issueTicket";
import voidTicket from "./VoidTicket";
import ticketManagement from "./TicketManagement";
import myPnr from "./MyPnrs";
import revalidateTicket from "./RevalidateTicket";
import refundReports from "./RefundReports";
import childRefundReports from "./ChildRefundReport";
import bookFlights from "./bookFlight";
import invoicePursuing from "./InvoicePursuing";

const reducers = combineReducers({
  routing: routerReducer,
  settings: Settings,
  auth: Auth,
  notes: Notes,
  contact: Contact,
  common: Common,
  user: User,
  calendar: Calendar,
  issueticket: issueTicket,
  voidTicket: voidTicket,
  ticketManagement: ticketManagement,
  myPnr: myPnr,
  revalidateTicket: revalidateTicket,
  refundReports: refundReports,
  childRefundReports: childRefundReports,
  bookFlight: bookFlights,
  invoicePursuing: invoicePursuing
});

export default reducers;
