const INIT_STATE = {
  user_id: null,
  showEditWindow: false,
  confirmDelete: false,
  selectedUser: null,
  ticketFormData: null,
  showTableData: false,
  sectorDetailsInformation: {
    selectedUser: null,
    errorMessage: false,
    userData: [
      {
        key: 1,
        select: "true",
        contract_code: "CK124",
        limit: 50,
        available_limit: "500",
        fare_review: "fine",
        currency: "PKR 6500",
        details: "this is a dummy information"
      }
    ]
  },
  passengerDetailInformation: {
    selectedUser: null,
    errorMessage: false,
    userData: [
      {
        key: "1",
        PASSENGER: "Zohaib Jawaid",
        SEGMENT: 32,
        CURRENCY: "PKR 4500",
        FARE_TYPE: "10",
        BASE_FARE: "dummy text",
        TAXES: "RS",
        OTHER_CHARGES: "Dummy detail text",
        TOTAL: "1000",
        PL: "dummy",
        EXC_RATE: "150",
        EQUIV_TOTAL: "2000"
      }
    ]
  }
};

const issueTicket = (state = INIT_STATE, action) => {
  switch (action.type) {
    case "SUBMIT_DATA":
      return {
        ...state,
        showTableData: true,
        ticketFormData: action.payload
      };
    default:
      return state;
  }
};

export default issueTicket;
