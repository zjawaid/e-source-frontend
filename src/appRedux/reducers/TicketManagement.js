const INIT_STATE = {
  user_id: null,
  confirmVoidTicket: false,
  ticketsFormData: null,
  showUserData: false,
  successfullMessage: "success",
  errorMessage: "error",
  selectedUser: null,
  ticketDetailsData: [
    {
      key: "1",
      ticket_number: "PK123-1345-1",
      date_of_issue: Date.now(),
      PNR: "1312",
      ROUTE: "KHI - ISB",
      BASE_FARE: "500",
      TAXES: "3123",
      OTHER_CHARGES: "4234",
      PSF_COMM: "12",
      TOTAL_AMOUNT: "4000",
      STATUS: "Void",
      CREATED_BY: "UATCON"
    }
  ]
};

const ticketManagement = (state = INIT_STATE, action) => {
  switch (action.type) {
    case "TICKET_POST_DATA":
      return {
        ...state,
        showUserData: true,
        ticketsFormData: action.payload
      };
    default:
      return state;
  }
};

export default ticketManagement;
