const INIT_STATE = {
  user_id: null,
  confirmVoidTicket: false,
  pnrTicketData: null,
  showTableData: false,
  successfullMessage: false,
  unsucessfullMessage: false,
  selectedUser: null,
  isVoided: null,
  errorMessage: false,
  ticketDetailsData: [
    {
      key: "1",
      PASSENGER: "Zohaib Jawaid",
      TICKET_NUMBER: "PK0312451-12",
      FARE: "40,000",
      VOID_CHARGES: "2000",
      STATUS: "Active",
      IS_VOIDED: "false"
    },
    {
      key: "2",
      PASSENGER: "Shoaib Jawaid",
      TICKET_NUMBER: "PK0312451-12",
      FARE: "40,000",
      VOID_CHARGES: "2000",
      STATUS: "Active",
      IS_VOIDED: "true"
    }
  ]
};

const voidTicket = (state = INIT_STATE, action) => {
  switch (action.type) {
    case "POST_DATA":
      return {
        ...state,
        showTableData: true,
        pnrTicketData: action.payload
      };
    case "IS_VOIDED":
      return {
        ...state,
        showTableData: true,
        ticketDetailsData: [...updateVoid(action.payload)]
      };
    default:
      return state;
  }
};

const updateVoid = data => {
  console.log("update fun", data);
  console.log("INIT_STATE", INIT_STATE);
  INIT_STATE["ticketDetailsData"] = data[0]["IS_VOIDED"] = "true";

  return data;
};

export default voidTicket;
