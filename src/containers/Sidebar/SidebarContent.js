import React, { Component } from "react";
import { Menu } from "antd";
import { Link } from "react-router-dom";

import CustomScrollbars from "util/CustomScrollbars";
import SidebarLogo from "./SidebarLogo";

import Auxiliary from "util/Auxiliary";
import UserProfile from "./UserProfile";
import AppsNavigation from "./AppsNavigation";
import {
  NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR,
  NAV_STYLE_NO_HEADER_MINI_SIDEBAR,
  THEME_TYPE_LITE
} from "../../constants/ThemeSetting";
import IntlMessages from "../../util/IntlMessages";
import { connect } from "react-redux";

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

class SidebarContent extends Component {
  getNoHeaderClass = navStyle => {
    if (
      navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR ||
      navStyle === NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR
    ) {
      return "gx-no-header-notifications";
    }
    return "";
  };
  getNavStyleSubMenuClass = navStyle => {
    if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR) {
      return "gx-no-header-submenu-popup";
    }
    return "";
  };

  render() {
    const { themeType, navStyle, pathname } = this.props;
    const selectedKeys = pathname.substr(1);
    const defaultOpenKeys = selectedKeys.split("/")[1];
    return (
      <Auxiliary>
        <SidebarLogo />
        <div className="gx-sidebar-content">
          <div
            className={`gx-sidebar-notifications ${this.getNoHeaderClass(
              navStyle
            )}`}
          >
            <UserProfile />
            {/* <AppsNavigation/> */}
          </div>
          <CustomScrollbars className="gx-layout-sider-scrollbar">
            <Menu
              defaultOpenKeys={[defaultOpenKeys]}
              selectedKeys={[selectedKeys]}
              theme={themeType === THEME_TYPE_LITE ? "lite" : "dark"}
              mode="inline"
            >
              <MenuItemGroup
                key="main"
                className="gx-menu-group"
                title={<IntlMessages id="sidebar.main" />}
              >
                <Menu.Item key="dashboard">
                  <Link to="/dashboard">
                    <i className="icon icon-dasbhoard" />
                    <IntlMessages id="sidebar.dashboard" />
                  </Link>
                </Menu.Item>
                {/* <SubMenu
                  key="dashboard"
                  className={this.getNavStyleSubMenuClass(navStyle)}
                  title={
                    <span>
                      {" "}
                      <i className="icon icon-dasbhoard" />
                      <IntlMessages id="sidebar.dashboard" />
                    </span>
                  }
                >
                  <Menu.Item key="main/dashboard/crypto">
                    <Link to="/main/dashboard/crypto">
                      <i className="icon icon-crypto" />
                      <IntlMessages id="sidebar.dashboard.crypto" />
                    </Link>
                  </Menu.Item>
                  <Menu.Item key="main/dashboard/crm">
                    <Link to="/main/dashboard/crm">
                      <i className="icon icon-crm" />
                      <IntlMessages id="sidebar.dashboard.crm" />
                    </Link>
                  </Menu.Item>
                  <Menu.Item key="main/dashboard/listing">
                    <Link to="/main/dashboard/listing">
                      <i className="icon icon-listing-dbrd" />
                      <IntlMessages id="sidebar.dashboard.listing" />
                    </Link>
                  </Menu.Item>
                </SubMenu> */}
                <SubMenu
                  key="Manage"
                  title={
                    <span>
                      {" "}
                      <i className="icon icon-dasbhoard" />
                      <IntlMessages id="sidebar.manage" />
                    </span>
                  }
                >
                  <Menu.Item key="Manage/calendar">
                    <Link to="/Manage/calendars">
                      <i className="icon icon-widgets" />
                      <IntlMessages id="sidebar.manage.calendar" />
                    </Link>
                  </Menu.Item>

                  <Menu.Item key="Manage/user">
                    <Link to="/manage/user">
                      <i className="icon icon-widgets" />
                      <IntlMessages id="sidebar.manage.user" />
                    </Link>
                  </Menu.Item>

                  <Menu.Item key="Manage/contracts">
                    <Link to="/manage/contract">
                      <i className="icon icon-widgets" />
                      <IntlMessages id="siderbar.manage.contract" />
                    </Link>
                  </Menu.Item>

                  <Menu.Item key="Manage/childContracts">
                    <Link to="/main/widgets">
                      <i className="icon icon-widgets" />
                      <IntlMessages id="siderbar.manage.childcontract" />
                    </Link>
                  </Menu.Item>

                  <Menu.Item key="Manage/commandControl">
                    <Link to="/Manage/commandControl">
                      <i className="icon icon-widgets" />
                      <IntlMessages id="siderbar.manage.commandcontrol" />
                    </Link>
                  </Menu.Item>

                  <Menu.Item key="Manage/generalPolicy">
                    <Link to="/manage/generalPolicy">
                      <i className="icon icon-widgets" />
                      <IntlMessages id="siderbar.manage.generalpolicy" />
                    </Link>
                  </Menu.Item>

                  <Menu.Item key="Manage/privatePolicy">
                    <Link to="/Manage/privatePolicy">
                      <i className="icon icon-widgets" />
                      <IntlMessages id="siderbar.manage.privatepolicy" />
                    </Link>
                  </Menu.Item>

                  <Menu.Item key="Manage/myContracts">
                    <Link to="/Manage/myContracts">
                      <i className="icon icon-widgets" />
                      <IntlMessages id="siderbar.manage.mycontract" />
                    </Link>
                  </Menu.Item>

                  <Menu.Item key="Manage/sector">
                    <Link to="/Manage/sectors">
                      <i className="icon icon-widgets" />
                      <IntlMessages id="siderbar.manage.sector" />
                    </Link>
                  </Menu.Item>
                </SubMenu>
                <SubMenu
                  key="transactions"
                  title={
                    <span>
                      {" "}
                      <i className="icon icon-dasbhoard" />
                      <IntlMessages id="sidebar.transactions" />
                    </span>
                  }
                >
                  <Menu.Item key="transactions/bookFlight">
                    <Link to="/transactions/bookFlights">
                      <i className="icon icon-widgets" />
                      <IntlMessages id="sidebar.transactions.bookflight" />
                    </Link>
                  </Menu.Item>

                  <Menu.Item key="transactions/issueTicket">
                    <Link to="/transactions/issueTicket">
                      <i className="icon icon-widgets" />
                      <IntlMessages id="sidebar.transactions.issueticket" />
                    </Link>
                  </Menu.Item>

                  <Menu.Item key="transactions/manualReservation">
                    <Link to="/transactions/manualReservation">
                      <i className="icon icon-widgets" />
                      <IntlMessages id="sidebar.transactions.manualreservation" />
                    </Link>
                  </Menu.Item>

                  <Menu.Item key="transactions/voidTicket">
                    <Link to="/transactions/voidTicket">
                      <i className="icon icon-widgets" />
                      <IntlMessages id="sidebar.transactions.voidticket" />
                    </Link>
                  </Menu.Item>

                  <Menu.Item key="transactions/ticketManagement">
                    <Link to="/transactions/ticketManagement">
                      <i className="icon icon-widgets" />
                      <IntlMessages id="sidebar.transactions.ticketmanagement" />
                    </Link>
                  </Menu.Item>
                  <Menu.Item key="transactions/myPnrs">
                    <Link to="/transactions/myPnrs">
                      <i className="icon icon-widgets" />
                      <IntlMessages id="sidebar.transactions.mypnrs" />
                    </Link>
                  </Menu.Item>
                  <Menu.Item key="transactions/exchangeTicket">
                    <Link to="/transactions/exchangeTicket">
                      <i className="icon icon-widgets" />
                      <IntlMessages id="sidebar.transactions.exchangeticket" />
                    </Link>
                  </Menu.Item>
                  <Menu.Item key="transactions/revalidateTicket">
                    <Link to="/transactions/revalidateTicket">
                      <i className="icon icon-widgets" />
                      <IntlMessages id="sidebar.transactions.revalidateticket" />
                    </Link>
                  </Menu.Item>
                  <Menu.Item key="transactions/lccBookFlight">
                    <Link to="/transactions/lccBookFlight">
                      <i className="icon icon-widgets" />
                      <IntlMessages id="sidebar.transactions.lccbookflight" />
                    </Link>
                  </Menu.Item>
                  <Menu.Item key="transactions/invoicePursuing">
                    <Link to="/transactions/invoicePursuing">
                      <i className="icon icon-widgets" />
                      <IntlMessages id="sidebar.transactions.invoicepursuing" />
                    </Link>
                  </Menu.Item>
                </SubMenu>
                <SubMenu
                  className={this.getNavStyleSubMenuClass(navStyle)}
                  key="Reports"
                  title={
                    <span>
                      {" "}
                      <i className="icon icon-dasbhoard" />
                      <IntlMessages id="sidebar.reports" />
                    </span>
                  }
                >
                  <Menu.Item key="reports/refundReports">
                    <Link to="/reports/refundReports">
                      <i className="icon icon-widgets" />
                      <IntlMessages id="sidebar.reports.refundreports" />
                    </Link>
                  </Menu.Item>
                  <Menu.Item key="reports/childRefundReports">
                    <Link to="/reports/childRefundReports">
                      <i className="icon icon-widgets" />
                      <IntlMessages id="sidebar.reports.childrefundreports" />
                    </Link>
                  </Menu.Item>
                  <Menu.Item key="reports/saleReports">
                    <Link to="/reports/saleReports">
                      <i className="icon icon-widgets" />
                      <IntlMessages id="sidebar.reports.salereports" />
                    </Link>
                  </Menu.Item>
                  <Menu.Item key="reports/childSaleReports">
                    <Link to="/reports/childSaleReports">
                      <i className="icon icon-widgets" />
                      <IntlMessages id="sidebar.reports.childsalereports" />
                    </Link>
                  </Menu.Item>
                  <Menu.Item key="reports/ledgerReports">
                    <Link to="/reports/ledgerReports">
                      <i className="icon icon-widgets" />
                      <IntlMessages id="sidebar.reports.ledgerreport" />
                    </Link>
                  </Menu.Item>
                  <Menu.Item key="reports/voidReports">
                    <Link to="/reports/voidReports">
                      <i className="icon icon-widgets" />
                      <IntlMessages id="sidebar.reports.voidreport" />
                    </Link>
                  </Menu.Item>
                  <Menu.Item key="reports/missedTicketReport">
                    <Link to="/reports/missedTicketReport">
                      <i className="icon icon-widgets" />
                      <IntlMessages id="sidebar.reports.missedticketreport" />
                    </Link>
                  </Menu.Item>
                </SubMenu>
              </MenuItemGroup>
            </Menu>
          </CustomScrollbars>
        </div>
      </Auxiliary>
    );
  }
}

SidebarContent.propTypes = {};
const mapStateToProps = ({ settings }) => {
  const { navStyle, themeType, locale, pathname } = settings;
  return { navStyle, themeType, locale, pathname };
};
export default connect(mapStateToProps)(SidebarContent);
