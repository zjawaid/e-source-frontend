import React, { Component } from "react";
import { Card, Col, Form, Icon, Input, Row, Checkbox, Select } from "antd";

const FormItem = Form.Item;

class Details extends Component {
  state = {
    expand: false
  };

  handleSearch = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      console.log("Received values of form: ", values, err);
    });
  };

  handleReset = () => {
    this.props.form.resetFields();
  };

  toggle = () => {
    const { expand } = this.state;
    this.setState({ expand: !expand });
  };

  // To generate mock Form.Item
  getFields(j, k) {
    const count = this.state.expand ? 10 : 8;
    const { getFieldDecorator } = this.props.form;
    const children = [];
    const fieldNames = [
      "Contract With",
      "Agency name",
      "Agency Adress",
      "Agency Phone",
      "Assigned Limit"
    ];
    for (let i = j; i < k; i++) {
      const divider = null;

      children.push(
        <Col
          lg={6}
          md={6}
          sm={12}
          xs={24}
          key={i}
          style={{ display: i < count ? "block" : "none" }}
        >
          <div className="gx-form-row0">
            <FormItem>
              <Input
                placeholder={fieldNames[`${i}`]}
                style={{ width: "300px" }}
              />
            </FormItem>
          </div>
        </Col>
      );
    }
    return children;
  }

  render() {
    return (
      <Card className="gx-card" title="Details">
        <Form className="ant-advanced-search-form" onSubmit={this.handleSearch}>
          <Row>
            {this.getFields(0, 5)}
            <Col lg={6} md={6} sm={12} xs={24}>
              <div className="gx-form-row0">
                <FormItem>
                  <Select
                    placeholder="currency"
                    style={{ width: "300px", margin: "1%" }}
                  >
                    <Select.Option value="USD">USD</Select.Option>
                    <Select.Option value="INR">INR</Select.Option>
                    <Select.Option value="PKR">PKR</Select.Option>
                  </Select>
                </FormItem>
              </div>
            </Col>

            <Col lg={6} md={6} sm={12} xs={24}>
              <div className="gx-form-row0">
                <FormItem>
                  <Checkbox>Allow Distribution</Checkbox>
                </FormItem>
              </div>
            </Col>

            <Col lg={6} md={6} sm={12} xs={24}>
              <div className="gx-form-row0">
                <FormItem>
                  <Checkbox>Allow Conversion</Checkbox>
                </FormItem>
              </div>
            </Col>
          </Row>
        </Form>
      </Card>
    );
  }
}

const ContractDetails = Form.create()(Details);

export default ContractDetails;
