import React, { useState } from "react";
import { Avatar, Icon, Checkbox, Divider, Collapse, Modal } from "antd";

import Aux from "util/Auxiliary";
import { taskStatus } from "../../../../src/routes/main/dashboard/CRM/data";
import Configuration from "./ConfigureAirline";

const getStatus = status => {
  const statusData = taskStatus.filter(
    (taskStatus, index) => status === taskStatus.id
  )[0];
  return (
    <Aux>
      <span className="gx-nonhover">
        <i
          className={`icon icon-circle gx-fs-sm gx-text-${statusData.color}`}
        />
      </span>
      <span
        className={`gx-badge gx-hover gx-mb-0 gx-text-white gx-badge-${statusData.color}`}
      >
        {statusData.title}
      </span>
    </Aux>
  );
};

const SelectedAirline = props => {
  const showModal = () => {
    setState(!state);
  };
  const [state, setState] = useState(false);
  return (
    <div>
      <Modal
        width={750}
        title="Basic Modal"
        visible={state}
        onOk={() => setState(!state)}
        onCancel={() => setState(!state)}
      >
        <Configuration />
      </Modal>{" "}
      <div
        key={"TicketItem" + props.id}
        className="gx-media gx-task-list-item gx-flex-nowrap"
      >
        <div className="gx-media-body gx-task-item-content">
          <div className="gx-task-item-content-left">
            <h3 className="gx-text-truncate gx-task-item-title ">
              {props.airlineName}
            </h3>
            {/* <Collapse
              bordered={false}
              defaultActiveKey={["1"]}
              expandIcon={({ isActive }) => (
                <Icon type="caret-right" rotate={isActive ? 90 : 0} />
              )}
            >
              <Collapse.Panel
                header={
                  <h3 className="gx-text-truncate gx-task-item-title ">
                    {props.airlineName}
                  </h3>
                }
              >
                <p key={props.id} className="gx-text-grey gx-fs-sm gx-mb-0">
                  <Checkbox defaultChecked>Terminal Access</Checkbox>
                  <Checkbox>Allow Terminal Distribution</Checkbox>
                </p>
              </Collapse.Panel>
            </Collapse> */}

            <p key={props.id} className="gx-text-grey gx-fs-sm gx-mb-0">
              <Checkbox defaultChecked>Terminal Access</Checkbox>
              <Checkbox>Allow Terminal Distribution</Checkbox>
            </p>
          </div>
          <div className="gx-task-item-content-right">
            <Icon type="setting" onClick={showModal} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default SelectedAirline;
