import React, { useState } from "react";
import { Card, Select, Divider } from "antd";
import { render } from "react-dom";
import SelectedAirline from "./SelectedAirline";
import Widget from "components/Widget/index";

const Option = Select.Option;

const count = 3;
// const fakeDataUrl = `https://randomuser.me/api/?results=${count}&inc=name,gender,email,nat&noinfo`;

const airlines = ["Qatar Airways", "5Q1Q"];
const children = [];
const temp = [];

for (let i = 0; i < 2; i++) {
  children.push(<Option key={airlines[i]}>{airlines[i]}</Option>);
}

const ContractConfig = () => {
  const [state, setState] = React.useState({ selected: [], show: false });

  function handleChange(value) {
    console.log("pushed state:", value[1]);

    setState({ selected: value });
  }

  return (
    console.log("rendered"),
    (
      <Widget title="CONFIGURE">
        <Select
          mode="multiple"
          placeholder="Please select Airlines to Configure"
          style={{ width: "50%" }}
          onChange={handleChange}
        >
          {children}
        </Select>

        {state.selected.map(selectedValue => {
          return (
            <p style={{ margin: "20px" }}>
              {" "}
              <SelectedAirline
                id={selectedValue}
                airlineName={selectedValue}
              />{" "}
              <Divider />
            </p>
          );
        })}

        {/* <Checkbox className="gx-mb-3" value="A">A</Checkbox>
          <Checkbox className="gx-mb-3" value="B">B</Checkbox>
          
          <Button className="gx-text-right">Configure</Button>
           */}
      </Widget>
    )
  );
};

export default ContractConfig;
