import React from "react";
import { Card, Checkbox, Button, Icon, InputNumber } from "antd";

const CheckboxGroup = Checkbox.Group;

function onChange(checkedValues) {
  console.log("checked = ", checkedValues);
}

const plainOptions = ["Fare Review", "Refund Review", "Allow Revalidation"];

const options = [
  { label: "Fare Review", value: "Fare" },
  { label: "Refund Review", value: "Refund" },
  { label: "Allow Revalidation", value: "Allow" }
];

const optionsWithDisabled = [
  { label: "Apple", value: "Apple" },
  { label: "Pear", value: "Pear" },
  { label: "Orange", value: "Orange", disabled: false }
];

const TicketingFunctionalities = () => {
  return (
    <Card
      className="gx-card"
      title="Ticketing Functionalities"
      style={{ width: "100%" }}
    >
      <CheckboxGroup
        options={plainOptions}
        defaultValue={["Apple"]}
        onChange={onChange}
      />

      <br />
      <br />
      <label>Void</label>
      <InputNumber
        min={0}
        max={100000}
        defaultValue={0}
        style={{ margin: "1%", width: "100px" }}
      />

      <label>Refund</label>
      <InputNumber
        min={0}
        max={100000}
        defaultValue={0}
        style={{ margin: "1%", width: "100px" }}
      />
      <label>Exchange</label>
      <InputNumber
        min={0}
        max={100000}
        defaultValue={0}
        style={{ margin: "1%", width: "100px" }}
      />
    </Card>
  );
};
export default TicketingFunctionalities;
