import React from "react";
import { Transfer, Checkbox } from "antd";

class Configure extends React.Component {
  state = {
    mockData: [],
    targetKeys: [],
    vissible: false
  };

  componentDidMount() {
    this.getMock();
  }

  onChange = e => {
    this.setState({ vissible: !this.state.vissible });
  };

  getMock = () => {
    const targetKeys = [];
    const mockData = [];
    for (let i = 0; i < 20; i++) {
      const data = {
        key: i.toString(),
        title: `content${i + 1}`,
        description: `description of content${i + 1}`,
        chosen: Math.random() * 2 > 1
      };
      if (data.chosen) {
        targetKeys.push(data.key);
      }
      mockData.push(data);
    }
    this.setState({ mockData, targetKeys });
  };

  filterOption = (inputValue, option) =>
    option.description.indexOf(inputValue) > -1;

  handleChange = targetKeys => {
    this.setState({ targetKeys });
  };

  handleSearch = (dir, value) => {
    console.log("search:", dir, value);
  };

  render() {
    return (
      <div>
        <Checkbox onChange={this.onChange}>6S</Checkbox>
        {this.state.vissible ? (
          <div style={{ float: "center" }}>
            <Transfer
              listStyle={{
                width: 250,
                height: 300
              }}
              dataSource={this.state.mockData}
              titles={["All Sectors", "Locked Sectors"]}
              filterOption={this.filterOption}
              showSearch
              targetKeys={this.state.targetKeys}
              onChange={this.handleChange}
              onSearch={this.handleSearch}
              render={item => item.title}
            />
          </div>
        ) : null}
      </div>
    );
  }
}

export default Configure;
