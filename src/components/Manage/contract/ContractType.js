import React, { Component } from "react";
import {
  Button,
  Card,
  Checkbox,
  Radio,
  Select,
  DatePicker,
  Divider,
  InputNumber,
  Tag
} from "antd";

class ContractType extends Component {
  state = {
    checked: false,
    disabled: false,
    show: false
  };
  toggleChecked = () => {
    this.setState({ checked: !this.state.checked });
  };
  toggleDisable = () => {
    this.setState({ disabled: !this.state.disabled });
  };
  onChange = e => {
    console.log("checked = ", e.target.checked);
    this.setState({
      show: !this.state.show,
      checked: false
    });
  };

  render() {
    const { RangePicker } = DatePicker;
    const selectPane = (
      <p>
        <Select defaultValue="locked" style={{ width: "100px", margin: "1%" }}>
          <Select.Option key="locked" Value="locked">
            Locked
          </Select.Option>
          <Select.Option key="unlocked" Value="locked">
            unlocked
          </Select.Option>
        </Select>
        <RangePicker
          placeholder={["Contract from", "Contract To"]}
          style={{ margin: "1%" }}
        />
        <Checkbox onChange={this.toggleChecked}>
          Distribute Content Wise Limit
        </Checkbox>
      </p>
    );

    const label = `${this.state.checked ? "Checked" : "Unchecked"}-${
      this.state.disabled ? "Disabled" : "Enabled"
    }`;
    return (
      <Card className="gx-card gx-card" title="Contract Type">
        <p st yle={{ marginBottom: "20px" }}>
          <Radio.Group onChange={this.onChange}>
            <Radio defaultChecked>Debit</Radio>
            <Radio value={2}>Credit</Radio>
          </Radio.Group>
        </p>
        {this.state.show ? selectPane : null}
        {this.state.checked ? (
          <p>
            <Divider orientation="left">Select Content</Divider>
            <label>Airline</label>
            <InputNumber
              min={0}
              max={100000}
              defaultValue={0}
              style={{ margin: "1%", width: "100px" }}
            />

            <label>Hotel</label>
            <InputNumber
              min={0}
              max={100000}
              defaultValue={0}
              style={{ margin: "1%", width: "100px" }}
            />
            <label>Car</label>
            <InputNumber
              min={0}
              max={100000}
              defaultValue={0}
              style={{ margin: "1%", width: "100px" }}
            />
            <label>Tims</label>
            <InputNumber
              min={0}
              max={100000}
              defaultValue={0}
              style={{ margin: "1%", width: "100px" }}
            />
          </p>
        ) : null}
      </Card>
    );
  }
}

export default ContractType;
