import React from "react";
import {
  Form,
  Input,
  Icon,
  Button,
  Card,
  Row,
  Col,
  Modal,
  Divider,
  Select
} from "antd";
import Default from "../../../routes/extraComponents/timeLine/Default";
import { routerReducer } from "react-router-redux";
import PolicyRecordTabe from "./SummaryTable";

let id = 0;

class AddPolicy extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ModalText: "Content of the modal",
      visible: false,
      confirmLoading: false,
      openSettingDrawer: false
    };
  }

  remove = k => {
    const { form } = this.props;
    // can use data-binding to get
    const keys = form.getFieldValue("keys");
    // We need at least one passenger
    if (keys.length === 1) {
      return;
    }

    // can use data-binding to set
    form.setFieldsValue({
      keys: keys.filter(key => key !== k)
    });
  };

  add = () => {
    // const { form } = this.props;
    // // can use data-binding to get
    // const keys = form.getFieldValue("keys");
    // const nextKeys = keys.concat(id++);
    // // can use data-binding to set
    // // important! notify form to detect changes
    // form.setFieldsValue({
    //   keys: nextKeys
    // });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { keys, names } = values;
        console.log("Received values of form: ", values);
        console.log(
          "Merged values:",
          keys.map(key => names[key])
        );
      }
    });
  };

  onContentValueChange = e => {
    e.preventDefault();
    if (e.target.value) {
      this.setState({ openSettingDrawer: true });
    }
  };

  render() {
    const { confirmLoading, visible } = this.state;
    const columns = [
      {
        title: "Service Type",
        dataIndex: "service",
        key: "service",
        render: text => <a>{text}</a>
      },
      {
        title: "Charge Summary",
        dataIndex: "charge",
        key: "charge"
      },
      {
        title: "TMU Summary",
        dataIndex: "tmu",
        key: "tmu"
      },

      {
        title: "Action",
        key: "action",
        render: (text, record) => (
          <span>
            <Icon type="delete" />
          </span>
        )
      }
    ];
    const data = [
      {
        key: "1",
        service: "John Brown",
        charge: 32,
        tmu: "New York No. 1 Lake Park"
      }
    ];

    return (
      <Card className="gx-card" title="Summary">
        <Row>
          <Col span={24}>
            <PolicyRecordTabe columns={columns} data={data} />
          </Col>
        </Row>
      </Card>
    );
  }
}

export default AddPolicy;
