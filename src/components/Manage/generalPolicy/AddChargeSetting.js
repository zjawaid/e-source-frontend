import React, { Component } from "react";
import { Modal } from "antd";
import AddRecord from "./ChargeSettingsForm";
class ChargeSettingModal extends Component {
  constructor(props) {
    super(props);
    console.log("charge Setting ", props);
    this.state = { confirmLoading: false, visible: this.props.visible };
  }

  handleOk = () => {
    this.props.showModal(false);
    this.setState({
      confirmLoading: true
    });
    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false
      });
    }, 2000);
  };

  handleCancel = () => {
    this.props.showModal(false);
    console.log("Clicked cancel button", this.props.visible);
    this.setState({
      visible: !this.props.visible
    });
  };
  getData = data => {
    this.props.onAdd(data);
  };

  render() {
    return (
      <Modal
        style={{ top: 20 }}
        title="Add Settings"
        visible={this.props.visible}
        okText={this.props.action}
        onOk={this.handleOk}
        confirmLoading={this.state.confirmLoading}
        onCancel={this.handleCancel}
        footer={null}
      >
        <p>
          <AddRecord />
        </p>
      </Modal>
    );
  }
}
export default ChargeSettingModal;
