import React, { Component } from "react";
import {
  Form,
  Input,
  Select,
  DatePicker,
  Tooltip,
  Icon,
  Card,
  Row,
  Col,
  Divider
} from "antd";
import TicketSummary from "./SummaryTable";
import Widget from "components/Widget/index";

class TicketModificationSettings extends Component {
  constructor(props) {
    super(props);
  
  }
  render() {
    const columns = [
      {
        title: "Airline",
        dataIndex: "airline",
        key: "airline",
        render: text => <a>{text}</a>
      },
      {
        title: "Commission Percentage",
        dataIndex: "commisionPercentage",
        key: "commissionPercentage"
      },
      ,
      {
        title: "Period",
        dataIndex: "Period",
        key: "Period"
      },

      {
        title: "Payment Type",
        dataIndex: "paymentType",
        key: "paymentType"
      },
      {
        title: "Action",
        key: "action",

        render: (text, record) => (
          <span>
            {data ? (
              <div>
                {" "}
                <Icon type="edit" />
                <a onClick={this.showModal}>Edit</a>
                <Divider type="vertical" />
                <Icon type="delete" />
                <a>Delete</a>
              </div>
            ) : null}
          </span>
        )
      }
    ];
    const data = [];
    return (
      <Widget
        title={
          <h2 className="h4 gx-text-capitalize gx-mb-0">Ticket/TMU Settings</h2>
        }
        extra={
          <p className="gx-text-primary gx-mb-0 gx-pointer gx-d-none gx-d-sm-block">
            <i className="icon icon-add-circle gx-fs-lg gx-d-inline-flex gx-vertical-align-middle" />{" "}
            Add New
          </p>
        }
      >
        <Row>
          <Col span={24}>
            <TicketSummary columns={columns} data={data} />
          </Col>
        </Row>
      </Widget>
    );
  }
}

export default TicketModificationSettings;
