import React, { Component } from "react";
import { Modal } from "antd";
import EditRecord from "./ChargeSettingsForm";
AddChargeSetting = props => {
  handleOk = () => {};

  handleCancel = () => {};

  return (
    <Modal
      style={{ top: 20 }}
      title="Edit Settings"
      visible={}
      okText="update"
      onOk={handleOk}
      onCancel={handleCancel}
    >
      <p>
        <EditRecord />
      </p>
    </Modal>
  );
};

export default EditChargeSetting;
