import React, { Component } from "react";
import { Icon, Modal, Card, Row, Divider, Col } from "antd";
import DisplayTable from "./SummaryTable";
import Charge from "./AddChargeSetting";
import Widget from "components/Widget/index";
import { array } from "prop-types";
import Record from "./ChargeSettingsForm";

class DiscountSettings extends Component {
  constructor(props) {
    super(props);
    this.state = { visible: false, data: [] };
  }
  showModal = () => {
    this.setState({ visible: !this.state.visible});
  };

  handleCancel=()=>{
    this.setState({visible:false})
  }

  showData = data => {
    console.log("received data: ", data);
    let array = this.state.data;

    array.push(data);
    this.setState({ data: array });
  };
  render() {
    console.log("visible value:", this.state.visible);
    const columns = [
      {
        title: "Content",
        dataIndex: "content",
        key: "airline",
        render: text => <a>{text}</a>
      },
      {
        title: "Airline",
        dataIndex: "airline",
        key: "airline",
        render: text => <a>{text}</a>
      },
      {
        title: "Comm/PSF",
        dataIndex: "commision",
        key: "commision"
      },
      {
        title: "Value",
        dataIndex: "value",
        key: "value"
      },

      {
        title: "Apply On",
        dataIndex: "applyOn",
        key: "applyOn"
      },
      {
        title: "Time Period",
        dataIndex: "time",
        key: "time"
      },
      {
        title: "Class",
        dataIndex: "class",
        key: "class"
      },

      {
        title: "Action",
        key: "action",
        render: (text, record) => (
          <span>
            {this.state.data ? (
              <div>
                <i
                  className="icon icon-add-circle gx-fs-lg gx-d-inline-flex gx-vertical-align-middle"
                  onClick={this.showModal}
                />

                <Divider type="vertical" />
                <Icon type="edit" onClick={this.showModal} />

                <Divider type="vertical" />
                <Icon type="delete" />
              </div>
            ) : null}
          </span>
        )
      }
    ];

    return (
      <Widget
        title={
          <h2 className="h4 gx-text-capitalize gx-mb-0">Charge Settings</h2>
        }
        extra={
          <p
            className="gx-text-primary gx-mb-0 gx-pointer gx-d-none gx-d-sm-block"
            onClick={this.showModal}
          >
            <i className="icon icon-add-circle gx-fs-lg gx-d-inline-flex gx-vertical-align-middle" />{" "}
            Add New
          </p>
        }
      >
        <Modal
          style={{ top: 20 }}
          title="Settings"
          visible={this.state.visible}
          
          onCancel={this.handleCancel}
        >
          <p>
            <Record />
          </p>
        </Modal>
        <Row>
          <Col span={24}>
            <DisplayTable columns={columns} data={this.state.data} />
          </Col>
        </Row>
      </Widget>
    );
  }
}

export default DiscountSettings;
