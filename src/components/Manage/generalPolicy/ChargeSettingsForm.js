import React from "react";
import {
  Form,
  Input,
  Tooltip,
  Icon,
  Cascader,
  Select,
  Row,
  Col,
  Checkbox,
  Button,
  AutoComplete,
  DatePicker,
  Divider
} from "antd";
import WrappedDynamicFieldSet from "../../../routes/components/dataEntry/Form/DynamicFormItem";
import moment from "moment";

const { Option } = Select;
const AutoCompleteOption = AutoComplete.Option;

class RegistrationForm extends React.Component {
  state = {
    confirmDirty: false,
    autoCompleteResult: []
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, value) => {
      if (!err) {
        this.props.onSubmit(value);
        console.log("Received values of form: ", value);
      }
    });
  };

  render() {
    const { RangePicker } = DatePicker;
    const { getFieldDecorator } = this.props.form;
    const { autoCompleteResult } = this.state;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
      }
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0
        },
        sm: {
          span: 16,
          offset: 8
        }
      }
    };

    const content = (
      <Select>
        <Select.Option>Airline</Select.Option>
      </Select>
    );

    return (
      <Form {...formItemLayout} onSubmit={this.handleSubmit}>
        <Form.Item label="Content">
          {getFieldDecorator("content", {
            rules: [
              {
                required: true,
                message: "Please Select Content"
              }
            ]
          })(
            <Select>
              <Select.Option key="airline">Airline</Select.Option>
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Airline">
          {getFieldDecorator("airline", {
            rules: [
              {
                required: true,
                message: "Please input name of airline"
              }
            ]
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Commission/PSF">
          {getFieldDecorator("commision", {
            rules: [
              {
                required: true,
                message: "Please select this field!"
              }
            ]
          })(
            <Select>
              <Select.Option key="commision">Commision</Select.Option>
              <Select.Option key="PSF">PSF</Select.Option>
            </Select>
          )}
        </Form.Item>
        <Form.Item label="value">
          {getFieldDecorator("value", {
            rules: [
              {
                required: true,
                message: "Please enter value for com/PSF !"
              }
            ]
          })(<Input />)}
        </Form.Item>

        <Form.Item
          label={
            <span>
              Apply on&nbsp;
              <Tooltip title="What do you want others to call you?">
                <Icon type="question-circle-o" />
              </Tooltip>
            </span>
          }
        >
          {getFieldDecorator("applyOn", {
            rules: [
              {
                required: true,
                message: "Please select this field"
              }
            ]
          })(
            <Select>
              <Select.Option key="flat">Flat</Select.Option>
              <Select.Option key="percentOnBaseFair">
                Percent on base fair
              </Select.Option>

              <Select.Option key="percentOnTotTax">
                Percent on TOT tax
              </Select.Option>
              <Select.Option key="percentOnTotAmount">
                Percent on TOT amount
              </Select.Option>
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Select Time Period">
          {getFieldDecorator("time", {
            rules: [
              {
                initialValue: [moment().format(), moment().format()],
                required: true,
                message: "Please select select the time period"
              }
            ]
          })(<RangePicker defaultValue={[moment(), moment()]} />)}
        </Form.Item>
        <Form.Item label="Class">
          {getFieldDecorator("class", {
            rules: [{ required: true, message: "please select the class" }]
          })(
            <Select>
              <Select.Option key="a">A</Select.Option>
            </Select>
          )}
        </Form.Item>

        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            style={{ marginLeft: "410px" }}
          >
            Add
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

const WrappedRegistrationForm = Form.create({ name: "register" })(
  RegistrationForm
);

export default WrappedRegistrationForm;
