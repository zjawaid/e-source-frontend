import { Table, Divider, Tag, Icon, Modal } from "antd";
import React, { Component } from "react";
import AddRecord from "./ChargeSettingsForm";

class Records extends Component {
  constructor(props) {
    super(props);
    this.state = {
      confirmLoading: false,
      visible: false
    };
  }

  render() {
    const { confirmLoading, visible } = this.state;
    return (
      <div>
        <Table
          pagination={false}
          size="default"
          className="gx-table-responsive"
          columns={this.props.columns}
          dataSource={this.props.data}
        />
        {this.props.modal}
      </div>
    );
  }
}

export default Records;
