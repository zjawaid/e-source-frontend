import React from "react";
import { Table, Button, Badge, Dropdown, Icon, Divider } from "antd";
import Widget from "components/Widget/index";
import AddPeriod from "./AddPeriod";
import GeneratePeriod from "./GeneratePeriod";
import AddCalendar from "./AddCalendar";
import { connect } from "react-redux";
import renderEmpty from "antd/lib/config-provider/renderEmpty";
import { addPeriod } from "appRedux/actions/Calendar";

const columns = [
  {
    title: "Title",
    dataIndex: "title",
    key: "title",
    ellipsis: true
  },
  {
    title: "Description",
    dataIndex: "description",
    key: "description",
    ellipsis: true
  },
  {
    title: "Action",
    dataIndex: "action",
    key: "action",
    ellipsis: true,
    render: () => (
      <span>
        <Icon type="edit" />
        <Divider type="vertical" />
        <Icon type="delete" />
      </span>
    )
  }
];

class AvailableCalendars extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRowKeys: [], // Check here to configure the default column
      loading: false,
      addPeriod: false,
      generatePeriod: false,
      addCalendar: false
    };
  }

  cancel = () => {
    this.setState({ addCalendar: false });
  };

  expandedRowRender = () => {
    const columns = [
      { title: "Title", dataIndex: "title", key: "title" },
      { title: "SP. Start Date", dataIndex: "spStartDate", key: "spStartDate" },
      {
        title: "SP. End Date",
        key: "spEndDate",
        dataIndex: "spEndDate"
      },
      { title: "No. of Days", dataIndex: "numOfDays", key: "days" },
      { title: "ST. Start Date", dataIndex: "stStartDate", key: "stStartDate" },
      { title: "ST. End Date", dataIndex: "stEndDate", key: "stEndDate" }
    ];

    const data = this.props.timePeriods;

    return (
      <Table
        columns={columns}
        dataSource={data}
        pagination={false}
        footer={() => {
          return (
            <span>
              <Button
                onClick={() =>
                  this.setState({ addPeriod: true, generatePeriod: false })
                }
              >
                Add Period
              </Button>
              <Button
                onClick={() =>
                  this.setState({ addPeriod: false, generatePeriod: true })
                }
              >
                Generate Period
              </Button>
              {this.state.addPeriod ? <AddPeriod /> : null}
              {this.state.generatePeriod ? <GeneratePeriod /> : null}
            </span>
          );
        }}
      />
    );
  };

  start = () => {
    this.setState({ loading: true });
    // ajax request after empty completing
    setTimeout(() => {
      this.setState({
        selectedRowKeys: [],
        loading: false
      });
    }, 1000);
  };

  onSelectChange = selectedRowKeys => {
    console.log("selectedRowKeys changed: ", selectedRowKeys);
    this.setState({ selectedRowKeys });
  };

  render() {
    const { calendars, addCalendar } = this.props;
    const { loading, selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange
    };
    const data = calendars;
    const hasSelected = selectedRowKeys.length > 0;
    return (
      <Widget
        title={
          <h2 className="h4 gx-text-capitalize gx-mb-0">Available Calendars</h2>
        }
        extra={
          <p
            className="gx-text-primary gx-mb-0 gx-pointer gx-d-none gx-d-sm-block"
            onClick={() => {
              this.setState({ addCalendar: !this.state.addCalendar });
            }}
          >
            <i
              className={
                this.state.addCalendar
                  ? ""
                  : "icon icon-add-circle gx-fs-lg gx-d-inline-flex gx-vertical-align-middle"
              }
            />{" "}
            {this.state.addCalendar ? "Cancel" : "Add New Calendar"}
          </p>
        }
      >
        {this.state.addCalendar ? <AddCalendar onCancel={this.cancel} /> : null}

        <div style={{ marginBottom: 16 }}>
          <span style={{ marginLeft: 8 }}>
            {hasSelected ? `Selected ${selectedRowKeys.length} items` : ""}
          </span>
        </div>
        <Table
          rowSelection={rowSelection}
          columns={columns}
          dataSource={data}
          className="gx-table-no-bordered"
          expandedRowRender={this.expandedRowRender}
        />
      </Widget>
    );
  }
}

const mapStateToProps = ({ calendar }) => {
  const { calendars, addCalendar, timePeriods } = calendar;
  return { calendars, addCalendar, timePeriods };
};
export default connect(mapStateToProps, { addPeriod })(AvailableCalendars);
