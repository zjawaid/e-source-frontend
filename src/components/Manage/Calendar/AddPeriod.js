import React from "react";
import { Form, Input, DatePicker, Button } from "antd";
import { connect } from "react-redux";
import { addPeriod, generatePeriod } from "appRedux/actions/Calendar";

var moment = require("moment");

const AddPeriod = props => {
  const handleSubmit = e => {
    e.preventDefault();

    props.form.validateFields((err, values) => {
      if (!err) {
        let title = e.target[0].value;
        let spStartDate = e.target[1].value;
        let spEndDate = e.target[2].value;
        let StStartDate = e.target[3].value;
        let StEndDate = e.target[4].value;

        let start = moment(spStartDate);
        let end = moment(spEndDate);

        const numOfDays = end.diff(start, "days");

        props.addPeriod(
          title,
          spStartDate,
          spEndDate,
          numOfDays,
          StStartDate,
          StEndDate
        );
      }
    });
  };
  const { getFieldDecorator } = props.form;
  return (
    <Form layout="inline" onSubmit={handleSubmit}>
      <Form.Item>
        {getFieldDecorator("title", {
          rules: [{ required: true, message: "please provide the title" }]
        })(<Input placeholder="title" />)}
      </Form.Item>

      <Form.Item>
        {getFieldDecorator("SpDate", {
          initialValue: [moment()],
          rules: [{ required: true, message: "this field is required" }]
        })(
          <DatePicker.RangePicker
            disabledDate={current => {
              return current && current <= moment().subtract(1, "days");
            }}
            placeholder={["SP Start Date", "SP End Date"]}
          />
        )}
      </Form.Item>

      <Form.Item>
        {getFieldDecorator("StDate", {
          rules: [{ required: true, message: "this field is required" }]
        })(
          <DatePicker.RangePicker
            placeholder={["ST Start Date", "ST End Date"]}
          />
        )}
      </Form.Item>
      <Form.Item>
        <Button htmlType="submit">Add</Button>
      </Form.Item>

      <Form.Item>
        <Button>Cancel</Button>
      </Form.Item>
    </Form>
  );
};

const AddPeriodForm = Form.create()(AddPeriod);

export default connect(null, { addPeriod, generatePeriod })(AddPeriodForm);
