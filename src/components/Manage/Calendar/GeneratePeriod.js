import React from "react";
import { Form, Input, Select, DatePicker, InputNumber, Button } from "antd";

const GeneratePeriod = props => {
  const { getFieldDecorator } = props.form;
  return (
    <Form layout="inline">
      <Form.Item>
        {getFieldDecorator("title", {
          initialValue: "Select Time Period",
          rules: [{ required: true, message: "please select time period" }]
        })(
          <Select style={{ width: "200px" }}>
            <Select.Option key="Fortnightly" value="Fortnightly">
              Fortnightly
            </Select.Option>
            <Select.Option key="Monthly" value="Monthly">
              Monthly
            </Select.Option>
          </Select>
        )}
      </Form.Item>

      <Form.Item>
        {getFieldDecorator("periodStartDate", {
          rules: [{ required: true, message: "please select start date" }]
        })(<DatePicker />)}
      </Form.Item>

      <Form.Item>
        <InputNumber min={1} defaultValue={1} />
      </Form.Item>

      <Form.Item>
        <Button htmlType="submit">Add</Button>
      </Form.Item>

      <Form.Item>
        <Button>Cancel</Button>
      </Form.Item>
    </Form>
  );
};

const GeneratePeriodForm = Form.create()(GeneratePeriod);

export default GeneratePeriodForm;
