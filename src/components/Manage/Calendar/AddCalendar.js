import React from "react";
import { Form, Input, Button } from "antd";
import { connect } from "react-redux";
import { addCalendar, cancel } from "../../../appRedux/actions/Calendar";
const Fields = props => {
  const handleSubmit = e => {
    e.preventDefault();
    props.form.validateFields((err, value) => {
      if (!err) {
        console.log("received values from calendar", value);
        let title = e.target[0].value;
        let description = e.target[1].value;
        props.addCalendar(title, description);
      }
    });
  };

  return (
    <Form layout="inline" onSubmit={handleSubmit}>
      <Form.Item>
        {props.form.getFieldDecorator("title", {
          rules: [{ required: true, message: "please give the title" }]
        })(<Input placeholder="Calendar title" />)}
      </Form.Item>

      <Form.Item>
        <Input placeholder="description(optional)" />{" "}
      </Form.Item>
      <Form.Item>
        <Button htmlType="submit">Add</Button>
      </Form.Item>
    </Form>
  );
};
const AddNew = Form.create()(Fields);

export default connect(null, { addCalendar, cancel })(AddNew);
