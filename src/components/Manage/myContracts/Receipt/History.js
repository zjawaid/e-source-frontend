import React, { Component } from "react";
import { Table } from "antd";

import Widget from "components/Widget/index";

class History extends Component {
  state = { sortedInfo: null, filteredInfo: null };

  handleChange = (pagination, filters, sorter) => {
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter
    });
  };

  render() {
    let { sortedInfo, filteredInfo } = this.state;
    sortedInfo = sortedInfo || {};
    filteredInfo = filteredInfo || {};
    const columns = [
      {
        title: "Voucher No",
        dataIndex: "voucher",
        key: "voucher"
      },

      {
        title: "Creation Date",
        dataIndex: "creationDate",
        key: "creationDate"
      },

      {
        title: "Payment",
        dataIndex: "payment",
        key: "payment"
      },
      {
        title: "Cheque/Ref No",
        dataIndex: "cheque",
        key: "cheque"
      },

      {
        title: "Payment Detail",
        dataIndex: "paymentDetail",
        key: "paymentDetail"
      },

      {
        title: "Amount",
        dataIndex: "amount",
        key: "amount"
      },

      {
        title: "Received From",
        dataIndex: "receivedFrom",
        key: "receivedFrom"
      },

      {
        title: "Status",
        dataIndex: "status",
        key: "status"
      }
    ];

    const data = [];

    return (
      <Widget title="History">
        <Table
          className="gx-table-no-bordered"
          columns={columns}
          dataSource={data}
          onChange={this.handleChange}
        />
      </Widget>
    );
  }
}
export default History;
