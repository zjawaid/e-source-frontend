import React from "react";
import Widget from "components/Widget/index";
import { Table } from "antd";
const OutStanding = () => {
  const columns = [
    {
      title: "Period",
      dataIndex: "period",
      key: "period"
    },
    {
      title: "Amount",
      dataIndex: "amount",
      key: "amount"
    }
  ];
  const data = [];
  return (
    <Widget title="Outstanding">
      <Table
        className="gx-table-no-bordered"
        columns={columns}
        dataSource={data}
        footer={() => {
          return "Total Outstanding";
        }}
      />
    </Widget>
  );
};

export default OutStanding;
