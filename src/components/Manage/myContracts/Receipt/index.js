import React from "react";
import { Row, Col } from "antd";
import Outstanding from "./Outstanding";
import History from "./History";

const Receipt = () => {
  return (
    <Row>
      <Col span={24}>
        <Outstanding />
      </Col>
      <Col span={24}>
        <History />
      </Col>
    </Row>
  );
};
export default Receipt;
