import React, { Component } from "react";
import { Table } from "antd";

import Widget from "components/Widget/index";

class History extends Component {
  state = { sortedInfo: null, filteredInfo: null };

  handleChange = (pagination, filters, sorter) => {
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter
    });
  };

  render() {
    let { sortedInfo, filteredInfo } = this.state;
    sortedInfo = sortedInfo || {};
    filteredInfo = filteredInfo || {};
    const columns = [
      {
        title: "Period",
        dataIndex: "period",
        key: "period",
        filters: [
          { text: "CON1_AG1", value: "CON1_AG1" },
          { text: "UMER_CON1", value: "UMER_CON1" },
          { text: "UATCON1_UATAG1", value: "UATCON1_UATAG1" }
        ],
        filteredValue: filteredInfo.period || null,
        onFilter: (value, record) => record.period.includes(value),

        ellipsis: true
      },

      {
        title: "User",
        dataIndex: "user",
        key: "user",

        sorter: (a, b) => a.user.length - b.user.length,
        sortOrder: sortedInfo.columnKey === "user" && sortedInfo.order,
        ellipsis: true
      },

      {
        title: "Date/Time",
        dataIndex: "dateTime",
        key: "dateTime",

        sorter: (a, b) => a.dateTime.length - b.dateTime.length,
        sortOrder: sortedInfo.columnKey === "dateTime" && sortedInfo.order,
        ellipsis: true
      },

      {
        title: "Type",
        dataIndex: "type",
        key: "type",
        filters: [
          { text: "UAT CONSOLIDATOR 1", value: "UAT CONSOLIDATOR 1" },
          { text: "UAT CONSOLIDATOR 2", value: "UAT CONSOLIDATOR 2" },
          { text: "UAT CONSOLIDATOR 3", value: "UAT CONSOLIDATOR 3" }
        ],
        filteredValue: filteredInfo.toAgency || null,
        onFilter: (value, record) => record.type.includes(value),

        ellipsis: true
      },

      {
        title: "Amount",
        dataIndex: "amount",
        key: "amount",

        sorter: (a, b) => a.amount - b.amount,
        sortOrder: sortedInfo.columnKey === "amount" && sortedInfo.order,
        ellipsis: true
      }
    ];

    const data = [];

    return (
      <Widget title="History">
        <Table
          className="gx-table-no-bordered"
          columns={columns}
          dataSource={data}
          onChange={this.handleChange}
        />
      </Widget>
    );
  }
}
export default History;
