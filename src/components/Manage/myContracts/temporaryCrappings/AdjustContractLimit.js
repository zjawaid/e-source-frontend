import React from "react";
import Widget from "components/Widget/index";
import { Table, Button } from "antd";

const AdjustContractLimit = () => {
  const columns = [
    {
      title: "User",
      dataIndex: "user",
      key: "user"
    },
    {
      title: "Date/Time",
      dataIndex: "dateTime",
      key: "dateTime"
    },
    {
      title: "Type",
      dataIndex: "type",
      key: "type"
    },
    {
      title: "Amount",
      dataIndex: "amount",
      key: "amount"
    }
  ];

  const data = [];

  return (
    <Widget title="Period: 12/12/2019-31/12/2019">
      <div className="gx-table-operations">
        <Button>Increase</Button>
        <Button>Decrease</Button>
      </div>
      <Table
        className="gx-table-no-bordered"
        columns={columns}
        dataSource={data}
        pagination={false}
        bordered={false}
        size="small"
      />
    </Widget>
  );
};
export default AdjustContractLimit;
