import React from "react";
import { Row, Col } from "antd";
import AdjustContractLimit from "./AdjustContractLimit";
import History from "./History";

const TemporaryCrappings = () => {
  return (
    <Row>
      <Col span={24}>
        <AdjustContractLimit />
      </Col>
      <Col span={24}>
        <History />
      </Col>
    </Row>
  );
};

export default TemporaryCrappings;
