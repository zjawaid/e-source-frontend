import React from "react";
import { Table, Button } from "antd";

const data = [];

class App extends React.Component {
  state = {
    filteredInfo: null,
    sortedInfo: null
  };

  handleChange = (pagination, filters, sorter) => {
    console.log("Various parameters", pagination, filters, sorter);
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter
    });
  };

  clearFilters = () => {
    this.setState({ filteredInfo: null });
  };

  clearAll = () => {
    this.setState({
      filteredInfo: null,
      sortedInfo: null
    });
  };

  setAgeSort = () => {
    this.setState({
      sortedInfo: {
        order: "descend",
        columnKey: "age"
      }
    });
  };

  render() {
    let { sortedInfo, filteredInfo } = this.state;
    sortedInfo = sortedInfo || {};
    filteredInfo = filteredInfo || {};
    const columns = [
      {
        title: "Code",
        dataIndex: "code",
        key: "code",
        filters: [
          { text: "CON1_AG1", value: "CON1_AG1" },
          { text: "UMER_CON1", value: "UMER_CON1" },
          { text: "UATCON1_UATAG1", value: "UATCON1_UATAG1" }
        ],
        filteredValue: filteredInfo.code || null,
        onFilter: (value, record) => record.name.includes(value),
        sorter: (a, b) => a.code.length - b.code.length,
        sortOrder: sortedInfo.columnKey === "code" && sortedInfo.order,
        ellipsis: true
      },

      {
        title: "From Agency",
        dataIndex: "fromAgency",
        key: "fromAgency",
        filters: [
          { text: "UAT CONSOLIDATOR 1", value: "UAT CONSOLIDATOR 1" },
          { text: "UAT CONSOLIDATOR 2", value: "UAT CONSOLIDATOR 2" },
          { text: "UAT CONSOLIDATOR 3", value: "UAT CONSOLIDATOR 3" }
        ],
        filteredValue: filteredInfo.fromAgency || null,
        onFilter: (value, record) => record.fromAgency.includes(value),
        sorter: (a, b) => a.fromAgency.length - b.fromAgency.length,
        sortOrder: sortedInfo.columnKey === "fromAgency" && sortedInfo.order,
        ellipsis: true
      },

      {
        title: "To Agency",
        dataIndex: "toAgency",
        key: "toAgency",
        filters: [
          { text: "UAT CONSOLIDATOR 1", value: "UAT CONSOLIDATOR 1" },
          { text: "UAT CONSOLIDATOR 2", value: "UAT CONSOLIDATOR 2" },
          { text: "UAT CONSOLIDATOR 3", value: "UAT CONSOLIDATOR 3" }
        ],
        filteredValue: filteredInfo.toAgency || null,
        onFilter: (value, record) => record.toAgency.includes(value),
        sorter: (a, b) => a.toAgency.length - b.toAgency.length,
        sortOrder: sortedInfo.columnKey === "toAgency" && sortedInfo.order,
        ellipsis: true
      },

      {
        title: "Limit",
        dataIndex: "limit",
        key: "limit",

        sorter: (a, b) => a.limit - b.limit,
        sortOrder: sortedInfo.columnKey === "limit" && sortedInfo.order,
        ellipsis: true
      },

      {
        title: "Utilized",
        dataIndex: "utilized",
        key: "utilized",

        sorter: (a, b) => a.utilized - b.utilized,
        sortOrder: sortedInfo.columnKey === "utilized" && sortedInfo.order,
        ellipsis: true
      },

      {
        title: "Utilized(%)",
        dataIndex: "utilizedPercent",
        key: "utilizedPercent",

        sorter: (a, b) => a.utilizedPercent - b.utilizedPercent,
        sortOrder:
          sortedInfo.columnKey === "utilizedPercent" && sortedInfo.order,
        ellipsis: true
      },

      {
        title: "Balance",
        dataIndex: "balance",
        key: "balance",

        sorter: (a, b) => a.balance - b.balance,
        sortOrder: sortedInfo.columnKey === "balance" && sortedInfo.order,
        ellipsis: true
      },

      {
        title: "Status",
        dataIndex: "status",
        key: "status",
        filters: [
          { text: "Active", value: "Active" },
          { text: "Inactive", value: "Inactive" }
        ],
        filteredValue: filteredInfo.status || null,
        onFilter: (value, record) => record.status.includes(value),

        ellipsis: true
      },

      {
        title: "Block Status",
        dataIndex: "blockStatus",
        key: "blockStatus",
        filters: [
          { text: "Blocked", value: "Blocked" },
          { text: "Unblocked", value: "Unblocked" }
        ],
        filteredValue: filteredInfo.blockStatus || null,
        onFilter: (value, record) => record.blockStatus.includes(value),

        ellipsis: true
      },

      {
        title: "Action",
        dataIndex: "action",
        key: "action",
        render: (record, text) => {
          return (
            <span>
              <a>view</a>
            </span>
          );
        },
        ellipsis: true
      }
    ];
    return (
      <div>
        <div className="gx-table-operations">
          <Button onClick={this.clearFilters}>Clear filters</Button>
          <Button onClick={this.clearAll}>Clear filters and sorters</Button>
        </div>
        <Table
          className="gx-table-no-bordered"
          columns={columns}
          dataSource={data}
          onChange={this.handleChange}
        />
      </div>
    );
  }
}

export default App;
