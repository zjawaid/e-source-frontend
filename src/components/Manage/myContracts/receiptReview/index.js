import React from "react";
import { Row, Col } from "antd";
import ReceiptCredentials from "./ReceiptCredentials";

const ReceiptReview = () => {
  return (
    <Row>
      <Col span={24}>
        <ReceiptCredentials />
      </Col>
    </Row>
  );
};

export default ReceiptReview;
