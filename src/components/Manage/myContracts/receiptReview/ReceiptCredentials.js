import React from "react";
import { Table, Button } from "antd";
import Widget from "components/Widget/index";

const data = [];

class ReceiptCredentials extends React.Component {
  state = {
    filteredInfo: null,
    sortedInfo: null
  };

  handleChange = (pagination, filters, sorter) => {
    console.log("Various parameters", pagination, filters, sorter);
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter
    });
  };

  clearFilters = () => {
    this.setState({ filteredInfo: null });
  };

  clearAll = () => {
    this.setState({
      filteredInfo: null,
      sortedInfo: null
    });
  };

  render() {
    let { sortedInfo, filteredInfo } = this.state;
    sortedInfo = sortedInfo || {};
    filteredInfo = filteredInfo || {};
    const columns = [
      {
        title: "Date",
        dataIndex: "date",
        key: "date"
      },

      {
        title: "Payment Mode",
        dataIndex: "paymentMode",
        key: "paymentMode"
      },

      {
        title: "Reference No",
        dataIndex: "reference",
        key: "reference"
      },

      {
        title: "Amount",
        dataIndex: "amount",
        key: "amount",

        sorter: (a, b) => a.amount - b.amount,
        sortOrder: sortedInfo.columnKey === "amount" && sortedInfo.order,
        ellipsis: true
      },

      {
        title: "Status",
        dataIndex: "status",
        key: "status",

        filters: [
          { text: "Approved", value: "Approved" },
          { text: "Rejected", value: "Rejected" }
        ],
        filteredValue: filteredInfo.status || null,
        onFilter: (value, record) => record.status.includes(value),

        ellipsis: true
      }
    ];
    return (
      <Widget title="Receipt Review">
        <Table
          className="gx-table-no-bordered"
          columns={columns}
          dataSource={data}
          onChange={this.handleChange}
        />
      </Widget>
    );
  }
}

export default ReceiptCredentials;
