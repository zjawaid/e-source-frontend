import React from "react";
import Widget from "components/Widget";

import { connect } from "react-redux";
import { Checkbox, Row, Col, Form, Button, Input, Select } from "antd";

const GeneralInfo = props => {
  return (
    <Widget title="General Info">
      {" "}
      <Row>
        <Col span={12}>
          <div>
            <div className="gx-media gx-align-items-center gx-flex-nowrap gx-pro-contact-list">
              <div className="gx-mr-3">
                <i className="fas fa-file-contract gx-fs-xxl gx-text-grey"></i>
              </div>
              <div className="gx-media-body">
                <span className="gx-mb-0 gx-text-grey gx-fs-sm">Code</span>
                <p className="gx-mb-0">UATAG 1</p>
              </div>
            </div>
            <div className="gx-media gx-align-items-center gx-flex-nowrap gx-pro-contact-list">
              <div className="gx-mr-3">
                <i className="fas fa-dollar-sign gx-fs-xxl gx-text-grey"></i>
              </div>
              <div className="gx-media-body">
                <span className="gx-mb-0 gx-text-grey gx-fs-sm">Currency</span>
                <p className="gx-mb-0">PKR</p>
              </div>
            </div>
            <div className="gx-media gx-align-items-center gx-flex-nowrap gx-pro-contact-list">
              <div className="gx-mr-3">
                <i className="fas fa-question-circle gx-fs-xl gx-text-grey"></i>
              </div>
              <div className="gx-media-body">
                <span className="gx-mb-0 gx-text-grey gx-fs-sm">Status</span>
                <p className="gx-mb-0">Active</p>
              </div>
            </div>
            <div className="gx-media gx-align-items-center gx-flex-nowrap gx-pro-contact-list">
              <div className="gx-mr-3">
                <i className="fas fa-calendar-plus gx-fs-xxl gx-text-grey"></i>
              </div>
              <div className="gx-media-body">
                <span className="gx-mb-0 gx-text-grey gx-fs-sm">
                  Start date
                </span>
                <p className="gx-mb-0">12/12/2019</p>
              </div>
            </div>

            <div className="gx-media gx-align-items-center gx-flex-nowrap gx-pro-contact-list">
              <div className="gx-mr-3">
                <i className="fas fa-ban gx-fs-xxl gx-text-grey"></i>
              </div>
              <div className="gx-media-body">
                <span className="gx-mb-0 gx-text-grey gx-fs-sm">Blocked</span>

                <p className="gx-mb-0">Yes</p>
              </div>
            </div>
          </div>
        </Col>
        <Col span={12}>
          <div>
            <div className="gx-media gx-align-items-center gx-flex-nowrap gx-pro-contact-list">
              <div className="gx-mr-3">
                <i className="fas fa-question-circle gx-fs-xxl gx-text-grey"></i>
              </div>
              <div className="gx-media-body">
                <span className="gx-mb-0 gx-text-grey gx-fs-sm">Type</span>
                <p className="gx-mb-0">Credit</p>
              </div>
            </div>

            <div className="gx-media gx-align-items-center gx-flex-nowrap gx-pro-contact-list">
              <div className="gx-mr-3">
                <i className="fas fa-question-circle gx-fs-xxl gx-text-grey"></i>
              </div>
              <div className="gx-media-body">
                <span className="gx-mb-0 gx-text-grey gx-fs-sm">
                  Is Distributed
                </span>
                <p className="gx-mb-0">Yes</p>
              </div>
            </div>

            <div className="gx-media gx-align-items-center gx-flex-nowrap gx-pro-contact-list">
              <div className="gx-mr-3">
                <i className="fas fa-calendar gx-fs-xxl gx-text-grey"></i>
              </div>
              <div className="gx-media-body">
                <span className="gx-mb-0 gx-text-grey gx-fs-sm">Calendar</span>
                <p className="gx-mb-0">2019-2029</p>
              </div>
            </div>

            <div className="gx-media gx-align-items-center gx-flex-nowrap gx-pro-contact-list">
              <div className="gx-mr-3">
                <i className="fas fa-calendar-minus gx-fs-xxl gx-text-grey"></i>
              </div>
              <div className="gx-media-body">
                <span className="gx-mb-0 gx-text-grey gx-fs-sm">End date</span>
                <p className="gx-mb-0">10/12/2020</p>
              </div>
            </div>

            <div className="gx-media gx-align-items-center gx-flex-nowrap gx-pro-contact-list">
              <div className="gx-mr-3">
                <i className="fas fa-file-signature  gx-fs-xxl gx-text-grey"></i>
              </div>
              <div className="gx-media-body">
                <span className="gx-mb-0 gx-text-grey gx-fs-sm">
                  Contract Type
                </span>
                <p className="gx-mb-0">Locked</p>
              </div>
            </div>
          </div>
        </Col>
      </Row>
    </Widget>
  );
};
const mapStateToProps = ({ user }) => {
  const { selectedUser, showEditWindow } = user;
  return { selectedUser };
};

export default connect(mapStateToProps)(GeneralInfo);
