import React from "react";
import { Table } from "antd";
import Widget from "components/Widget/index";

const columns = [
  {
    title: "WAP",
    dataIndex: "wap"
  },
  {
    title: "Allow Terminal Access",
    dataIndex: "allowTerminalAccess"
  },

  {
    title: "Action",
    dataIndex: "action",
    render: text => {
      return <span className="gx-text-red">{text}</span>;
    }
  }
];

const data = [
  // {
  //   key: "1",
  //   wap: "q12",
  //   allowTerminalAccess: "Allow"
  // }
];

const USAPTable = () => {
  return (
    <Widget
      styleName="gx-order-history"
      title={<h2 className="h4 gx-text-capitalize gx-mb-0">USAPs</h2>}
    >
      <div className="gx-table-responsive">
        <Table
          className="gx-table-no-bordered"
          columns={columns}
          dataSource={data}
          pagination={false}
          bordered={false}
          size="small"
        />
      </div>
    </Widget>
  );
};

export default USAPTable;
