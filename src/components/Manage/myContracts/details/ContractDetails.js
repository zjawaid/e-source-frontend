import React from "react";
import Widget from "components/Widget";

import { connect } from "react-redux";
import { Row, Col } from "antd";

const Contact = props => {
  return (
    <Widget title="Contract Details">
      {" "}
      <Row>
        <Col span={12}>
          <div>
            <div className="gx-media gx-align-items-center gx-flex-nowrap gx-pro-contact-list">
              <div className="gx-mr-3">
                <i class="fas fa-building gx-fs-xxl gx-text-grey"></i>
                {/* <i className={`icon icon-email gx-fs-xxl gx-text-grey`} /> */}
              </div>
              <div className="gx-media-body">
                <span className="gx-mb-0 gx-text-grey gx-fs-sm">
                  Contract With
                </span>
                <p className="gx-mb-0">UATAG 1</p>
              </div>
            </div>
            <div className="gx-media gx-align-items-center gx-flex-nowrap gx-pro-contact-list">
              <div className="gx-mr-3">
                <i class="fas fa-tag gx-fs-xxl gx-text-grey"></i>
              </div>
              <div className="gx-media-body">
                <span className="gx-mb-0 gx-text-grey gx-fs-sm">Title</span>
                <p className="gx-mb-0">UAT AGENCY 1</p>
              </div>
            </div>
            <div className="gx-media gx-align-items-center gx-flex-nowrap gx-pro-contact-list">
              <div className="gx-mr-3">
                <i className="fas fa-map-marker-alt gx-fs-xxl gx-text-grey"></i>
                {"     "}
              </div>
              <div className="gx-media-body">
                <span className="gx-mb-0 gx-text-grey gx-fs-sm">address</span>
                <p className="gx-mb-0">Karachi</p>
              </div>
            </div>
            <div className="gx-media gx-align-items-center gx-flex-nowrap gx-pro-contact-list">
              <div className="gx-mr-3">
                <i class="fas fa-phone gx-fs-xxl gx-text-grey"></i>
              </div>
              <div className="gx-media-body">
                <span className="gx-mb-0 gx-text-grey gx-fs-sm">Phone</span>
                <p className="gx-mb-0">02136547894</p>
              </div>
            </div>
          </div>
        </Col>
      </Row>
    </Widget>
  );
};
const mapStateToProps = ({ user }) => {
  const { selectedUser, showEditWindow } = user;
  return { selectedUser };
};

export default connect(mapStateToProps)(Contact);
