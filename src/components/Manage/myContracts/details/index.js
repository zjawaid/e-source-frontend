import React from "react";
import { Col, Row } from "antd";

import ContractDetails from "./ContractDetails";
import ValueLimit from "./ValueLimit";
import GeneralInfo from "./GeneralInfo";
import USAP from "./USAPTable";
import Airlines from "./AirlinesTable";
import Configuration from "./Configuration";
import Blocked from "./SectorTable";

const Details = () => {
  return (
    <Row>
      <Col span={6}>
        <ContractDetails />
      </Col>
      <Col span={18}>
        <ValueLimit />
      </Col>
      <Col span={6}>
        <GeneralInfo />
      </Col>
      <Col span={18}>
        <Configuration />
      </Col>
      <Col span={8}>
        <USAP />
      </Col>
      <Col span={8}>
        <Airlines />
      </Col>
      <Col span={8}>
        <Blocked />
      </Col>
    </Row>
  );
};

export default Details;
