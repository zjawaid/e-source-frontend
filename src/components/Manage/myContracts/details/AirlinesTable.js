import React from "react";
import { Table } from "antd";
import Widget from "components/Widget";

const columns = [
  {
    title: "Code",
    dataIndex: "code"
  },
  {
    title: "Title",
    dataIndex: "title"
  },
  {
    title: "Amount",
    dataIndex: "amount"
  },
  {
    title: "Action",
    dataIndex: "action",
    render: text => {
      return <span className="gx-text-red">{text}</span>;
    }
  }
];

const data = [
  //   {
  //     code: "123",
  //     title: "Qatar Airways",
  //     amount: "10000"
  //   }
];

const AirlinesTable = () => {
  return (
    <Widget
      styleName="gx-order-history"
      title={<h2 className="h4 gx-text-capitalize gx-mb-0">Airlines</h2>}
    >
      <div className="gx-table-responsive">
        <Table
          className="gx-table-no-bordered"
          columns={columns}
          dataSource={data}
          pagination={false}
          bordered={false}
          size="small"
        />
      </div>
    </Widget>
  );
};

export default AirlinesTable;
