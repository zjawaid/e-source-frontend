import React from "react";
import Widget from "components/Widget";

import { connect } from "react-redux";
import {
  Checkbox,
  Row,
  Col,
  Form,
  Button,
  Input,
  Select,
  Tag,
  Divider
} from "antd";

import EcommerceStatus from "./MetricsCard";

const Configuration = props => {
  return (
    <Widget title="Configuration">
      <Row>
        <Col span={10}>
          <Row>
            <Col span={8}>
              <p className="gx-mb-0">Fare Review</p>
            </Col>
            <Col span={16} offset={8}>
              <Tag color="#f50">No</Tag>
            </Col>
          </Row>
          <Row>
            <Col span={8}>
              <p className="gx-mb-0">Allow Void</p>
            </Col>
            <Col span={8} offset={8}>
              <Tag color="#f50">No</Tag>
            </Col>
          </Row>
          <Row>
            <Col span={8}>
              <p className="gx-mb-0">Allow Refund</p>
            </Col>
            <Col span={8} offset={8}>
              <Tag color="#f50">No</Tag>
            </Col>
          </Row>
          <Row>
            <Col span={8}>
              <p className="gx-mb-0">Allow Exchange</p>
            </Col>
            <Col span={8} offset={8}>
              <Tag color="#f50">No</Tag>
            </Col>
          </Row>
          <Row>
            <Col span={8}>
              <p className="gx-mb-0">Refund review</p>
            </Col>
            <Col span={8} offset={8}>
              <Tag color="#f50">No</Tag>
            </Col>
          </Row>
          <Row>
            <Col span={8}>
              <p className="gx-mb-0">Allow Revaliation</p>
            </Col>
            <Col span={8} offset={8}>
              <Tag color="#f50">No</Tag>
            </Col>
          </Row>
          <br /> <br /> <br />
        </Col>

        <Col span={14}>
          <Row>
            <Col span={8}>
              <EcommerceStatus
                color="cyan"
                icon="revenue-new"
                title="PKR 100"
                colorTitle="cyan"
                subTitle="Void Charges"
                colorSubTitle="cyan"
              />
            </Col>
            <Col span={8}>
              <EcommerceStatus
                color="orange"
                icon="revenue-new"
                title="PKR 300"
                colorTitle="cyan"
                subTitle="Refund Charges"
                colorSubTitle="cyan"
              />
            </Col>
            <Col span={8}>
              <EcommerceStatus
                color="magenta"
                icon="revenue-new"
                title="PKR 200"
                colorTitle="cyan"
                subTitle="Exchane Charges"
                colorSubTitle="cyan"
              />
            </Col>
          </Row>
        </Col>
      </Row>
    </Widget>
  );
};
const mapStateToProps = ({ user }) => {
  const { selectedUser, showEditWindow } = user;
  return { selectedUser };
};

export default connect(mapStateToProps)(Configuration);
