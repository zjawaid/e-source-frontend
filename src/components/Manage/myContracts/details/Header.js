import React from "react";
import { Icon, Divider } from "antd";

const Header = () => {
  return (
    <div className="gx-profile-banner">
      <div className="gx-profile-container">
        <div className="gx-profile-banner-top">
          <div className="gx-profile-banner-top-left">
            <div className="gx-profile-banner-avatar"></div>
            <div className="gx-profile-banner-avatar-info">
              <h2 className="gx-mb-2 gx-mb-sm-3 gx-fs-xxl gx-font-weight-light">
                My Contract Details
              </h2>
            </div>
          </div>
          <div className="gx-profile-banner-top-right">
            <span className="gx-link">
              <Icon type="edit" />
            </span>
            <Divider type="vertical" />

            <span className="gx-link">
              <Icon type="lock" />
            </span>
            <Divider type="vertical" />
            <span className="gx-link">
              <Icon type="user" />
            </span>
          </div>
        </div>
        <div className="gx-profile-banner-bottom">
          <div className="gx-tab-list"></div>
        </div>
      </div>
    </div>
  );
};

export default Header;
