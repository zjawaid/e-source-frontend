import React from "react";
import { Table } from "antd";
import Widget from "components/Widget/index";

const columns = [
  {
    title: "Blocked Sectors",
    dataIndex: "blocked"
  }
];

const data = [
  //   {
  //     key: 1,
  //     blocked: ""
  //   }
];

const SectorTable = () => {
  return (
    <Widget
      styleName="gx-order-history"
      title={<h2 className="h4 gx-text-capitalize gx-mb-0">Sector</h2>}
    >
      <div className="gx-table-responsive">
        <Table
          className="gx-table-no-bordered"
          columns={columns}
          dataSource={data}
          pagination={false}
          bordered={false}
          size="small"
        />
      </div>
    </Widget>
  );
};

export default SectorTable;
