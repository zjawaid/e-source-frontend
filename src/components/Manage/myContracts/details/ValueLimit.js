import React from "react";
import Widget from "components/Widget";

import { connect } from "react-redux";
import { Checkbox, Row, Col, Form, Button, Input, Select } from "antd";

import EcommerceStatus from "components/Metrics/EcommerceStatus";

const Contact = props => {
  return (
    <Widget title="Value Limit">
      {" "}
      <Row>
        <Col span={6}>
          <EcommerceStatus
            color="cyan"
            icon="revenue-new"
            title="$9,623"
            colorTitle="cyan"
            subTitle="Contract Value"
            colorSubTitle="cyan"
          />
        </Col>
        <Col span={10}>
          <EcommerceStatus
            color="orange"
            icon="calendar  "
            title="12/12/2019-12/12/2020"
            colorTitle="orange"
            subTitle="Contract Period"
            colorSubTitle="orange"
          />
        </Col>
        <Col span={8}>
          <EcommerceStatus
            color="red"
            icon="revenue-new"
            title="PKR 1,623"
            colorTitle="red"
            subTitle="Balance"
            colorSubTitle="red"
          />
        </Col>
      </Row>
    </Widget>
  );
};
const mapStateToProps = ({ user }) => {
  const { selectedUser, showEditWindow } = user;
  return { selectedUser };
};

export default connect(mapStateToProps)(Contact);
