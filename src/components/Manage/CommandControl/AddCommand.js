import React from "react";
import { Form, Input, Button, Radio } from "antd";

const Fields = props => {
  return (
    <Form layout="inline">
      <Form.Item>
        {props.form.getFieldDecorator("command", {
          rules: [{ required: true, message: "please give command name" }]
        })(<Input placeholder="Command" />)}
      </Form.Item>

      <Form.Item>
        <Radio.Group name="radiogroup" defaultValue={1}>
          <Radio value="startsWith">Starts With</Radio>
          <Radio value="exactMatch">Exact Match</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item>
        <Button htmlType="submit">Add</Button>
      </Form.Item>
    </Form>
  );
};
export default Form.create()(Fields);
