import React from "react";
import { Table, Icon } from "antd";
import AddCommand from "./AddCommand";

const columns = [
  {
    title: "Command List",
    dataIndex: "commandList",
    ellipises: true
  },
  {
    title: "Command Type",
    dataIndex: "commandType",
    ellipises: true
  },
  {
    title: "Action",
    dataIndex: "commandList",
    ellipises: true,
    render: record => {
      return (
        <span>
          <Icon type="delete" />
        </span>
      );
    }
  }
];

const data = [];

const CommandList = () => {
  return (
    <div>
      <AddCommand />
      <Table
        columns={columns}
        dataSource={data}
        className="gx-table-no-bordered"
      />
    </div>
  );
};

export default CommandList;
