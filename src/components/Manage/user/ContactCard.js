import React from "react";
import Widget from "components/Widget";
import { connect } from "react-redux";
import { Checkbox } from "antd";

const Contact = props => {
  return (
    <Widget title="Info" styleName="gx-card-profile-sm">
      <div className="gx-media gx-align-items-center gx-flex-nowrap gx-pro-contact-list">
        <div className="gx-mr-3">
          <i className={`icon icon-email gx-fs-xxl gx-text-grey`} />
        </div>
        <div className="gx-media-body">
          <span className="gx-mb-0 gx-text-grey gx-fs-sm">Login Email</span>
          <p className="gx-mb-0">{props.selectedUser.data.loginEmail}</p>
        </div>
      </div>
      <div className="gx-media gx-align-items-center gx-flex-nowrap gx-pro-contact-list">
        <div className="gx-mr-3">
          <i className={`icon icon-link gx-fs-xxl gx-text-grey`} />
        </div>
        <div className="gx-media-body">
          <span className="gx-mb-0 gx-text-grey gx-fs-sm">Agency</span>
          <p className="gx-mb-0">{props.selectedUser.data.agency}</p>
        </div>
      </div>
      <div className="gx-media gx-align-items-center gx-flex-nowrap gx-pro-contact-list">
        <div className="gx-mr-3">
          <i className={`icon icon-phone gx-fs-xxl gx-text-grey`} />
        </div>
        <div className="gx-media-body">
          <span className="gx-mb-0 gx-text-grey gx-fs-sm">phone</span>
          <p className="gx-mb-0">021-55667789</p>
        </div>
      </div>
      <div className="gx-media gx-align-items-center gx-flex-nowrap gx-pro-contact-list">
        <div className="gx-mr-3">
          <i className={`icon icon-user gx-fs-xxl gx-text-grey`} />
        </div>
        <div className="gx-media-body">
          <span className="gx-mb-0 gx-text-grey gx-fs-sm">Name</span>
          <p className="gx-mb-0">{props.selectedUser.data.name}</p>
        </div>
      </div>
      <div className="gx-media gx-align-items-center gx-flex-nowrap gx-pro-contact-list">
        <div className="gx-mr-3">
          <Checkbox defaultChecked disabled>
            Allow Void
          </Checkbox>
        </div>
        <div className="gx-mr-3">
          <Checkbox defaultChecked disabled>
            Allow Refund
          </Checkbox>
        </div>
      </div>
    </Widget>
  );
};
const mapStateToProps = ({ user }) => {
  const { selectedUser } = user;
  return { selectedUser };
};

export default connect(mapStateToProps)(Contact);
