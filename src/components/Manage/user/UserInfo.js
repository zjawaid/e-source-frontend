import React, { Component } from "react";
import Widget from "components/Widget/index";
import UserTable from "./UsersTable";
import { Divider, Icon, Card } from "antd";
import EditProfile from "./EditProfile";
import { connect } from "react-redux";
class UserInfo extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let data = this.props.userData;

    return (
      <Widget>
        <UserTable data={data} />
        <EditProfile />
      </Widget>
    );
  }
}
const mapStateToProps = ({ user }) => {
  const { userData } = user;
  return { userData };
};
export default connect(mapStateToProps)(UserInfo);
