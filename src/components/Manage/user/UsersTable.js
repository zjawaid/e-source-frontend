import { Table, Divider, Tag, Icon, Modal } from "antd";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { editUser, deleteUser } from "../../../appRedux/actions/User";

class UserTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      confirmLoading: false,
      visible: false
    };
  }

  editProfile = (id, data) => {
    this.props.editUser(id, data);
  };

  render() {
    const columns = [
      {
        title: "Name",
        dataIndex: "name",
        key: "name",
        render: text => <a>{text}</a>
      },
      {
        title: "Agency",
        dataIndex: "agency",
        key: "airline",
        render: text => <a>{text}</a>
      },
      {
        title: "Login Email",
        dataIndex: "loginEmail",
        key: "loginEmail"
      },
      {
        title: "Status",
        dataIndex: "status",
        key: "status"
      },

      {
        title: "Action",
        key: "action",
        render: (text, record) => (
          <span>
            {console.log("column text", text, "Column record", record)}
            {this.props.data ? (
              <div>
                <Icon
                  type="edit"
                  onClick={() => this.editProfile(record.key, record)}
                />
                <Divider type="vertical" />
                <Link to="/custom-views/user-auth/reset-password">
                  {" "}
                  <Icon type="lock" theme="filled" />
                </Link>
              </div>
            ) : null}
          </span>
        )
      }
    ];
    const { confirmLoading, visible } = this.state;
    return (
      <div>
        <Table
          pagination={false}
          size="default"
          className="gx-table-responsive"
          columns={columns}
          dataSource={this.props.data}
        />
      </div>
    );
  }
}

export default connect(null, { editUser, deleteUser })(UserTable);
