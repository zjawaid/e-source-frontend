import React from "react";
import {
  Drawer,
  Form,
  Button,
  Col,
  Row,
  Input,
  Select,
  DatePicker,
  Icon,
  Checkbox
} from "antd";
import ContactInfo from "./ContactCard";
import { connect } from "react-redux";
import { onCancel, onSubmit } from "./../../../appRedux/actions/User";

const { Option } = Select;

class DrawerForm extends React.Component {
  state = { visible: false };

  onClose = () => {
    this.props.onCancel();
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log("values", values);
        console.log("selecteduser", this.props.selectedUser.id);

        this.props.onSubmit(this.props.selectedUser.id, values);
      }
    });
  };

  render() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
      }
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0
        },
        sm: {
          span: 16,
          offset: 8
        }
      }
    };
    const { getFieldDecorator } = this.props.form;
    const { showEditWindow } = this.props;
    // let userName = selectedUser.data.name;
    //console.log("selected user", userName);
    return (
      <div>
        <Drawer
          title="Edit account"
          width={1200}
          onClose={this.onClose}
          visible={showEditWindow}
          bodyStyle={{ paddingBottom: 80 }}
        >
          <ContactInfo />
          <Form
            {...formItemLayout}
            hideRequiredMark
            onSubmit={this.handleSubmit}
          >
            <Form.Item label="Name">
              {getFieldDecorator("name", {
                initialValue: this.props.selectedUser
                  ? this.props.selectedUser.data.name
                  : "userName",
                rules: [{ required: true, message: "please enter name" }]
              })(<Input placeholder="Please Enter name" />)}
            </Form.Item>

            <Form.Item label="Role">
              {getFieldDecorator("role", {
                rules: [{ required: true, message: "Please choose the type" }]
              })(
                <Select
                  style={{ width: "200px" }}
                  placeholder="please select user role"
                >
                  <Option value="private">Private</Option>
                  <Option value="public">Public</Option>
                </Select>
              )}
            </Form.Item>

            <div
              style={{
                position: "absolute",
                right: 0,
                bottom: 0,
                width: "100%",
                borderTop: "1px solid #e9e9e9",
                padding: "10px 16px",
                background: "#fff",
                textAlign: "right"
              }}
            >
              <Button onClick={this.onClose} style={{ marginRight: 8 }}>
                Cancel
              </Button>
              <Button type="primary" htmlType="submit">
                Update
              </Button>
            </div>
          </Form>
        </Drawer>
      </div>
    );
  }
}

const editUser = Form.create()(DrawerForm);

const mapStateToProps = ({ user }) => {
  const { selectedUser, showEditWindow } = user;
  return { selectedUser, showEditWindow };
};

export default connect(mapStateToProps, { onCancel, onSubmit })(editUser);
