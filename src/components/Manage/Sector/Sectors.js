import React from "react";
import Widget from "components/Widget/index";
import { Icon, Divider, Table } from "antd";
const columns = [
  {
    title: "Sectors",
    dataIndex: "sectors",
    ellipises: true
  },
  {
    title: "Action",
    dataIndex: "action",

    render: () => {
      return (
        <span>
          <Icon type="edit" />
          <Divider type="vertical" />
          <Icon type="delete" />
        </span>
      );
    }
  }
];
const data = [];
const Sectors = () => {
  return (
    <Widget title="Sectors">
      <Table
        columns={columns}
        dataSource={data}
        className="gx-table-no-bordered"
      />
    </Widget>
  );
};
export default Sectors;
