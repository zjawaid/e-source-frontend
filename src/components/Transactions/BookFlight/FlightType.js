import React, { Component } from "react";
import { Input, Card, Col, Checkbox, Row } from "antd";

function onChange(checkedValues) {
  console.log("Flight Type = ", checkedValues);
}
class FlightTypes extends Component {
  render() {
    return (
      <div>
        <Card className="gx-card" title="Flight Types">
          <Row gutter={[8, 8]}>
            <Checkbox.Group style={{ width: "100%" }} onChange={onChange}>
              <Col xs={16} sm={16} md={16} lg={16}>
                <Checkbox value="Business">BUSINESS</Checkbox>
              </Col>
              <Col xs={16} sm={16} md={16} lg={16}>
                <Checkbox value="Economy">ECONOMY</Checkbox>
              </Col>
              <Col xs={16} sm={16} md={16} lg={16}>
                <Checkbox value="First-class">FIRST CLASS</Checkbox>
              </Col>
            </Checkbox.Group>
          </Row>
        </Card>
      </div>
    );
  }
}

export default FlightTypes;
