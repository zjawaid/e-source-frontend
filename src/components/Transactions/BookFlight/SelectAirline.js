import React, { Component } from "react";
import { Card, Col, Row, Checkbox } from "antd";

function onChange(checkedValues) {
  console.log("Airline Selection = ", checkedValues);
}
class SelectAirline extends Component {
  render() {
    return (
      <div>
        <Card className="gx-card" title="Airlines">
          <Row gutter={[8, 8]}>
            <Checkbox.Group style={{ width: "100%" }} onChange={onChange}>
              <Col xs={16} sm={16} md={16} lg={16}>
                <Checkbox value="LUFTHANSA">LUFTHANSA</Checkbox>
              </Col>
              <Col xs={16} sm={16} md={16} lg={16}>
                <Checkbox value="UNITED AIRLINES">UNITED AIRLINES</Checkbox>
              </Col>
              <Col xs={16} sm={16} md={16} lg={16}>
                <Checkbox value="AIR BERLIN">AIR BERLIN</Checkbox>
              </Col>
              <Col xs={16} sm={16} md={16} lg={16}>
                <Checkbox value="AIR FRANCE">AIR FRANCE</Checkbox>
              </Col>
            </Checkbox.Group>
          </Row>
        </Card>
      </div>
    );
  }
}

export default SelectAirline;
