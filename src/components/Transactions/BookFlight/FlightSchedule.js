import React, { Component } from "react";
import {
  Input,
  Button,
  Col,
  Row,
  DatePicker,
  Radio,
  AutoComplete,
  Form,
  Select,
  Card
} from "antd";
import { connect } from "react-redux";
import { BOOK_FLIGHT_FORM } from "../../../appRedux/actions/bookFlight";
import { airportDescriptions } from "./airports";
const { RangePicker } = DatePicker;
const { Option } = Select;
class FlightSchedule extends Component {
  constructor() {
    super();
    this.state = {
      roundTrip: false,
      value: "one-way"
    };
  }
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      this.props.onFormSubmit(values);
    });
  };
  onChange = e => {
    const values = e.target.value;
    values === "one-way"
      ? this.setState({
          roundTrip: false
        })
      : this.setState({
          roundTrip: true
        });
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    const { roundTrip } = this.state;
    return (
      <div>
        <Card className="gx-card">
          <Form
            onSubmit={this.handleSubmit.bind(this)}
            style={{ margin: "10px" }}
          >
            <Row gutter={[8, 16]}>
              <Col xs={24} sm={24} md={24} lg={24}>
                {getFieldDecorator("trip", {
                  initialValue: "one-way",
                  rules: [
                    {
                      required: true,
                      message: "Please select cabin!"
                    }
                  ]
                })(
                  <Radio.Group onChange={this.onChange}>
                    <Radio value="one-way">One-way</Radio>
                    <Radio value="Round-trip">Round-Trip</Radio>
                  </Radio.Group>
                )}
              </Col>
              <Col xs={24} sm={12} md={12} lg={12}>
                {getFieldDecorator("from", {
                  rules: [
                    {
                      required: true,
                      message: "Please select your start designation!"
                    }
                  ]
                })(
                  <AutoComplete
                    style={{ width: "100%" }}
                    dataSource={airportDescriptions}
                    placeholder="from"
                    filterOption={(inputValue, option) =>
                      option.props.children
                        .toUpperCase()
                        .indexOf(inputValue.toUpperCase()) !== -1
                    }
                  />
                )}
              </Col>

              <Col xs={24} sm={12} md={12} lg={12}>
                {getFieldDecorator("to", {
                  rules: [
                    {
                      required: true,
                      message: "Please select your end designation!"
                    }
                  ]
                })(
                  <AutoComplete
                    style={{ width: "100%" }}
                    dataSource={airportDescriptions}
                    placeholder="to"
                    filterOption={(inputValue, option) =>
                      option.props.children
                        .toUpperCase()
                        .indexOf(inputValue.toUpperCase()) !== -1
                    }
                  />
                )}
              </Col>

              {roundTrip === false ? (
                <Col xs={24} sm={24} md={24} lg={24}>
                  {getFieldDecorator("schedule", {
                    rules: [
                      {
                        required: true,
                        message: "Please select your schedule!"
                      }
                    ]
                  })(
                    <DatePicker
                      style={{ width: "100%" }}
                      placeholder="Departure Date"
                    />
                  )}
                </Col>
              ) : (
                <Col xs={24} sm={24} md={24} lg={24}>
                  {/* <label>Select schedule</label> */}
                  {getFieldDecorator("schedule", {
                    rules: [
                      {
                        required: true,
                        message: "Please select your schedule!"
                      }
                    ]
                  })(
                    <RangePicker
                      style={{ width: "100%" }}
                      placeholder={["Departure Date", "Return Date"]}
                    />
                  )}
                </Col>
              )}
              <Col xs={24} sm={12} md={12} lg={12}>
                {/* <label>Adults</label> */}
                {getFieldDecorator("adults", {
                  rules: [
                    {
                      required: true,
                      message: "Please select your end designation!"
                    }
                  ]
                })(
                  <Input type="number" placeholder="Adults" min="0" max="9" />
                )}
              </Col>
              <Col xs={24} sm={12} md={12} lg={12}>
                {/* <label>Child</label> */}
                {getFieldDecorator("child", {
                  rules: [
                    {
                      required: true,
                      message: "Please select cabin!"
                    }
                  ]
                })(<Input type="number" placeholder="Child" min="0" max="9" />)}
              </Col>
              <Col xs={24} sm={12} md={12} lg={12}>
                {/* <label>Infant</label> */}
                {getFieldDecorator("infant", {
                  rules: [
                    {
                      required: true,
                      message: "Please select cabin!"
                    }
                  ]
                })(
                  <Input type="number" placeholder="Infant" min="0" max="9" />
                )}
              </Col>
              <Col xs={24} sm={12} md={12} lg={12}>
                {getFieldDecorator("class", {
                  rules: [
                    {
                      required: true,
                      message: "Please select cabin!"
                    }
                  ]
                })(
                  <Select
                    showSearch
                    style={{ width: "100%" }}
                    placeholder="Class"
                  >
                    <Option value="Business-class">Business</Option>
                    <Option value="economy-class">Economy</Option>
                    <Option value="first-class">First</Option>
                  </Select>
                )}
              </Col>
              <Col span={24}>
                <Button type="primary" htmlType="submit" block>
                  Search
                </Button>
              </Col>
            </Row>
          </Form>
        </Card>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onFormSubmit: data => {
      dispatch(BOOK_FLIGHT_FORM(data));
    }
  };
};

const WrappedNormalLoginForm = Form.create()(FlightSchedule);

export default connect(null, mapDispatchToProps)(WrappedNormalLoginForm);
