import React, { Component } from "react";
import { Card, Slider } from "antd";

class FlightTimmings extends Component {
  render() {
    return (
      <div>
        <Card className="gx-card" title="Flight Times">
          <div className="deptTimeHeading">DEPARTURE FLIGHT</div>
          <div className="DepartureTime">
            <label>DEPARTURE TIME 3:00 UP TO 20:00</label>
            <Slider range defaultValue={[20, 80]} />
            <label>ARRIVAL TIME 3:00 UP TO 20:00</label>
            <Slider range defaultValue={[20, 80]} />
          </div>
          <div className="deptTimeHeading">RETURN FLIGHT</div>
          <div className="ArrivalTime">
            <label>DEPARTURE TIME 3:00 UP TO 20:00</label>
            <Slider range defaultValue={[20, 80]} />
            <label>ARRIVAL TIME 3:00 UP TO 20:00</label>
            <Slider range defaultValue={[20, 80]} />
          </div>
        </Card>
      </div>
    );
  }
}

export default FlightTimmings;
