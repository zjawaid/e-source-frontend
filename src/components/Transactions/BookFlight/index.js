import React, { Component } from "react";
import { Col, Row } from "antd";
import Widget from "../../Widget/index";
import FlightSchedule from "./FlightSchedule";
import FlightsPriceRange from "./FlightPriceRange";
import SelectAirline from "./SelectAirline";
import FlightTypes from "./FlightType";
import FlightTimmings from "./FlightTimmings";
import AvailableFlights from "./AvailableFlight";
import FilterResults from "./FlightFilter";
import Auxiliary from "util/Auxiliary";
import "./flights.scss";

class BookFlightComponent extends Component {
  render() {
    return (
      <div className="">
        <Auxiliary>
          <Row gutter={[8, 8]}>
            <Col xs={24} sm={24} md={8} lg={7} span={7}>
              <div className="sideDiv">
                <div className="searchResult">
                  <Widget>
                    <div className="searchText">2,435 RESULTS FOUND.</div>
                  </Widget>
                </div>
                <div className="FlightsSearchDiv">
                  <FlightSchedule />
                </div>
                <div className="priceRangeDiv">
                  <FlightsPriceRange />
                </div>
                <div className="AirlinesOptions">
                  <SelectAirline />
                </div>
                <div className="FlightTypeDiv">
                  <FlightTypes />
                </div>
                <div className="flightTimeDiv">
                  <FlightTimmings />
                </div>
              </div>
            </Col>
            <Col xs={24} sm={24} md={16} lg={17} span={17}>
              <div className="rightDiv">
                <div className="filterResult">
                  <FilterResults />
                </div>
                <div className="ticketMainInform">
                  <AvailableFlights />
                </div>
              </div>
            </Col>
          </Row>
        </Auxiliary>
      </div>
    );
  }
}

export default BookFlightComponent;
