import React, { Component } from "react";
import { connect } from "react-redux";
import Widget from "../../Widget/index";
import { Col, Row } from "antd";
import Logo from "./../../../assets/images/fl-transp-01.png";
import dept_icon from "./../../../assets/images/alt-depart.png";

class AvailableFlights extends Component {
  render() {
    return (
      <div>
        {this.props.showSearchFlightsResponse === false ? null : (
          <Widget>
            <Row className="ticketDetail">
              <Col
                xl={18}
                lg={24}
                md={24}
                sm={24}
                xs={24}
                className="ticketInfo"
              >
                <div className="ticketLogo">
                  <a>
                    <img src={Logo} alt=" ticket logo" />
                  </a>
                </div>
                <div className="ticketDetInfo">
                  <div className="tripInfo">
                    <h2>NEW YORK - VIENNA</h2>
                    <p>
                      <span className="schedules">06.01.15 </span>
                      <span className="tripWays">ONE WAY TRIP</span>
                    </p>
                  </div>
                  <div className="departureDetails">
                    <div className="totalDepInfo">
                      <div className="departureMainWrp">
                        <div className="alt-data-i alt-departure">
                          <b>Departure</b>
                          <span>14:12</span>
                        </div>
                        <div className="alt-data-i alt-arrival">
                          <b>Arrive</b>
                          <span>14:12</span>
                        </div>
                        <div className="alt-data-i alt-time">
                          <b>Time</b>
                          <span>14:12</span>
                        </div>
                      </div>
                      <div className="seats">
                        <b>details</b>
                        <span>Only 2 seats!</span>
                      </div>
                    </div>
                  </div>
                </div>
              </Col>
              <Col
                xl={6}
                lg={24}
                md={24}
                sm={24}
                xs={24}
                className="ticketPrice"
              >
                <div className="flt-i-price">634.24$</div>
                <div className="flt-i-price-b">avg/person</div>
                <a className="cat-list-btn" href="#">
                  select now
                </a>
              </Col>
            </Row>
          </Widget>
        )}
      </div>
    );
  }
}

const mapStateToProps = ({ bookFlight }) => {
  const { showSearchFlightsResponse, BookfligtFilterData } = bookFlight;
  return { showSearchFlightsResponse, BookfligtFilterData };
};

export default connect(mapStateToProps, null)(AvailableFlights);
