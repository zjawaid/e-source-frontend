import React, { Component } from "react";
import { connect } from "react-redux";
import Widget from "components/Widget/index";
import { Input, Card, Col, DatePicker, Select, Button, Row, Form } from "antd";
import { SEARCH_FILTER } from "../../../appRedux/actions/bookFlight";
import "./flights.scss";
const { Option } = Select;
class FilterResults extends Component {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.onFormSubmit(values);
      }
    });
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        {/* <Card className="gx-card"> */}
        <Form onSubmit={this.handleSubmit}>
          <Row gutter={[8, 16]}>
            <label>SORT RESULTS BY:</label>
            <Col xs={24} sm={24} md={6} lg={8}>
              {getFieldDecorator("flights", {
                rules: [
                  { required: true, message: "Select Flight atleast one!" }
                ]
              })(
                <Select
                  showSearch
                  style={{ width: "100%" }}
                  placeholder="Flight"
                  optionFilterProp="children"
                >
                  <Option value="PIA">PIA</Option>
                  <Option value="Emirates">Emirates</Option>
                  <Option value="Shaheen">Shaheen</Option>
                  <Option value="AirBlue">Air Blue</Option>
                </Select>
              )}
            </Col>
            <Col xs={24} sm={24} md={6} lg={8}>
              {getFieldDecorator("class", {
                rules: [
                  {
                    required: true,
                    message: "Please select atleast one class !"
                  }
                ]
              })(
                <Select
                  showSearch
                  style={{ width: "100%" }}
                  placeholder="Class"
                  optionFilterProp="children"
                >
                  <Option value="Business">Business</Option>
                  <Option value="First">First</Option>
                  <Option value="Economy">Economy</Option>
                </Select>
              )}
            </Col>
            <Col xs={24} sm={24} md={2} lg={2}>
              <Button type="primary" htmlType="submit">
                Search
              </Button>
            </Col>
          </Row>
        </Form>
        {/* </Card> */}
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onFormSubmit: data => {
      dispatch(SEARCH_FILTER(data));
    }
  };
};

const WrappedHorizontalLoginForm = Form.create()(FilterResults);
export default connect(null, mapDispatchToProps)(WrappedHorizontalLoginForm);
