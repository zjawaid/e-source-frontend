import React, { Component } from "react";
import { Card, Slider } from "antd";
import Widget from "../../Widget/index";

class FlightsPriceRange extends Component {
  render() {
    let testing;
    function onChange(value) {
      console.log("onChange: ", value);
      testing = value;
      console.log(testing[0]);
    }
    function onAfterChange(value) {
      console.log("onAfterChange: ", value);
    }
    return (
      <div>
        <Card className="gx-card" title="Price">
          <Slider
            range
            step={10}
            defaultValue={[20, 50]}
            onChange={onChange}
            onAfterChange={onAfterChange}
          />
          <div className="priceRange">
            <div>10$</div>
            <div>275$</div>
          </div>
        </Card>
      </div>
    );
  }
}

export default FlightsPriceRange;
