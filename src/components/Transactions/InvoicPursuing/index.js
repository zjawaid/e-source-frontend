import React, { Component } from "react";
import { Col, Row, Table, Button } from "antd";
import { connect } from "react-redux";
import Auxiliary from "util/Auxiliary";
import { INVOICE_PURSUING_DATA_FETCH_DATA } from "../../../appRedux/actions/InvoicePursuing";
import "./style.scss";
import InvoicePursuingData from "./InvoicePursuingData";

class InvoicePursuingComponent extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount = () => {
    this.props.onLoadComponent();
  };
  render() {
    console.log(this.props);
    return (
      <div>
        <Auxiliary>
          <Row gutter={[8, 16]}>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <div
                style={{
                  backgroundColor: "#eaeaea",
                  padding: "10px"
                }}
              >
                <div style={{ fontWeight: "500", fontSize: "20px" }}>
                  Invoice Pursuing
                </div>
                <p>List of invoice which is not sent to the back office</p>
              </div>
            </Col>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <InvoicePursuingData data={this.props.InvoicePursuingResponse} />
            </Col>
          </Row>
        </Auxiliary>
      </div>
    );
  }
}

const mapStateToProps = ({ invoicePursuing }) => {
  const { InvoicePursuingResponse } = invoicePursuing;
  return { InvoicePursuingResponse };
};

const mapDispatchToProps = dispatch => {
  return {
    onLoadComponent: () => {
      dispatch(INVOICE_PURSUING_DATA_FETCH_DATA());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InvoicePursuingComponent);
