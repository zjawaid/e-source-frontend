import React, { Component } from "react";
import { Col, Row, Table, Button } from "antd";
import { connect } from "react-redux";
import Auxiliary from "util/Auxiliary";
import { INVOICE_PURSUING_ACTION_DELETE } from "../../../appRedux/actions/InvoicePursuing";

import "./style.scss";

class InvoicePursuingData extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {};
  // }
  rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      console.log(
        `selectedRowKeys: ${selectedRowKeys}`,
        "selectedRows: ",
        selectedRows
      );
    },
    onSelect: (record, selected, selectedRows) => {
      console.log(record, selected, selectedRows);
      this.props.onDeleteAction(selectedRows);
    },
    onSelectAll: (selected, selectedRows, changeRows) => {
      console.log(selected, selectedRows, changeRows);
    }
  };
  render() {
    console.log(this.props);
    const columns = [
      {
        title: "REF TYPE",
        dataIndex: "ref_type",
        key: "ref_type",
        render: text => <a>{text}</a>
      },
      {
        title: "PNR",
        dataIndex: "pnr",
        key: "pnr"
      },
      {
        title: "REF NO.",
        dataIndex: "ref_no",
        key: "ref_no"
      },
      {
        title: "CREATION DATE",
        dataIndex: "CREATION_DATE",
        key: "CREATION_DATE"
      },
      {
        title: "TOTAL BOOKINGS",
        dataIndex: "TOTAL_BOOKINGS",
        key: "TOTAL_BOOKINGS"
      },
      {
        title: "BOOKINGS TOTAL",
        dataIndex: "BOOKINGS_TOTAL",
        key: "BOOKINGS_TOTAL"
      },
      {
        title: "AGENCY",
        dataIndex: "AGENCY",
        key: "AGENCY"
      },
      {
        title: "DESCRIPTION",
        dataIndex: "DESCRIPTION",
        key: "DESCRIPTION"
      }
    ];
    return (
      <div>
        <Auxiliary>
          <Row gutter={[8, 16]}>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <Table
                columns={columns}
                rowSelection={this.rowSelection}
                dataSource={this.props.data}
              />
              <Row type="flex" justify="space-between">
                <Button type="danger">DELETE</Button>
                <Button type="primary">RESEND</Button>
              </Row>
            </Col>
          </Row>
        </Auxiliary>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onDeleteAction: data => {
      dispatch(INVOICE_PURSUING_ACTION_DELETE(data));
    }
  };
};

export default connect(null, mapDispatchToProps)(InvoicePursuingData);
