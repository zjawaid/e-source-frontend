import React, { Component } from "react";
import { Col, Row, Input, Table, DatePicker, Button, Icon, Form } from "antd";
import Widget from "../../Widget/index";

import { connect } from "react-redux";
import { MY_PNR_FORM_DATA } from "../../../appRedux/actions/MyPnrs";
function onChange(date, dateString) {
  console.log(date, dateString);
}
class FilterData extends Component {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);
        this.props.onFormSubmit(values);
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Widget>
          <Form onSubmit={this.handleSubmit}>
            <Row gutter={[8, 24]}>
              {/* <Col span={24}>
                <div
                  style={{
                    backgroundColor: "#eaeaea",
                    padding: "10px",
                    fontWeight: "500"
                  }}
                >
                  Filter
                </div>
              </Col> */}
              <Col xs={12} sm={12} md={4} lg={6}>
                {/* <label htmlFor="">From Date</label> */}
                <br />
                {getFieldDecorator("from_date", {
                  rules: [
                    { required: true, message: "Please input your username!" }
                  ]
                })(
                  <DatePicker
                    placeholder="From date"
                    style={{ width: "100%" }}
                    onChange={onChange}
                  />
                )}
              </Col>
              <Col xs={12} sm={12} md={4} lg={6}>
                {/* <label htmlFor="">To Date</label> */}
                <br />
                {getFieldDecorator("to_date", {
                  rules: [
                    { required: true, message: "Please input your username!" }
                  ]
                })(
                  <DatePicker
                    placeholder="To Date"
                    style={{ width: "100%" }}
                    onChange={onChange}
                  />
                )}
              </Col>
              <Col xs={24} sm={24} md={4} lg={6}>
                {/* <label htmlFor="">PNR</label> */}
                <br />
                {getFieldDecorator("pnr", {
                  rules: [
                    { required: true, message: "Please input your username!" }
                  ]
                })(<Input placeholder="PNR" />)}
              </Col>
              <Col xs={24} sm={24} md={4} lg={6}>
                <br />
                <Button htmlType="submit" type="primary">
                  <Icon type="search" /> Search
                </Button>
              </Col>
            </Row>
          </Form>
        </Widget>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onFormSubmit: data => {
      dispatch(MY_PNR_FORM_DATA(data));
    }
  };
};

const myPrnFormData = Form.create()(FilterData);
export default connect(null, mapDispatchToProps)(myPrnFormData);
