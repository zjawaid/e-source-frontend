import React, { Component, useState } from "react";
import { Col, Row, Input, Table, DatePicker, Button, Icon } from "antd";
import { connect } from "react-redux";
import FilterData from "./FilterData";
import Pnrdata from "./PnrData";
class MyPnrComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      SampleData: [
        {
          key: "1",
          date: new Date().toISOString().slice(0, 10),
          pnr: "LM21BG",
          contract: "V8V-UATAG2",
          sector: "BWN-SIN",
          TOTAL_FARE: "2860",
          PNR_STATUS: "Created"
        },
        {
          key: "2",
          date: new Date().toISOString().slice(0, 10),
          pnr: "LM21BG",
          contract: "V8V-UATAG2",
          sector: "BWN-SIN",
          TOTAL_FARE: "2860",
          PNR_STATUS: "Created"
        }
      ]
    };
  }
  render() {
    const selectedPnrForm =
      this.props.showUserData === true && this.props.myPnrData
        ? this.props.myPnrData
        : this.state.SampleData;
    return (
      <div>
        <Row gutter={[8, 24]}>
          <Col span={24}>
            <FilterData />
          </Col>
          <Col span={24}>
            <Pnrdata selectedDatePnrData={selectedPnrForm} />
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ myPnr }) => {
  const { showUserData, myPnrData } = myPnr;
  return { showUserData, myPnrData };
};

export default connect(mapStateToProps)(MyPnrComponent);
