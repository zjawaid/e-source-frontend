import React, { Component } from "react";
import { Col, Row, Input, Table, DatePicker, Button, Icon } from "antd";
import Widget from "../../Widget/index";

function onChange(date, dateString) {
  console.log(date, dateString);
}
class Pnrdata extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const sampleData = this.props.selectedDatePnrData;
    const columns = [
      {
        title: "DATE",
        dataIndex: "date",
        key: "date",
        render: text => <a>{text}</a>
      },
      {
        title: "PNR",
        dataIndex: "pnr",
        key: "pnr"
      },
      {
        title: "CONTRACT",
        dataIndex: "contract",
        key: "contract"
      },
      {
        title: "SECTOR",
        dataIndex: "sector",
        key: "sector"
      },
      {
        title: "TOTAL FARE",
        dataIndex: "TOTAL_FARE",
        key: "TOTAL_FARE"
      },
      {
        title: "PNR_STATUS",
        dataIndex: "PNR_STATUS",
        key: "PNR_STATUS"
      },
      {
        title: "ACTION",
        key: "action",
        render: (text, record) => (
          <span>
            <Button type="primary">ISSUE TICKET</Button>
            <Button type="danger">CANCEL</Button>
          </span>
        )
      }
    ];
    return (
      <div>
        <Widget>
          <Row gutter={[8, 24]}>
            <Col span={24}>
              <Table columns={columns} dataSource={sampleData} />
            </Col>
          </Row>
        </Widget>
      </div>
    );
  }
}

export default Pnrdata;
