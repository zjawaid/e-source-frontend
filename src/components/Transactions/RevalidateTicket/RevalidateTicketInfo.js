import React, { Component } from "react";
import {
  Col,
  Row,
  Input,
  Card,
  Checkbox,
  Form,
  Button,
  DatePicker
} from "antd";
import Auxiliary from "util/Auxiliary";
import { connect } from "react-redux";
import {
  REVALIDATE_POST_DATA,
  SET_FlAG
} from "../../../appRedux/actions/RevalidateTicket";
import "./style.scss";
function onChange(date, dateString) {
  console.log(date, dateString);
}

class RevalidateTicketInfo extends Component {
  componentDidMount() {
    console.log("called component");
    this.props.setFag();
  }
  constructor() {
    super();
  }
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const payLoad = {
          revalidateTicketData: values,
          RevalidateFormData: this.props.revalidateDetailsData
        };
        this.props.onFormSubmit(payLoad);
      }
    });
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Auxiliary>
          {this.props.showUserData === true ? (
            <Row gutter={[8, 0]}>
              <Col xl={24} lg={24} md={24} sm={24} xs={24}>
                <Card className="gx-card">
                  <div className="TicketDetails">
                    {this.props.revalidateDetailsData.map((data, idx) => (
                      <div key={idx}>
                        <div>
                          <span>PNR: </span> {data.PNR}
                        </div>
                        <div>
                          <span>TKT No: </span> {data.TKT_NO}
                        </div>
                        <div>
                          <span>Passenger: </span> {data.Passenger}
                        </div>
                        <div>
                          <span>Base Fare: </span> {data.BASE_FARE}
                        </div>
                        <div>
                          <span>Taxes: </span> {data.TAXES}
                        </div>
                        <div>
                          <span>Other Charges: </span> {data.OTHER_CHARGES}
                        </div>
                        <div>
                          <span>PSF/(COMM): </span> {data.PSF_COMM}
                        </div>
                        <div>
                          <span>Total Fare: </span> {data.TOTAL_FARE}
                        </div>
                      </div>
                    ))}
                  </div>
                </Card>
              </Col>
              <Col xl={24} lg={24} md={24} sm={24} xs={24}>
                <Card className="gx-card">
                  <Form onSubmit={this.handleSubmit} className="login-form">
                    <Row>
                      <Col xl={6} lg={12} md={12} sm={24} xs={24}>
                        <label>Segment</label>
                        {getFieldDecorator("segment", {
                          rules: [
                            {
                              required: true,
                              message: "Please input your username!"
                            }
                          ]
                        })(<Input placeholder="Segment.." />)}
                      </Col>
                      <Col xl={6} lg={12} md={12} sm={24} xs={24}>
                        <label>NVB</label>
                        {getFieldDecorator("nvb", {
                          rules: [
                            {
                              required: true,
                              message: "Please input your username!"
                            }
                          ]
                        })(<DatePicker style={{ width: "100%" }} />)}
                      </Col>
                      <Col xl={6} lg={12} md={12} sm={24} xs={24}>
                        <label>Coupon</label>
                        {getFieldDecorator("coupon", {
                          rules: [
                            {
                              required: true,
                              message: "Please input your username!"
                            }
                          ]
                        })(<Input placeholder="Enter Coupon.." />)}
                      </Col>
                      <Col xl={6} lg={12} md={12} sm={24} xs={24}>
                        <label>NVA</label>
                        {getFieldDecorator("nva", {
                          rules: [
                            {
                              required: true,
                              message: "Please input your username!"
                            }
                          ]
                        })(<DatePicker style={{ width: "100%" }} />)}
                      </Col>
                      <Col xl={6} lg={12} md={12} sm={12} xs={24}>
                        <br />
                        {getFieldDecorator("passenger_infant", {
                          valuePropName: "checked",
                          initialValue: false
                        })(
                          <Checkbox onChange={onChange}>
                            Mark if passenger is infant
                          </Checkbox>
                        )}
                      </Col>
                    </Row>
                    <Row type="flex" justify="end" gutter={[8, 0]}>
                      <Col>
                        <Button htmlType="submit" type="primary">
                          RE-VALIDATE TICKET
                        </Button>
                      </Col>
                    </Row>
                  </Form>
                </Card>
              </Col>
            </Row>
          ) : null}
        </Auxiliary>
      </div>
    );
  }
}

const mapStateToProps = ({ revalidateTicket }) => {
  const { revalidateDetailsData, showUserData } = revalidateTicket;
  return { revalidateDetailsData, showUserData };
};

const mapDispatchToProps = dispatch => {
  return {
    onFormSubmit: (formData, ticketData) => {
      dispatch(REVALIDATE_POST_DATA(formData, ticketData));
    },
    setFag: () => {
      dispatch(SET_FlAG());
    }
  };
};

const WrappedNormalLoginForm = Form.create({ name: "normal_login" })(
  RevalidateTicketInfo
);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WrappedNormalLoginForm);
