import React, { Component } from "react";
import { Col, Row, Input, Card, Checkbox, Button } from "antd";
import Auxiliary from "util/Auxiliary";
import RevalidateTicketForm from "./RevalidateForm";
import RevalidateTicketInfo from "./RevalidateTicketInfo";
function onChange(date, dateString) {
  console.log(date, dateString);
}
class RevalidateTicketComponent extends Component {
  render() {
    return (
      <div>
        <Auxiliary>
          <Row gutter={[8, 24]}>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <div
                style={{
                  backgroundColor: "#eaeaea",
                  padding: "10px"
                }}
              >
                <div style={{ fontWeight: "400", fontSize: "20px" }}>
                  Revalidate Ticket
                </div>
                <p>Fill the following form to revalid the ticket.</p>
              </div>
            </Col>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <RevalidateTicketForm />
            </Col>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <RevalidateTicketInfo />
            </Col>
          </Row>
        </Auxiliary>
      </div>
    );
  }
}

export default RevalidateTicketComponent;
