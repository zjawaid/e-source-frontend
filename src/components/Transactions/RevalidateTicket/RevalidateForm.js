import React, { Component } from "react";
import { Col, Row, Input, Card, Form, Checkbox, Button } from "antd";
import Auxiliary from "util/Auxiliary";
import { connect } from "react-redux";
import { REVALIDATE_SUBMIT_FORM } from "../../../appRedux/actions/RevalidateTicket";
function onChange(date, dateString) {
  console.log(date, dateString);
}
class RevalidateTicketForm extends Component {
  constructor() {
    super();
  }
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);
        this.props.onFormSubmit(values);
      }
    });
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Auxiliary>
          <Form
            layout="inline"
            onSubmit={this.handleSubmit}
            className="login-form"
          >
            <Row gutter={[0, 0]}>
              <Form.Item>
                <Col xl={24} lg={24} md={24} sm={24} xs={24}>
                  <label>Ticket Number</label>
                  {getFieldDecorator("ticket_no", {
                    rules: [
                      {
                        required: true,
                        message: `Please input your ticket number!`
                      }
                    ]
                  })(<Input placeholder="Enter Ticket Number.." />)}
                </Col>
              </Form.Item>
              <Form.Item>
                <Col xl={24} lg={24} md={24} sm={24} xs={24}>
                  <br />
                  <Button htmlType="submit" type="primary">
                    RETRIEVE
                  </Button>
                </Col>
              </Form.Item>
              <Form.Item>
                <Col xl={24} lg={24} md={24} sm={24} xs={24}>
                  <br />
                  {getFieldDecorator("conjuction_ticket", {
                    valuePropName: "checked",
                    initialValue: true
                  })(
                    <Checkbox onChange={onChange}>
                      Mark if Conjunction Ticket
                    </Checkbox>
                  )}
                </Col>
              </Form.Item>
            </Row>
          </Form>
        </Auxiliary>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onFormSubmit: data => {
      dispatch(REVALIDATE_SUBMIT_FORM(data));
    }
  };
};

const WrappedNormalLoginForm = Form.create({ name: "normal_login" })(
  RevalidateTicketForm
);
export default connect(null, mapDispatchToProps)(WrappedNormalLoginForm);
