import React, { Component } from "react";
import { Col, Row, Input, Table, Button, Select } from "antd";
import { connect } from "react-redux";
import PassengerDetails from "./PassengerDetail";
import SectorDetails from "./SectorDetail";
import TicketInformation from "./TicketInformation";

class IssueTicketComponent extends Component {
  render() {
    return (
      <div>
        <Row gutter={[8, 24]}>
          <Col xs={24} sm={24} md={24} lg={24}>
            <div
              style={{
                backgroundColor: "#eaeaea",
                padding: "10px"
              }}
            >
              <div style={{ fontWeight: "400", fontSize: "20px" }}>
                ISSUE TICKET
              </div>
            </div>
          </Col>
          <Col xs={24} sm={24} md={12} lg={24}>
            <TicketInformation />
          </Col>
          <Col xs={12} sm={12} md={24} lg={24}>
            <h3>Sector Details</h3>
            <div style={{ borderBottom: "1px solid #cccc" }}></div>
          </Col>
          <Col xs={24} sm={24} md={24} lg={24}>
            <SectorDetails
              showTableData={this.props.showTableData}
              sectorDetailsInformation={this.props.sectorDetailsInformation}
            />
          </Col>
          <Col xs={12} sm={12} md={24} lg={24}>
            <h3>Passenger Details</h3>
            <div style={{ borderBottom: "1px solid #cccc" }}></div>
          </Col>
          <Col xs={24} sm={24} md={24} lg={24}>
            <PassengerDetails
              showTableData={this.props.showTableData}
              passengerDetailInformation={this.props.passengerDetailInformation}
            />
          </Col>
        </Row>
        <Row gutter={[24, 24]} type="flex" justify="end">
          <Col xs={24} sm={24} md={12} lg={8}>
            <div className="issueTicket">
              <div className="heading">Net Amount</div>
              <div className="price">0.00</div>
            </div>
            <div className="issueTicket">
              <div className="heading">Charges and Discount Total</div>
              <div className="price">0.00</div>
            </div>
            <div className="border"></div>
            <div className="issueTicket">
              <div className="heading">Gross total</div>
              <div className="price">0.00</div>
            </div>
            <Button type="primary" block>
              Issue Ticket
            </Button>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ issueticket }) => {
  const {
    passengerDetailInformation,
    sectorDetailsInformation,
    showTableData
  } = issueticket;
  return {
    passengerDetailInformation,
    sectorDetailsInformation,
    showTableData
  };
};

export default connect(mapStateToProps, null)(IssueTicketComponent);
