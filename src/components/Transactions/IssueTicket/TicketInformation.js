import React, { Component } from "react";
import { Col, Row, Input, Table, Button, Select, Form } from "antd";
import { connect } from "react-redux";
import { ISSUE_TICKET_FORM_DATA } from "../../../appRedux/actions/issueTicket";
import Widget from "components/Widget/index";
const { Option } = Select;
function onChange(value) {
  console.log(`selected ${value}`);
}
class TicketInformation extends Component {
  constructor() {
    super();

    this.state = {
      inputLength: 0,
      submitForm: ""
    };
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);
        this.props.onSubmitForm(values);
        this.setState({
          submitFormValues: values
        });
      }
    });
  };

  handleChange = e => {
    this.setState({
      inputLength: e.target.value.length
    });
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Widget>
          <Form layout="inline" onSubmit={this.handleSubmit.bind(this)}>
            <Row gutter={[8, 24]}>
              <Col xs={24} sm={24} md={12} lg={6}>
                <label htmlFor="">PNR</label>
                {getFieldDecorator("pnr", {
                  rules: [
                    { required: true, message: "Please input your username!" }
                  ]
                })(
                  <Input
                    placeholder="Enter PNR number"
                    min="0"
                    maxLength="6"
                    onChange={this.handleChange}
                  />
                )}
              </Col>
              {this.state.inputLength === 6 ? (
                <Col xs={24} sm={24} md={12} lg={6}>
                  <label htmlFor="">WSAP</label>
                  <br />
                  {getFieldDecorator("wsap", {
                    rules: [
                      { required: true, message: "Please input your Password!" }
                    ]
                  })(
                    <Select
                      showSearch
                      style={{ width: "100%" }}
                      placeholder="Select WSAP"
                      optionFilterProp="children"
                      onChange={onChange}
                    >
                      <Option value="5Q1Q">5Q1Q</Option>
                      <Option value="QATAR AIRWAYS">QATAR AIRWAYS</Option>
                      <Option value="Galileo HAP 1">Galileo HAP 1</Option>
                      <Option value="Galileo HAP 2">Galileo HAP 2</Option>
                    </Select>
                  )}
                </Col>
              ) : null}
              {this.state.inputLength === 6 ? (
                <Col xs={24} sm={24} md={12} lg={8}>
                  <br />
                  <Button htmlType="submit" type="primary">
                    Retrive
                  </Button>
                </Col>
              ) : null}
            </Row>
          </Form>
        </Widget>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSubmitForm: ticketFormData => {
      dispatch(ISSUE_TICKET_FORM_DATA(ticketFormData));
    }
  };
};

const WrappedNormalTicketForm = Form.create()(TicketInformation);
export default connect(null, mapDispatchToProps)(WrappedNormalTicketForm);
