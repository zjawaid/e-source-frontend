import React, { Component } from "react";
import { Col, Row, Table } from "antd";
import { connect } from "react-redux";
import Widget from "components/Widget/index";

function onChange(value) {
  console.log(`selected ${value}`);
}

const rowSelection = {
  onChange: (selectedRowKeys, selectedRows) => {
    console.log(
      `selectedRowKeys: ${selectedRowKeys}`,
      "selectedRows: ",
      selectedRows
    );
  },
  onSelect: (record, selected, selectedRows) => {
    console.log(record, selected, selectedRows);
  },
  onSelectAll: (selected, selectedRows, changeRows) => {
    console.log(selected, selectedRows, changeRows);
  }
};

class PassengerDetails extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const passengerDetailt_data = [];
    const passengerDetailt_column = [
      {
        title: "PASSENGER",
        dataIndex: "PASSENGER",
        key: "PASSENGER",
        render: text => <a>{text}</a>
      },
      {
        title: "SEGMENT",
        dataIndex: "SEGMENT",
        key: "SEGMENT"
      },
      {
        title: "CURRENCY",
        dataIndex: "CURRENCY",
        key: "CURRENCY"
      },
      {
        title: "FARE TYPE",
        dataIndex: "FARE_TYPE",
        key: "FARE_TYPE"
      },
      {
        title: "BASE FARE",
        dataIndex: "BASE_FARE",
        key: "BASE_FARE"
      },
      {
        title: "TAXES",
        dataIndex: "TAXES",
        key: "TAXES"
      },
      {
        title: "OTHER CHARGES",
        dataIndex: "OTHER_CHARGES",
        key: "OTHER_CHARGES"
      },
      {
        title: "TOTAL",
        dataIndex: "TOTAL",
        key: "TOTAL"
      },
      {
        title: "PL",
        dataIndex: "PL",
        key: "PL"
      },
      {
        title: "EXC.RATE",
        dataIndex: "EXC_RATE",
        key: "EXC_RATE"
      },
      {
        title: "EQUIV.TOTAL",
        dataIndex: "EQUIV_TOTAL",
        key: "EQUIV_TOTAL"
      }
    ];
    return (
      <div>
        <Widget>
          <Row gutter={[8, 24]}>
            {this.props.showTableData === true ? (
              <Col xs={24} sm={24} md={24} lg={24}>
                <Table
                  columns={passengerDetailt_column}
                  rowSelection={rowSelection}
                  dataSource={this.props.passengerDetailInformation["userData"]}
                />
              </Col>
            ) : (
              <Col xs={24} sm={24} md={24} lg={24}>
                <Table
                  columns={passengerDetailt_column}
                  rowSelection={rowSelection}
                  dataSource={passengerDetailt_data}
                />
              </Col>
            )}
          </Row>
        </Widget>
      </div>
    );
  }
}

// const mapStateToProps = ({ issueticket }) => {
//   const { passengerDetailInformation, showTableData } = issueticket;
//   return { passengerDetailInformation, showTableData };
// };

export default connect(null, null)(PassengerDetails);
