import React, { Component } from "react";
import { Col, Row, Input, Table, Button, Select } from "antd";
import { connect } from "react-redux";
import Widget from "components/Widget/index";

function onChange(value) {
  console.log(`selected ${value}`);
}

class SectorDetails extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const contract_data = [];
    const contract_column = [
      {
        title: "Select",
        dataIndex: "select",
        key: "select",
        render: text => <a>{text}</a>
      },
      {
        title: "Contract Code",
        dataIndex: "contract_code",
        key: "contract_code"
      },
      {
        title: "Limit",
        dataIndex: "limit",
        key: "limit"
      },
      {
        title: "Available Limit",
        dataIndex: "available_limit",
        key: "available_limit"
      },
      {
        title: "Fare Review",
        dataIndex: "fare_review",
        key: "fare_review"
      },
      {
        title: "Currency",
        dataIndex: "currency",
        key: "currency"
      },
      {
        title: "Details",
        dataIndex: "details",
        key: "details"
      }
    ];
    return (
      <div>
        <Widget>
          <Row gutter={[8, 24]}>
            {this.props.showTableData === true ? (
              <Col xs={24} sm={24} md={24} lg={24}>
                <Table
                  columns={contract_column}
                  dataSource={this.props.sectorDetailsInformation["userData"]}
                />
              </Col>
            ) : (
              <Col xs={24} sm={24} md={24} lg={24}>
                <Table columns={contract_column} dataSource={contract_data} />
              </Col>
            )}
          </Row>
        </Widget>
      </div>
    );
  }
}

// const mapStateToProps = ({ issueticket }) => {
//   const { sectorDetailsInformation, showTableData } = issueticket;
//   return { sectorDetailsInformation, showTableData };
// };

export default connect(null, null)(SectorDetails);
