import React, { Component } from "react";
import {
  Col,
  Row,
  Input,
  DatePicker,
  Table,
  Divider,
  Tag,
  Checkbox,
  Button,
  Select
} from "antd";
const { Option } = Select;
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
class ManualReservationComponent extends Component {
  render() {
    const data = [
      {
        key: "1",
        name: "John Brown",
        age: 32,
        address: "New York No. 1 Lake Park",
        tags: ["nice", "developer"]
      },
      {
        key: "2",
        name: "Jim Green",
        age: 42,
        address: "London No. 1 Lake Park",
        tags: ["loser"]
      },
      {
        key: "3",
        name: "Joe Black",
        age: 32,
        address: "Sidney No. 1 Lake Park",
        tags: ["cool", "teacher"]
      }
    ];
    const columns = [
      {
        title: "Select",
        dataIndex: "name",
        key: "name",
        render: text => <a>{text}</a>
      },
      {
        title: "Contract Code",
        dataIndex: "age",
        key: "age"
      },
      {
        title: "Limit",
        dataIndex: "address",
        key: "address"
      },
      {
        title: "Available Limits",
        key: "tags",
        dataIndex: "tags",
        render: tags => (
          <span>
            {tags.map(tag => {
              let color = tag.length > 5 ? "geekblue" : "green";
              if (tag === "loser") {
                color = "volcano";
              }
              return (
                <Tag color={color} key={tag}>
                  {tag.toUpperCase()}
                </Tag>
              );
            })}
          </span>
        )
      },
      {
        title: "Fare Review",
        key: "action",
        render: (text, record) => (
          <span>
            <a>Invite {record.name}</a>
            <Divider type="vertical" />
            <a>Delete</a>
          </span>
        )
      }
    ];
    return (
      <div>
        <Row gutter={[8, 24]}>
          <Col xs={24} sm={24} md={24} lg={24}>
            <div
              style={{
                backgroundColor: "#eaeaea",
                padding: "10px"
              }}
            >
              <div style={{ fontWeight: "400", fontSize: "20px" }}>
                MANUAL RESERVATION
              </div>
            </div>
          </Col>
          <Col xs={24} sm={24} md={24} lg={6}>
            <label htmlFor="">PNR</label>
            <Input placeholder="Enter PNR number" />
          </Col>
          <Col xs={24} sm={24} md={24} lg={6}>
            <label htmlFor="">WSAP</label>
            <br />
            <Select
              showSearch
              style={{ width: "100%" }}
              placeholder="Select WSAP"
              optionFilterProp="children"
            >
              <Option value="jack">Qatar Airways</Option>
              <Option value="lucy">5Q1Q</Option>
              <Option value="tom">Galileo HAP 2</Option>
            </Select>
          </Col>
          <Col xs={24} sm={24} md={24} lg={6}>
            <br />
            <Button type="primary">RETRIEVE</Button>
          </Col>
        </Row>
        <Row gutter={[8, 24]}>
          <Col xs={24} sm={24} md={24} lg={6}>
            <label htmlFor="">Plating Carrier</label>
            <Select
              showSearch
              style={{ width: "100%" }}
              placeholder="Select"
              optionFilterProp="children"
            >
              <Option value="Aeroflot">Aeroflot</Option>
            </Select>
          </Col>
          <Col xs={24} sm={24} md={24} lg={6}>
            <label htmlFor="">Contract</label>
            <Select
              showSearch
              style={{ width: "100%" }}
              placeholder="Select"
              optionFilterProp="children"
            >
              <Option value="Aeroflot">Aeroflot</Option>
            </Select>
          </Col>
          <Col xs={24} sm={24} md={24} lg={6}>
            <label htmlFor="">Vendor Locator</label>
            <Input placeholder="Enter Vendor Locator" />
          </Col>
        </Row>
        <Row gutter={[8, 24]}>
          <Col xs={24} sm={24} md={24} lg={24}>
            <div
              style={{
                backgroundColor: "#eaeaea",
                padding: "5px"
              }}
            >
              <div style={{ fontWeight: "400", fontSize: "16px" }}>
                Bookings
              </div>
            </div>
          </Col>
          <Col xs={24} sm={24} md={24} lg={4}>
            <label htmlFor="">First Name</label>
            <Input placeholder="First name" />
          </Col>
          <Col xs={24} sm={24} md={24} lg={4}>
            <label htmlFor="">Last Name</label>
            <Input placeholder="First name" />
          </Col>
          <Col xs={24} sm={24} md={24} lg={4}>
            <label htmlFor="">Passenger Type</label>
            <br />
            <Select
              showSearch
              style={{ width: "100%" }}
              placeholder="Select"
              optionFilterProp="children"
            >
              <Option value="jack">Adult</Option>
              <Option value="lucy">Child</Option>
              <Option value="tom">Infant</Option>
            </Select>
          </Col>
          <Col xs={24} sm={24} md={24} lg={4}>
            <label htmlFor="">Currency</label>
            <br />
            <Select
              showSearch
              style={{ width: "100%" }}
              placeholder="Select"
              optionFilterProp="children"
            >
              <Option value="jack">Pakistan Rupee</Option>
              <Option value="lucy">DXB Dirham</Option>
              <Option value="tom">India Rupee</Option>
            </Select>
          </Col>
          <Col xs={24} sm={24} md={24} lg={4}>
            <label htmlFor="">Base Fare</label>
            <Input placeholder="Form of Payment" />
          </Col>
          <Col xs={24} sm={24} md={24} lg={4}>
            <label htmlFor="">File Fare</label>
            <Input placeholder="File Fare" />
          </Col>
          <Col xs={24} sm={24} md={24} lg={4}>
            <label htmlFor="">Ticket Number</label>
            <Input placeholder="Enter Ticket Number." />
          </Col>
          <Col xs={24} sm={24} md={24} lg={4}>
            <label htmlFor="">Issue Date</label>
            <DatePicker style={{ width: "100%" }} />
          </Col>
          <Col xs={24} sm={24} md={24} lg={4}>
            <label>Form of Payment</label>
            <br />
            <Select
              showSearch
              style={{ width: "100%" }}
              placeholder="Select"
              optionFilterProp="children"
            >
              <Option value="jack">Cash</Option>
              <Option value="lucy">Check</Option>
              <Option value="tom">Credit Card</Option>
              <Option value="tom">Invoice</Option>
            </Select>
          </Col>
          <Col xs={24} sm={24} md={24} lg={2}>
            <br />
            <br />
            <Checkbox>is TMU?</Checkbox>
          </Col>
          <Col xs={24} sm={24} md={24} lg={4}>
            <label htmlFor="">Ticket Number</label>
            <Input placeholder="Enter Ticket Number." />
          </Col>
          <Col xs={24} sm={24} md={24} lg={4}>
            <br />
            <Button type="primary">Add</Button>
          </Col>
          <Col xs={24} sm={24} md={24} lg={24}>
            <label htmlFor="">Passenger Details</label>
            <Table columns={columns} dataSource={data} />
          </Col>
        </Row>
      </div>
    );
  }
}

export default ManualReservationComponent;
