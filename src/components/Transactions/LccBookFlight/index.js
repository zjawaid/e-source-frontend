import React, { Component } from "react";
import { Col, Row, Input, DatePicker, Button, Select } from "antd";
import Auxiliary from "util/Auxiliary";
const { Option } = Select;
function onChange(date, dateString) {
  console.log(date, dateString);
}
function handleChange(value) {
  console.log(`selected ${value}`);
}
class LccBookFlightComponent extends Component {
  render() {
    return (
      <div>
        <Auxiliary>
          <Row gutter={[8, 16]}>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <div
                style={{
                  backgroundColor: "#eaeaea",
                  padding: "10px"
                }}
              >
                <div style={{ fontWeight: "400", fontSize: "20px" }}>
                  LCC Book Flight
                </div>
              </div>
            </Col>
            <Col xl={6} lg={12} md={12} sm={12} xs={24}>
              <label>From City</label>
              <Input placeholder="Enter Ticket Number.." />
            </Col>
            <Col xl={6} lg={12} md={12} sm={12} xs={24}>
              <label>To City</label>
              <Input placeholder="Enter Ticket Number.." />
            </Col>
            <Col xl={6} lg={12} md={12} sm={12} xs={24}>
              <label>Departure Date</label>
              <br />
              <DatePicker style={{ width: "100%" }} onChange={onChange} />
            </Col>
            <Col xl={6} lg={12} md={12} sm={12} xs={24}>
              <label>Select WSAP</label>
              <br />
              <Select
                defaultValue="Select WSAP"
                style={{ width: "100%" }}
                onChange={handleChange}
              >
                <Option value="jack">Select WSAP</Option>
              </Select>
            </Col>
            <Col xl={6} lg={12} md={12} sm={12} xs={24}>
              <label>Adult</label>
              <br />
              <Select
                defaultValue="Adult"
                style={{ width: "100%" }}
                onChange={handleChange}
              >
                <Option value="1">1</Option>
                <Option value="2">2</Option>
                <Option value="3">3</Option>
                <Option value="4">4</Option>
                <Option value="5">5</Option>
                <Option value="6">6</Option>
              </Select>
            </Col>
            <Col xl={6} lg={12} md={12} sm={12} xs={24}>
              <label>Child</label>
              <br />
              <Select
                defaultValue="Child"
                style={{ width: "100%" }}
                onChange={handleChange}
              >
                <Option value="1">1</Option>
                <Option value="2">2</Option>
                <Option value="3">3</Option>
                <Option value="4">4</Option>
                <Option value="5">5</Option>
                <Option value="6">6</Option>
              </Select>
            </Col>
            <Col xl={6} lg={12} md={12} sm={12} xs={24}>
              <label>Infant</label>
              <br />
              <Select
                defaultValue="Child"
                style={{ width: "100%" }}
                onChange={handleChange}
              >
                <Option value="1">1</Option>
                <Option value="2">2</Option>
                <Option value="3">3</Option>
                <Option value="4">4</Option>
                <Option value="5">5</Option>
                <Option value="6">6</Option>
              </Select>
            </Col>
            <Col xl={6} lg={12} md={12} sm={12} xs={24}>
              <br />
              <Button type="danger">SEARCH FLIGHT</Button>
            </Col>
          </Row>
          <Row gutter={[8, 24]}>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <div
                style={{
                  padding: "10px",
                  backgroundColor: "#444",
                  color: "#fff"
                }}
              >
                Select Flight
              </div>
            </Col>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <div
                style={{
                  padding: "10px",
                  backgroundColor: "#444",
                  color: "#fff"
                }}
              >
                Passenger Details
              </div>
            </Col>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <div
                style={{
                  padding: "10px",
                  backgroundColor: "#444",
                  color: "#fff"
                }}
              >
                Fare Review
              </div>
            </Col>
          </Row>
        </Auxiliary>
      </div>
    );
  }
}

export default LccBookFlightComponent;
