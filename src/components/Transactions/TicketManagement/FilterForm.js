import React, { Component } from "react";
import { Col, Row, Input, Button, DatePicker, Select, Form } from "antd";
import { connect } from "react-redux";
import moment from "moment";

import { TICKET_MANAGEMENT_FORM_DATA } from "../../../appRedux/actions/TicketManagement";
const { Option } = Select;
class FilterForm extends Component {
  constructor() {
    super();

    this.state = {
      submitForm: ""
    };
  }

  submitForm = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      console.log("submitForm", values);
      this.props.onSubmitForm(values);
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const dateFormat = "YYYY/MM/DD";
    return (
      <div>
        <Form onSubmit={this.submitForm} className="">
          <Row gutter={[8, 24]}>
            <Col xs={24} sm={24} md={24} lg={24}>
              <div style={{ backgroundColor: "#eaeaea", padding: "10px" }}>
                Filter
              </div>
            </Col>
            <Col xs={24} sm={24} md={8} lg={4}>
              <label htmlFor="">From Date</label>
              {getFieldDecorator("from_date", {
                rules: [
                  {
                    required: true,
                    message: "Please select your start designation!"
                  }
                ]
              })(<DatePicker format={dateFormat} style={{ width: "100%" }} />)}
            </Col>
            <Col xs={24} sm={24} md={8} lg={4}>
              <label htmlFor="">To Date</label>
              {getFieldDecorator("to_date", {
                rules: [
                  {
                    required: true,
                    message: "Please select your start designation!"
                  }
                ]
              })(<DatePicker style={{ width: "100%" }} />)}
            </Col>
            <Col xs={24} sm={24} md={8} lg={4}>
              <label htmlFor="">Ticket No.</label>
              {getFieldDecorator("ticket_no", {
                rules: [
                  {
                    required: true,
                    message: "Please select your start designation!"
                  }
                ]
              })(<Input placeholder="Ticket No." />)}
            </Col>
            <Col xs={24} sm={24} md={8} lg={4}>
              <label htmlFor="">PNR</label>
              {getFieldDecorator("pnr", {
                rules: [
                  {
                    required: true,
                    message: "Please select your start designation!"
                  }
                ]
              })(<Input placeholder="Issued Date" />)}
            </Col>
            <Col xs={24} sm={24} md={8} lg={4}>
              <label htmlFor="">Status</label>
              {getFieldDecorator("status", {
                rules: [
                  {
                    required: true,
                    message: "Please select your start designation!"
                  }
                ]
              })(
                <Select
                  showSearch
                  style={{ width: "100%" }}
                  placeholder="Select status"
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.props.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  <Option value="Void">Void</Option>
                  <Option value="Issued">Issued</Option>
                  <Option value="Refund">Refund</Option>
                  <Option value="Re_Issue">Re-Issue</Option>
                </Select>
              )}
            </Col>
            <Col xs={24} sm={24} md={8} lg={4}>
              <br />
              <Button htmlType="submit" type="primary">
                Search
              </Button>
            </Col>
          </Row>
        </Form>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSubmitForm: ticketFormData => {
      dispatch(TICKET_MANAGEMENT_FORM_DATA(ticketFormData));
    }
  };
};

const WrappedFilterForm = Form.create()(FilterForm);

export default connect(null, mapDispatchToProps)(WrappedFilterForm);
