import React, { Component } from "react";
import { Col, Row, Input, Table, Divider, Tag, Button } from "antd";
import FilterForm from "./FilterForm";
import FilterData from "./FilterData";
import { connect } from "react-redux";
class TicketManagementComponent extends Component {
  render() {
    return (
      <div>
        <Row gutter={[8, 24]}>
          <Col xs={24} sm={24} md={24} lg={24}>
            <FilterForm />
          </Col>
          <Col xs={24} sm={24} md={24} lg={24}>
            <FilterData
              showUserData={this.props.showUserData}
              ticketDetailsData={this.props.ticketDetailsData}
            />
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ ticketManagement }) => {
  const { showUserData, ticketDetailsData } = ticketManagement;
  return { showUserData, ticketDetailsData };
};

export default connect(mapStateToProps, null)(TicketManagementComponent);
