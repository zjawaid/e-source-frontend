import React, { Component } from "react";
import {
  Col,
  Row,
  Table,
  Divider,
  Icon,
  Tooltip,
  Dropdown,
  Menu,
  Button
} from "antd";
import { connect } from "react-redux";
import {
  NotificationContainer,
  NotificationManager
} from "react-notifications";
import IntlMessages from "util/IntlMessages";

const createNotification = type => {
  return () => {
    switch (type) {
      case "info":
        NotificationManager.info(<IntlMessages id="notification.infoMsg" />);
        break;
      case "success":
        NotificationManager.success(
          <IntlMessages id="notification.successMessage" />,
          <IntlMessages id="notification.titleHere" />
        );
        break;
      case "warning":
        NotificationManager.warning(
          <IntlMessages id="notification.warningMessage" />,
          <IntlMessages id="notification.closeAfter3000ms" />,
          3000
        );
        break;
      case "error":
        NotificationManager.error(
          <IntlMessages id="notification.errorMessage" />,
          <IntlMessages id="notification.clickMe" />,
          5000,
          () => {
            alert("callback");
          }
        );
        break;
      default:
        NotificationManager.info(<IntlMessages id="notification.infoMsg" />);
        break;
    }
  };
};
class FilterData extends Component {
  render() {
    const menu = (
      <Menu>
        <Menu.Item>
          <a onClick={createNotification("success")}>Void Ticket</a>
        </Menu.Item>
        <Menu.Item>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="http://www.taobao.com/"
          >
            Issued Ticket
          </a>
        </Menu.Item>
        <Menu.Item>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="http://www.tmall.com/"
          >
            Re-Issue Ticket
          </a>
        </Menu.Item>
        <Menu.Item>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="http://www.tmall.com/"
          >
            Refund Ticket
          </a>
        </Menu.Item>
      </Menu>
    );
    const columns = [
      {
        title: "TICKET NUMBER",
        dataIndex: "ticket_number",
        key: "ticket_number",
        render: text => <a>{text}</a>
      },
      {
        title: "DATE OF ISSUE",
        dataIndex: "date_of_issue",
        key: "date_of_issue"
      },
      {
        title: "PNR",
        dataIndex: "PNR",
        key: "PNR"
      },
      {
        title: "ROUTE",
        dataIndex: "ROUTE",
        key: "ROUTE"
      },
      {
        title: "BASE FARE",
        dataIndex: "BASE_FARE",
        key: "BASE_FARE"
      },
      {
        title: "TAXES",
        dataIndex: "TAXES",
        key: "TAXES"
      },
      {
        title: "OTHER CHARGES	",
        dataIndex: "OTHER_CHARGES	",
        key: "OTHER_CHARGES"
      },
      {
        title: "PSF/(COMM)",
        dataIndex: "PSF_COMM",
        key: "PSF_COMM"
      },
      {
        title: "TOTAL AMOUNT",
        dataIndex: "TOTAL_AMOUNT",
        key: "TOTAL_AMOUNT"
      },
      {
        title: "STATUS",
        dataIndex: "STATUS",
        key: "STATUS"
      },
      {
        title: "CREATED BY",
        dataIndex: "CREATED_BY",
        key: "CREATED_BY"
      },
      {
        title: "Actions",
        key: "action",
        render: (text, record) => (
          <span>
            <Dropdown overlay={menu} placement="bottomLeft">
              <Tooltip title="Actions">
                <Icon type="more" />
              </Tooltip>
            </Dropdown>
            <Divider type="vertical" />
          </span>
        )
      }
    ];
    const userData =
      this.props.showUserData === true ? this.props.ticketDetailsData : null;
    return (
      <div>
        <Row gutter={[8, 24]}>
          <Col xs={24} sm={24} md={24} lg={24}>
            <Table columns={columns} dataSource={userData} />
          </Col>
        </Row>
        <NotificationContainer />
      </div>
    );
  }
}

export default connect(null, null)(FilterData);
