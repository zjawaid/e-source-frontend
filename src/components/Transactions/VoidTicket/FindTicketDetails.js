import React, { Component } from "react";
import { Col, Row, Input, Button, Form } from "antd";
import Widget from "components/Widget/index";
import { VOID_TICKET_FORM_DATA } from "../../../appRedux/actions/VoidTicket";

import { connect } from "react-redux";

class FindTicketDetials extends Component {
  constructor() {
    super();

    this.state = {
      submitForm: ""
    };
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);
        this.props.onSubmitForm(values);
        this.setState({
          submitFormValues: values
        });
      }
    });
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Widget>
          <Form layout="inline" onSubmit={this.handleSubmit.bind(this)}>
            <Row gutter={[8, 24]}>
              <Col xs={24} sm={24} md={24} lg={8}>
                <label htmlFor="">PNR</label>
                {getFieldDecorator("pnr", {
                  rules: [
                    { required: true, message: "Please input your username!" }
                  ]
                })(<Input placeholder="PNR" />)}
              </Col>
              <Col xs={24} sm={24} md={24} lg={8}>
                <label htmlFor="">Ticket Number</label>
                {getFieldDecorator("ticket_number", {
                  rules: [
                    { required: true, message: "Please input your username!" }
                  ]
                })(<Input placeholder="Ticket Number" />)}
              </Col>
              <Col xs={24} sm={24} md={24} lg={8}>
                <br />
                <Button htmlType="submit">RETRIEVE</Button>
              </Col>
            </Row>
          </Form>
        </Widget>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSubmitForm: ticketFormData => {
      dispatch(VOID_TICKET_FORM_DATA(ticketFormData));
    }
  };
};

const WrappedNormalTicketForm = Form.create()(FindTicketDetials);

export default connect(null, mapDispatchToProps)(WrappedNormalTicketForm);
