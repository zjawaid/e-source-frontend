import React, { Component } from "react";
import { Col, Row, Input, Table, Button } from "antd";
import { connect } from "react-redux";
import TicketInformation from "./TicketInformation";
import FindTicketDetials from "./FindTicketDetails";

class VoidTicketComponent extends Component {
  render() {
    return (
      <div>
        <Row gutter={[8, 24]}>
          <Col xs={24} sm={24} md={24} lg={24}>
            <FindTicketDetials />
          </Col>
          <Col span={24}>
            <TicketInformation
              showTableData={this.props.showTableData}
              ticketDetailsData={this.props.ticketDetailsData}
            />
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ voidTicket }) => {
  const { showTableData, ticketDetailsData } = voidTicket;
  return { showTableData, ticketDetailsData };
};

export default connect(mapStateToProps, null)(VoidTicketComponent);
