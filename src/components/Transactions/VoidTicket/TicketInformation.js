import React, { Component } from "react";
import { Col, Row, Table, Button, message } from "antd";
import Widget from "components/Widget/index";
import { connect } from "react-redux";
import { IS_VOIDED } from "../../../appRedux/actions/VoidTicket";
const key = "updatable";
class TicketInformation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVoidedSelection: ""
    };
  }
  rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      console.log(
        `selectedRowKeys: ${selectedRowKeys}`,
        "selectedRows: ",
        selectedRows
      );
      this.setState({
        isVoidedSelection: selectedRows
      });
      // this.props.isVoided(selectedRows);
    },
    onSelect: (record, selected, selectedRows) => {
      console.log(record, selected, selectedRows);
    },
    onSelectAll: (selected, selectedRows, changeRows) => {
      console.log(selected, selectedRows, changeRows);
    }
  };
  handleMenuClick = () => {
    console.log("click", this.state.isVoidedSelection);
    this.props.isVoided(this.state.isVoidedSelection);
    message
      .loading("Action in progress..", 2.5)
      .then(() => message.success("Loading finished", 2.5))
      .then(() => message.info("Records updated", 2.5));
  };
  render() {
    console.log(this.props);
    const data =
      this.props.showTableData === true ? this.props.ticketDetailsData : null;
    const columns = [
      {
        title: "PASSENGER",
        dataIndex: "PASSENGER",
        key: "PASSENGER",
        render: text => <a>{text}</a>
      },
      {
        title: "TICKET NUMBER",
        dataIndex: "TICKET_NUMBER",
        key: "TICKET_NUMBER"
      },
      {
        title: "FARE",
        dataIndex: "FARE",
        key: "FARE"
      },
      {
        title: "VOID CHARGES",
        dataIndex: "VOID_CHARGES",
        key: "VOID_CHARGES"
      },
      {
        title: "STATUS",
        dataIndex: "STATUS",
        key: "STATUS"
      },
      {
        title: "IS VOIDED?",
        dataIndex: "IS_VOIDED",
        key: "IS_VOIDED"
      }
    ];
    return (
      <div>
        <Widget title="Ticket Details">
          <Row gutter={[8, 24]}>
            <Col xs={24} sm={24} md={24} lg={24}>
              <Table
                rowSelection={this.rowSelection}
                columns={columns}
                dataSource={data}
              />
              <br />
              <Row gutter={[16, 24]} type="flex" justify="end">
                <Col>
                  <Button
                    type="primary"
                    onClick={this.handleMenuClick.bind(this)}
                  >
                    Void Ticket
                  </Button>
                  <Button type="danger">Cancel</Button>
                </Col>
              </Row>
            </Col>
          </Row>
        </Widget>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    isVoided: data => {
      dispatch(IS_VOIDED(data));
    }
  };
};

export default connect(null, mapDispatchToProps)(TicketInformation);
