import React, { Component } from "react";
import { Col, Row, Input, Card, Button } from "antd";
import Auxiliary from "util/Auxiliary";
class ContractDetails extends Component {
  render() {
    return (
      <div>
        <Auxiliary>
          <Row gutter={[8, 0]}>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <Card
                title="Contract Details"
                style={{ height: "200px" }}
                className="gx-card"
              >
                <Row gutter={[16, 0]}>
                  <Col xl={14} lg={14} md={24} sm={12} xs={24}>
                    <div style={{ marginTop: "5px" }}>
                      Contract Code: <span>5000/=</span>{" "}
                    </div>
                    <div style={{ marginTop: "5px" }}>Currency:</div>
                    <div style={{ marginTop: "5px" }}>Available Balance:</div>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
        </Auxiliary>
      </div>
    );
  }
}

export default ContractDetails;
