import React, { Component } from "react";
import { Col, Row, Input, Card, Button } from "antd";
import Auxiliary from "util/Auxiliary";
class ExchangeDetails extends Component {
  render() {
    return (
      <div>
        <Auxiliary>
          <Row gutter={[8, 0]}>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <Card
                title="Exchange Details"
                style={{ height: "200px" }}
                className="gx-card"
              ></Card>
            </Col>
          </Row>
        </Auxiliary>
      </div>
    );
  }
}

export default ExchangeDetails;
