import React, { Component } from "react";
import { Col, Row, Input, Card, Button } from "antd";
import Auxiliary from "util/Auxiliary";
import ExchangeTicketForm from "./ExchangeTicketForm";
import ExchangeDetails from "./ExchangeDetails";
import IssueTicketDetails from "./IssueTicketDetails";
import AmountCalculation from "./AmountCalculation";
import ContractDetails from "./ContractDetails";
class ExchangeTicketComponent extends Component {
  render() {
    return (
      <div>
        <Auxiliary>
          <Row gutter={[8, 24]}>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <ExchangeTicketForm />
            </Col>
          </Row>
          <Row gutter={[8, 0]}>
            <Col xl={12} lg={12} md={24} sm={12} xs={24}>
              <ExchangeDetails />
            </Col>
            <Col xl={12} lg={12} md={24} sm={12} xs={24}>
              <IssueTicketDetails />
            </Col>
            <Col xl={12} lg={12} md={24} sm={12} xs={24}>
              <AmountCalculation />
            </Col>
            <Col xl={12} lg={12} md={24} sm={12} xs={24}>
              <ContractDetails />
            </Col>
          </Row>
          <Row type="flex" justify="end" gutter={[8, 0]}>
            <Col>
              <Button type="primary">RE-ISSUE TICKET</Button>
            </Col>
          </Row>
        </Auxiliary>
      </div>
    );
  }
}

export default ExchangeTicketComponent;
