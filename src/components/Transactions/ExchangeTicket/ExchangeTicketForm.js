import React, { Component } from "react";
import { Col, Row, Input, Button } from "antd";
import Auxiliary from "util/Auxiliary";
class ExchangeTicketForm extends Component {
  render() {
    return (
      <div>
        <Auxiliary>
          <Row gutter={[8, 24]}>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <div
                style={{
                  backgroundColor: "#eaeaea",
                  padding: "10px",
                  fontWeight: "500"
                }}
              >
                Exchange Ticket
              </div>
            </Col>
            <Col xl={6} lg={12} md={12} sm={12} xs={24}>
              <label>Ticket Number</label>
              <Input
                type="number"
                maxLength="13"
                placeholder="Enter Ticket Number.."
              />
            </Col>
            <Col xl={6} lg={12} md={12} sm={12} xs={24}>
              <label>PNR</label>
              <Input maxLength="6" placeholder="Enter PNR Number.." />
            </Col>
            <Col xl={6} lg={12} md={12} sm={12} xs={24}>
              <br />
              <Button type="primary">RETRIEVE</Button>
            </Col>
          </Row>
        </Auxiliary>
      </div>
    );
  }
}

export default ExchangeTicketForm;
