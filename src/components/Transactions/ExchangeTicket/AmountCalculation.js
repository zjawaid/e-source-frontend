import React, { Component } from "react";
import { Col, Row, Input, Card, Button } from "antd";
import Auxiliary from "util/Auxiliary";

class AmountCalculation extends Component {
  render() {
    return (
      <div>
        <Auxiliary>
          <Row gutter={[8, 0]}>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <Card
                title="Amount Calculation"
                style={{ height: "200px" }}
                className="gx-card"
              >
                <Row gutter={[16, 0]}>
                  <Col xl={14} lg={14} md={24} sm={12} xs={24}>
                    <div style={{ marginTop: "5px" }}>
                      Fare Diff: <span>5000/=</span>{" "}
                    </div>
                    <div style={{ marginTop: "5px" }}>Tax</div>
                    <div style={{ marginTop: "5px" }}>Total:</div>
                    <div style={{ marginTop: "30px" }}>Net Amount:</div>
                  </Col>
                  <Col>
                    <div style={{ marginTop: "5px" }}>
                      Equ Fare Diff: <span>5000/=</span>{" "}
                    </div>
                    <div style={{ marginTop: "5px" }}>A/L Charges:</div>
                    <div style={{ marginTop: "5px" }}>Consolidator:</div>
                    <div style={{ marginTop: "30px" }}>Charge to Account:</div>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
        </Auxiliary>
      </div>
    );
  }
}

export default AmountCalculation;
