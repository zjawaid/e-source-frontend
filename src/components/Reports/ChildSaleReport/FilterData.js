import React, { Component } from "react";
import { Col, Row, Table, Button } from "antd";
import Auxiliary from "util/Auxiliary";

const rowSelection = {
  onChange: (selectedRowKeys, selectedRows) => {
    console.log(
      `selectedRowKeys: ${selectedRowKeys}`,
      "selectedRows: ",
      selectedRows
    );
  },
  onSelect: (record, selected, selectedRows) => {
    console.log(record, selected, selectedRows);
  },
  onSelectAll: (selected, selectedRows, changeRows) => {
    console.log(selected, selectedRows, changeRows);
  }
};

class FilterData extends Component {
  render() {
    const data = [
      //   {
      //     key: "1",
      //     ref_type: "INVOICE",
      //     pnr: "LM21BG",
      //     ref_no: "5de8c379ee01813d88955b24",
      //     CREATION_DATE: new Date().toISOString().slice(0, 10),
      //     TOTAL_BOOKINGS: "2",
      //     BOOKINGS_TOTAL: "4423",
      //     AGENCY: "UAT AGENCY 2",
      //     DESCRIPTION: "Supplier or Customer mapping is missing"
      //   },
      //   {
      //     key: "2",
      //     ref_type: "INVOICE",
      //     pnr: "LM21BG",
      //     ref_no: "5de8c379ee01813d88955b24",
      //     CREATION_DATE: new Date().toISOString().slice(0, 10),
      //     TOTAL_BOOKINGS: "3",
      //     BOOKINGS_TOTAL: "4234",
      //     AGENCY: "UAT AGENCY 2",
      //     DESCRIPTION: "Supplier or Customer mapping is missing"
      //   }
    ];
    const columns = [
      {
        title: "Issue Date",
        dataIndex: "issue_date",
        key: "issue_date",
        render: text => <a>{text}</a>
      },
      {
        title: "PNR",
        dataIndex: "PNR",
        key: "PNR"
      },
      {
        title: "User",
        dataIndex: "User",
        key: "User"
      },
      {
        title: "Passenger_name",
        dataIndex: "Passenger_name",
        key: "Passenger_name"
      },
      {
        title: "Ticket Number",
        dataIndex: "Ticket_Number",
        key: "Ticket_Number"
      },
      {
        title: "Ticket Type",
        dataIndex: "Ticket_type",
        key: "Ticket_type"
      },
      {
        title: "Status",
        dataIndex: "Status",
        key: "Status"
      },
      {
        title: "Airline",
        dataIndex: "Airline",
        key: "Airline"
      },
      {
        title: "Sector",
        dataIndex: "Sector",
        key: "Sector"
      },
      {
        title: "Fare",
        dataIndex: "Fare",
        key: "Fare"
      },
      {
        title: "Taxes",
        dataIndex: "Taxes",
        key: "Taxes"
      },
      {
        title: "Comm",
        dataIndex: "Comm",
        key: "Comm"
      },
      {
        title: "PSF",
        dataIndex: "PSF",
        key: "PSF"
      },
      {
        title: "Void Charges",
        dataIndex: "Void_Charges",
        key: "Void_Charges"
      },
      {
        title: "WHT",
        dataIndex: "WHT",
        key: "WHT"
      },
      {
        title: "APT/S.P",
        dataIndex: "APT_S_P",
        key: "APT_S_P"
      },
      {
        title: "RG/CVT",
        dataIndex: "RG_CVT",
        key: "RG_CVT"
      },
      {
        title: "Charge Amount",
        dataIndex: "charge_amount",
        key: "charge_amount"
      }
    ];
    return (
      <div>
        <Auxiliary>
          <Row gutter={[16, 16]}>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <br />
              <Row type="flex" justify="center">
                {`Child Sale Report - Report generated on date: ` + new Date()}
              </Row>
            </Col>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <Table columns={columns} dataSource={data} />
              <Row type="flex" justify="end">
                <Button type="danger">PDF</Button>
                <Button type="danger">EXCEL</Button>
              </Row>
            </Col>
          </Row>
        </Auxiliary>
      </div>
    );
  }
}

export default FilterData;
