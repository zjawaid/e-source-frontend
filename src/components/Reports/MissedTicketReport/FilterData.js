import React, { Component } from "react";
import { Col, Row, Table, Button } from "antd";
import Auxiliary from "util/Auxiliary";

const rowSelection = {
  onChange: (selectedRowKeys, selectedRows) => {
    console.log(
      `selectedRowKeys: ${selectedRowKeys}`,
      "selectedRows: ",
      selectedRows
    );
  },
  onSelect: (record, selected, selectedRows) => {
    console.log(record, selected, selectedRows);
  },
  onSelectAll: (selected, selectedRows, changeRows) => {
    console.log(selected, selectedRows, changeRows);
  }
};

class FilterData extends Component {
  render() {
    const data = [
      //   {
      //     key: "1",
      //     ref_type: "INVOICE",
      //     pnr: "LM21BG",
      //     ref_no: "5de8c379ee01813d88955b24",
      //     CREATION_DATE: new Date().toISOString().slice(0, 10),
      //     TOTAL_BOOKINGS: "2",
      //     BOOKINGS_TOTAL: "4423",
      //     AGENCY: "UAT AGENCY 2",
      //     DESCRIPTION: "Supplier or Customer mapping is missing"
      //   },
      //   {
      //     key: "2",
      //     ref_type: "INVOICE",
      //     pnr: "LM21BG",
      //     ref_no: "5de8c379ee01813d88955b24",
      //     CREATION_DATE: new Date().toISOString().slice(0, 10),
      //     TOTAL_BOOKINGS: "3",
      //     BOOKINGS_TOTAL: "4234",
      //     AGENCY: "UAT AGENCY 2",
      //     DESCRIPTION: "Supplier or Customer mapping is missing"
      //   }
    ];
    const columns = [
      {
        title: "Issue Date",
        dataIndex: "issue_date",
        key: "issue_date",
        render: text => <a>{text}</a>
      },
      {
        title: "Pax Name",
        dataIndex: "pax_name",
        key: "pax_name"
      },
      {
        title: "PNR",
        dataIndex: "pnr",
        key: "pnr"
      },
      {
        title: "Fare",
        dataIndex: "fare",
        key: "fare"
      },
      {
        title: "Taxes",
        dataIndex: "taxes",
        key: "taxes"
      },
      {
        title: "Total Amount",
        dataIndex: "total_amount",
        key: "total_amount"
      }
    ];
    return (
      <div>
        <Auxiliary>
          <Row gutter={[16, 16]}>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <br />
              <Row type="flex" justify="center">
                {`Refund Report - Report generated on date: ` + new Date()}
              </Row>
            </Col>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <Table columns={columns} dataSource={data} />
              <Row type="flex" justify="end">
                <Button type="danger">PDF</Button>
                <Button type="danger">EXCEL</Button>
              </Row>
            </Col>
          </Row>
        </Auxiliary>
      </div>
    );
  }
}

export default FilterData;
