import React, { Component } from "react";
import { Col, Row, Input, Card, Button } from "antd";
import Auxiliary from "util/Auxiliary";
import SearchFilter from "./SearchFilter";
import FilterData from "./FilterData";

class VoidReports extends Component {
  constructor(props) {
    super(props);
    this.state = {
      voidReports: [
        {
          key: "1",
          DOI: "INVOICE",
          Agency: "LM21BG",
          User: "5de8c379ee01813d88955b24",
          Pax_Name: new Date().toISOString().slice(0, 10),
          PNR: "2",
          Ticket_Number: "4423",
          doc_type: "UAT AGENCY 2",
          A_L: "Supplier or Customer mapping is missing",
          total_fare: 4000,
          void_charges_r: 120,
          void_charges_p: 12,
          amount_payable: 132
        }
      ]
    };
  }
  render() {
    return (
      <div>
        <Auxiliary>
          <Row gutter={[8, 24]}>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <SearchFilter />
            </Col>
          </Row>
          <Row gutter={[8, 0]}>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <FilterData reportsList={this.state.voidReports} />
            </Col>
          </Row>
        </Auxiliary>
      </div>
    );
  }
}

export default VoidReports;
