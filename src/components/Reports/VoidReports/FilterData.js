import React, { Component } from "react";
import { Col, Row, Table, Button } from "antd";
import Auxiliary from "util/Auxiliary";

class FilterData extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const columns = [
      {
        title: "DOI",
        dataIndex: "DOI",
        key: "DOI",
        render: text => <a>{text}</a>
      },
      {
        title: "Agency",
        dataIndex: "Agency",
        key: "Agency"
      },
      {
        title: "User",
        dataIndex: "User",
        key: "User"
      },
      {
        title: "Pax Name",
        dataIndex: "Pax_Name",
        key: "Pax_Name"
      },
      {
        title: "PNR",
        dataIndex: "PNR",
        key: "PNR"
      },
      {
        title: "Ticket Number",
        dataIndex: "Ticket_Number",
        key: "Ticket_Number"
      },
      {
        title: "Doc Type",
        dataIndex: "doc_type",
        key: "doc_type"
      },
      {
        title: "A/L",
        dataIndex: "A_L",
        key: "A_L"
      },
      {
        title: "Total Fare",
        dataIndex: "total_fare",
        key: "total_fare"
      },
      {
        title: "Void Charges/R",
        dataIndex: "void_charges_r",
        key: "void_charges_r"
      },
      {
        title: "Void Charges/P",
        dataIndex: "void_charges_p",
        key: "void_charges_p"
      },
      {
        title: "Amount Payable",
        dataIndex: "amount_payable",
        key: "amount_payable"
      }
    ];
    const { reportsList } = this.props;
    return (
      <div>
        <Auxiliary>
          <Row gutter={[16, 16]}>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <br />
              <Row type="flex" justify="center">
                {`Void Report - Report generated on date: ` + new Date()}
              </Row>
            </Col>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <Table columns={columns} dataSource={reportsList} />
              <Row type="flex" justify="end">
                <Button type="danger">PDF</Button>
                <Button type="danger">EXCEL</Button>
              </Row>
            </Col>
          </Row>
        </Auxiliary>
      </div>
    );
  }
}

export default FilterData;
