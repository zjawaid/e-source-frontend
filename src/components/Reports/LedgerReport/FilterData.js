import React, { Component } from "react";
import { Col, Row, Table, Button } from "antd";
import Auxiliary from "util/Auxiliary";

const rowSelection = {
  onChange: (selectedRowKeys, selectedRows) => {
    console.log(
      `selectedRowKeys: ${selectedRowKeys}`,
      "selectedRows: ",
      selectedRows
    );
  },
  onSelect: (record, selected, selectedRows) => {
    console.log(record, selected, selectedRows);
  },
  onSelectAll: (selected, selectedRows, changeRows) => {
    console.log(selected, selectedRows, changeRows);
  }
};

class FilterData extends Component {
  render() {
    const data = [
      //   {
      //     key: "1",
      //     ref_type: "INVOICE",
      //     pnr: "LM21BG",
      //     ref_no: "5de8c379ee01813d88955b24",
      //     CREATION_DATE: new Date().toISOString().slice(0, 10),
      //     TOTAL_BOOKINGS: "2",
      //     BOOKINGS_TOTAL: "4423",
      //     AGENCY: "UAT AGENCY 2",
      //     DESCRIPTION: "Supplier or Customer mapping is missing"
      //   },
      //   {
      //     key: "2",
      //     ref_type: "INVOICE",
      //     pnr: "LM21BG",
      //     ref_no: "5de8c379ee01813d88955b24",
      //     CREATION_DATE: new Date().toISOString().slice(0, 10),
      //     TOTAL_BOOKINGS: "3",
      //     BOOKINGS_TOTAL: "4234",
      //     AGENCY: "UAT AGENCY 2",
      //     DESCRIPTION: "Supplier or Customer mapping is missing"
      //   }
    ];
    const columns = [
      {
        title: "Name",
        dataIndex: "Name",
        key: "Name",
        render: text => <a>{text}</a>
      },
      {
        title: "Date",
        dataIndex: "Date",
        key: "Date"
      },
      {
        title: "Ticket No.",
        dataIndex: "ticket_no",
        key: "ticket_no"
      },
      {
        title: "Airline",
        dataIndex: "Airline",
        key: "Airline"
      },
      {
        title: "Amount",
        dataIndex: "Amount",
        key: "Amount"
      },
      {
        title: "Service Charges",
        dataIndex: "service_charges",
        key: "service_charges"
      },
      {
        title: "Comission Discount",
        dataIndex: "com_discount",
        key: "com_discount"
      },
      {
        title: "Void Charges",
        dataIndex: "void_charges",
        key: "void_charges"
      },
      {
        title: "Running Balance",
        dataIndex: "running_balance",
        key: "running_balance"
      },
      {
        title: "Chapping",
        dataIndex: "Chapping",
        key: "Chapping"
      },
      {
        title: "Capping Balance",
        dataIndex: "capping_balance",
        key: "capping_balance"
      },
      {
        title: "Ledger Type",
        dataIndex: "ledger_type",
        key: "ledger_type"
      }
    ];
    return (
      <div>
        <Auxiliary>
          <Row gutter={[16, 16]}>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <br />
              <Row type="flex" justify="center">
                {`Ledger Report - Report generated on date: ` + new Date()}
              </Row>
            </Col>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <Table columns={columns} dataSource={data} />
              <Row type="flex" justify="end">
                <Button type="danger">PDF</Button>
                <Button type="danger">EXCEL</Button>
              </Row>
            </Col>
          </Row>
        </Auxiliary>
      </div>
    );
  }
}

export default FilterData;
