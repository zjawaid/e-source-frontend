import React, { Component } from "react";
import { Col, Row, Table, Button } from "antd";
import Auxiliary from "util/Auxiliary";
import { connect } from "react-redux";

const rowSelection = {
  onChange: (selectedRowKeys, selectedRows) => {
    console.log(
      `selectedRowKeys: ${selectedRowKeys}`,
      "selectedRows: ",
      selectedRows
    );
  },
  onSelect: (record, selected, selectedRows) => {
    console.log(record, selected, selectedRows);
  },
  onSelectAll: (selected, selectedRows, changeRows) => {
    console.log(selected, selectedRows, changeRows);
  }
};

class FilterData extends Component {
  render() {
    const data =
      this.props.showUserData === true
        ? this.props.refundReportFilterData
        : null;
    const columns = [
      {
        title: "DOI",
        dataIndex: "DOI",
        key: "DOI",
        render: text => <a>{text}</a>
      },
      {
        title: "Date of Refund",
        dataIndex: "date_of_refund",
        key: "date_of_refund"
      },
      {
        title: "Agency",
        dataIndex: "Agency",
        key: "Agency"
      },
      {
        title: "User",
        dataIndex: "User",
        key: "User"
      },
      {
        title: "Pax Name",
        dataIndex: "Pax_Name",
        key: "Pax_Name"
      },
      {
        title: "PNR",
        dataIndex: "PNR",
        key: "PNR"
      },
      {
        title: "Ticket Number",
        dataIndex: "Ticket_Number",
        key: "Ticket_Number"
      },
      {
        title: "A/L",
        dataIndex: "A_L",
        key: "A_L"
      },
      {
        title: "Fare",
        dataIndex: "Fare",
        key: "Fare"
      },
      {
        title: "EQU to",
        dataIndex: "EQU_to",
        key: "EQU_to"
      },
      {
        title: "Fare_used",
        dataIndex: "Fare_used",
        key: "Fare_used"
      },
      {
        title: "Fare Refundable",
        dataIndex: "Fare_Refundable",
        key: "Fare_Refundable"
      },
      {
        title: "Taxes Refundable",
        dataIndex: "Taxes_Refundable",
        key: "Taxes_Refundable"
      },
      {
        title: "Other Charges",
        dataIndex: "Other_Charges",
        key: "Other_Charges"
      },
      {
        title: "DIS/R",
        dataIndex: "DIS_R",
        key: "DIS_R"
      },
      {
        title: "DIS/P",
        dataIndex: "DIS_P",
        key: "DIS_P"
      },
      {
        title: "PSF/R",
        dataIndex: "PSF_R",
        key: "PSF_R"
      },
      {
        title: "PSF/P",
        dataIndex: "PSF_P",
        key: "PSF_P"
      },
      {
        title: "A/L Refund Charges",
        dataIndex: "Al_Refund_Charges",
        key: "Al_Refund_Charges"
      },
      {
        title: "CONS Refund Charges/R",
        dataIndex: "CONS_Refund_Charges_R",
        key: "CONS_Refund_Charges_R"
      },
      {
        title: "CONS Refund Charges/P",
        dataIndex: "CONS_Refund_Charges_P",
        key: "CONS_Refund_Charges_P"
      },
      {
        title: "Refund/R",
        dataIndex: "Refund_R",
        key: "Refund/R"
      },
      {
        title: "Refund/P",
        dataIndex: "Refund_P",
        key: "Refund_P"
      },
      {
        title: "Status",
        dataIndex: "Status",
        key: "Status"
      }
    ];

    return (
      <div>
        <Auxiliary>
          <Row gutter={[16, 16]}>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <br />
              <Row type="flex" justify="center">
                {`Refund Report - Report generated on date: ` + new Date()}
              </Row>
            </Col>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <Table columns={columns} dataSource={data} />
              <Row type="flex" justify="end">
                <Button type="danger">PDF</Button>
                <Button type="danger">EXCEL</Button>
              </Row>
            </Col>
          </Row>
        </Auxiliary>
      </div>
    );
  }
}

const mapStateToProps = ({ refundReports }) => {
  const { refundReportFilterData, showUserData } = refundReports;
  return { refundReportFilterData, showUserData };
};

export default connect(mapStateToProps)(FilterData);
