import React, { Component } from "react";
import { Col, Row, Input, Card, Button } from "antd";
import Auxiliary from "util/Auxiliary";
import SearchFilter from "./SearchFilter";
import FilterData from "./FilterData";

class RefundReports extends Component {
  render() {
    return (
      <div>
        <Auxiliary>
          <Row gutter={[8, 24]}>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <SearchFilter />
            </Col>
          </Row>
          <Row gutter={[8, 0]}>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <FilterData />
            </Col>
          </Row>
        </Auxiliary>
      </div>
    );
  }
}

export default RefundReports;
