import React, { Component } from "react";
import { Col, Row, Table, Button } from "antd";
// import Auxiliary from "util/Auxiliary";
import { connect } from "react-redux";

class FilterData extends Component {
  render() {
    const data =
      this.props.showUserData === true
        ? this.props.ChildRefundReportFilterData
        : null;
    const columns = [
      {
        title: "Refund Date",
        dataIndex: "refund_date",
        key: "refund_date",
        render: text => <a>{text}</a>
      },
      {
        title: "Issue date",
        dataIndex: "issue_date",
        key: "issue_date"
      },
      {
        title: "Description",
        dataIndex: "Description",
        key: "Description"
      },
      {
        title: "Agency",
        dataIndex: "agency",
        key: "agency"
      },
      {
        title: "User",
        dataIndex: "user",
        key: "user"
      },
      {
        title: "PNR",
        dataIndex: "PNR",
        key: "PNR"
      },
      {
        title: "PAX",
        dataIndex: "PAX",
        key: "PAX"
      },
      {
        title: "Ticket Number",
        dataIndex: "ticket_number",
        key: "ticket_number"
      },
      {
        title: "Fare",
        dataIndex: "Fare",
        key: "Fare"
      },
      {
        title: "Fare EQU",
        dataIndex: "fare_equ",
        key: "fare_equ"
      },
      {
        title: "Fare_used",
        dataIndex: "Fare_used",
        key: "Fare_used"
      },
      {
        title: "Total Tax",
        dataIndex: "total_tax",
        key: "total_tax"
      },
      {
        title: "Airline Charges",
        dataIndex: "airline_charges",
        key: "airline_charges"
      },
      {
        title: "Total Refunded",
        dataIndex: "total_refunded",
        key: "total_refunded"
      },
      {
        title: "Rate",
        dataIndex: "Rate",
        key: "Rate"
      },
      {
        title: "Commission",
        dataIndex: "Commission",
        key: "Commission"
      },
      {
        title: "Back to Account",
        dataIndex: "back_to_account",
        key: "back_to_account"
      }
    ];
    return (
      <div>
        {/* <Auxiliary> */}
        <Row gutter={[16, 16]}>
          <Col xl={24} lg={24} md={24} sm={24} xs={24}>
            <br />
            <Row type="flex" justify="center">
              {`Child Refund Report - Report generated on date: ` + new Date()}
            </Row>
          </Col>
          <Col xl={24} lg={24} md={24} sm={24} xs={24}>
            <Table columns={columns} dataSource={data} />
            <Row type="flex" justify="end">
              <Button type="danger">PDF</Button>
              <Button type="danger">EXCEL</Button>
            </Row>
          </Col>
        </Row>
        {/* </Auxiliary> */}
      </div>
    );
  }
}

const mapStateToProps = ({ childRefundReports }) => {
  const { ChildRefundReportFilterData, showUserData } = childRefundReports;
  return { ChildRefundReportFilterData, showUserData };
};

export default connect(mapStateToProps)(FilterData);
