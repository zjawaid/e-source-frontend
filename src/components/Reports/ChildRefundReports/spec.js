import React from "react";
import { shallow } from "enzyme";
import ChildRefundReports from "./index";

const setUp = (props = {}) => {
  const component = shallow(<ChildRefundReports {...props} />);
  return component;
};

describe("Child Refund Component", () => {
  describe("Child Refund Index File", () => {
    it("It should render without errors", () => {
      const component = setUp();
      const wrapper = component.find(`[data-test='ChildRefundComponent']`);
      expect(wrapper.length).toBe(1);
    });
  });
  describe("Child Refund FilterData File", () => {
    it("It should render without errors", () => {
      const component = setUp();
      const wrapper = component.find(".ChildRefundComponent");
      expect(wrapper.length).toBe(1);
    });
  });
  describe("Child Refund Searc Filter File", () => {
    it("It should render without errors", () => {
      const component = setUp();
      const wrapper = component.find(".ChildRefundComponent");
      expect(wrapper.length).toBe(1);
    });
  });
});
