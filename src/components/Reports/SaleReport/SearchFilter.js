import React, { Component } from "react";
import { Col, Row, Input, DatePicker, Button, Select, Form } from "antd";
import Auxiliary from "util/Auxiliary";
const { Option } = Select;

function handleChange(value) {
  console.log(`selected ${value}`);
}
class SearchFilter extends Component {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Auxiliary>
          <Form onSubmit={this.handleSubmit}>
            <Row style={{ borderBottom: "1px solid #cccc" }} gutter={[8, 16]}>
              <Col xl={24} lg={24} md={24} sm={24} xs={24}>
                <div
                  style={{
                    backgroundColor: "#eaeaea",
                    padding: "10px"
                  }}
                >
                  <div style={{ fontWeight: "400", fontSize: "20px" }}>
                    Sale Report
                  </div>
                </div>
              </Col>
              <Col xl={6} lg={12} md={12} sm={12} xs={24}>
                <label>From Date</label>
                {getFieldDecorator("from_date", {
                  rules: [
                    { required: true, message: "Please input your username!" }
                  ]
                })(<DatePicker style={{ width: "100%" }} />)}
              </Col>
              <Col xl={6} lg={12} md={12} sm={12} xs={24}>
                <label>To Date</label>
                {getFieldDecorator("to_date", {
                  rules: [
                    { required: true, message: "Please input your username!" }
                  ]
                })(<DatePicker style={{ width: "100%" }} />)}
              </Col>
              <Col xl={6} lg={12} md={12} sm={12} xs={24}>
                <label>Agency</label>
                <br />
                {getFieldDecorator("agency", {
                  rules: [
                    { required: true, message: "Please input your username!" }
                  ]
                })(
                  <Select
                    placeholder="Select status"
                    style={{ width: "100%" }}
                    onChange={handleChange}
                  >
                    <Option value="SelectAll">SelectAll</Option>
                    <Option value="VA8IVE">VA8IVE</Option>
                    <Option value="UAT">UAT</Option>
                  </Select>
                )}
              </Col>
              <Col xl={6} lg={12} md={12} sm={12} xs={24}>
                <label>Contract</label>
                <br />
                {getFieldDecorator("contract", {
                  rules: [
                    { required: true, message: "Please input your username!" }
                  ]
                })(
                  <Select
                    placeholder="Select status"
                    style={{ width: "100%" }}
                    onChange={handleChange}
                  >
                    <Option value="VA8IVE">VA8IVE</Option>
                    <Option value="UAT">UAT</Option>
                  </Select>
                )}
              </Col>
              <Col xl={6} lg={12} md={12} sm={12} xs={24}>
                <label>User</label>
                <br />
                {getFieldDecorator("user", {
                  rules: [
                    { required: true, message: "Please input your username!" }
                  ]
                })(
                  <Select
                    placeholder="Select status"
                    style={{ width: "100%" }}
                    onChange={handleChange}
                  >
                    <Option value="VA8IVE">VA8IVE</Option>
                    <Option value="UAT">UAT</Option>
                  </Select>
                )}
              </Col>
              <Col xl={6} lg={12} md={12} sm={12} xs={24}>
                <label>Airline</label>
                <br />
                {getFieldDecorator("airline", {
                  rules: [
                    { required: true, message: "Please input your username!" }
                  ]
                })(<Input placeholder="Select Airline" />)}
              </Col>
              <Col xl={6} lg={12} md={12} sm={12} xs={24}>
                <label>Status</label>
                <br />
                {getFieldDecorator("status", {
                  rules: [
                    { required: true, message: "Please input your username!" }
                  ]
                })(
                  <Select
                    placeholder="Select status"
                    style={{ width: "100%" }}
                    onChange={handleChange}
                  >
                    <Option value="Issued">Issued</Option>
                    <Option value="ReIssued">Re-Issued</Option>
                  </Select>
                )}
              </Col>
              <Col xl={6} lg={12} md={12} sm={12} xs={24}>
                <br />
                <Button htmlType="submit" type="danger">
                  SEARCH
                </Button>
              </Col>
            </Row>
          </Form>
        </Auxiliary>
      </div>
    );
  }
}

const SearchFilterForm = Form.create()(SearchFilter);

export default SearchFilterForm;
