import React, { Component } from "react";
import { Col, Row, Table, Button } from "antd";
import Auxiliary from "util/Auxiliary";

const rowSelection = {
  onChange: (selectedRowKeys, selectedRows) => {
    console.log(
      `selectedRowKeys: ${selectedRowKeys}`,
      "selectedRows: ",
      selectedRows
    );
  },
  onSelect: (record, selected, selectedRows) => {
    console.log(record, selected, selectedRows);
  },
  onSelectAll: (selected, selectedRows, changeRows) => {
    console.log(selected, selectedRows, changeRows);
  }
};

class FilterData extends Component {
  render() {
    const data = [
      //   {
      //     key: "1",
      //     ref_type: "INVOICE",
      //     pnr: "LM21BG",
      //     ref_no: "5de8c379ee01813d88955b24",
      //     CREATION_DATE: new Date().toISOString().slice(0, 10),
      //     TOTAL_BOOKINGS: "2",
      //     BOOKINGS_TOTAL: "4423",
      //     AGENCY: "UAT AGENCY 2",
      //     DESCRIPTION: "Supplier or Customer mapping is missing"
      //   },
      //   {
      //     key: "2",
      //     ref_type: "INVOICE",
      //     pnr: "LM21BG",
      //     ref_no: "5de8c379ee01813d88955b24",
      //     CREATION_DATE: new Date().toISOString().slice(0, 10),
      //     TOTAL_BOOKINGS: "3",
      //     BOOKINGS_TOTAL: "4234",
      //     AGENCY: "UAT AGENCY 2",
      //     DESCRIPTION: "Supplier or Customer mapping is missing"
      //   }
    ];
    const columns = [
      {
        title: "DOI",
        dataIndex: "DOI",
        key: "DOI",
        render: text => <a>{text}</a>
      },
      {
        title: "Date of Exchange",
        dataIndex: "date_of_exhcan",
        key: "date_of_exhcan"
      },
      {
        title: "Agency",
        dataIndex: "Agency",
        key: "Agency"
      },
      {
        title: "User",
        dataIndex: "User",
        key: "User"
      },
      {
        title: "Pax Name",
        dataIndex: "Pax_Name",
        key: "Pax_Name"
      },
      {
        title: "PNR",
        dataIndex: "PNR",
        key: "PNR"
      },
      {
        title: "Ticket Number",
        dataIndex: "Ticket_Number",
        key: "Ticket_Number"
      },
      {
        title: "Excha for",
        dataIndex: "excha_for",
        key: "excha_for"
      },
      {
        title: "DOC Type",
        dataIndex: "Doc_type",
        key: "Doc_type"
      },
      {
        title: "A/L",
        dataIndex: "A_L",
        key: "A_L"
      },
      {
        title: "Fare",
        dataIndex: "Fare",
        key: "Fare"
      },
      {
        title: "Taxes",
        dataIndex: "Taxes",
        key: "Taxes"
      },
      {
        title: "Other Charges",
        dataIndex: "other_charges",
        key: "other_charges"
      },
      {
        title: "DOF",
        dataIndex: "DOF",
        key: "DOF"
      },
      {
        title: "A/L Charges",
        dataIndex: "Al_charges",
        key: "Al_charges"
      },
      {
        title: "CONS charges/P",
        dataIndex: "Cons_charges_p",
        key: "Cons_charges_p"
      },
      {
        title: "CONS charges/R",
        dataIndex: "Cons_charges_r",
        key: "Cons_charges_r"
      },
      {
        title: "DIS/R",
        dataIndex: "DIS_R",
        key: "DIS_R"
      },
      {
        title: "DIS/P",
        dataIndex: "DIS_P",
        key: "DIS_P"
      },
      {
        title: "PSF/R",
        dataIndex: "PSF_R",
        key: "PSF_R"
      },
      {
        title: "WHT",
        dataIndex: "WHT",
        key: "WHT"
      },
      {
        title: "PSF/P",
        dataIndex: "PSF_P",
        key: "PSF_P"
      },
      {
        title: "Amount Payable",
        dataIndex: "amount_payable",
        key: "amount_payable"
      },
      {
        title: "Amount Receivable",
        dataIndex: "amount_receivable",
        key: "amount_receivable"
      },
      {
        title: "Status",
        dataIndex: "Status",
        key: "Status"
      }
    ];
    return (
      <div>
        <Auxiliary>
          <Row gutter={[16, 16]}>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <br />
              <Row type="flex" justify="center">
                {`Sale Report - Report generated on date: ` + Date.now()}
              </Row>
            </Col>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <Table columns={columns} dataSource={data} />
              <Row type="flex" justify="end">
                <Button type="danger">PDF</Button>
                <Button type="danger">EXCEL</Button>
              </Row>
            </Col>
          </Row>
        </Auxiliary>
      </div>
    );
  }
}

export default FilterData;
