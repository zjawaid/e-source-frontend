import React from "react";
import {
  Area,
  AreaChart,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  PieChart,
  Legend,
  Line,
  Pie,
  Cell
} from "recharts";
import { Select } from "antd";

import Widget from "components/Widget/index";

const data = [
  { name: "JAN", value: 400 },
  { name: "FEB", value: 150 },
  { name: "MAR", value: 400 },
  { name: "APR", value: 1000 },
  { name: "MAY", value: 400 },
  { name: "JUN", value: 1200 },
  { name: "JUL", value: 1000 },
  { name: "AUG", value: 800 },
  { name: "SEP", value: 750 },
  { name: "OCT", value: 1500 },
  { name: "NOV", value: 1000 },
  { name: "DEC", value: 1500 }
];
const COLORS = [
  "#0088FE",
  "#00C49F",
  "#FFBB28",
  "#FF8042",
  "#FF8042",
  "#FF8042",
  "#FF8042",
  "#FF8042",
  "#FF8042",
  "#FF8042",
  "#FF8042",
  "#FF8042"
];

const data01 = [
  { name: "network 1", value: 2 },
  { name: "network 3", value: 4 }
];

const Option = Select.Option;

const valueHistory = props => {
  function handleChange(value) {
    console.log(`selected ${value}`);
  }

  return (
    <Widget styleName="gx-card-full">
      <div className="ant-row-flex gx-px-4 gx-pt-4">
        <h2 className="h4 gx-mb-3">{props.title}</h2>
        <div className="gx-ml-auto">
          <Select
            className="gx-mb-2 gx-select-sm"
            defaultValue="10"
            onChange={handleChange}
          >
            <Option value="10">Last 10 days</Option>
            <Option value="20">Last 20 days</Option>
          </Select>
        </div>
      </div>

      <ResponsiveContainer width="100%" height={350}>
        <PieChart height={250}>
          <Pie
            data={data}
            cx="50%"
            cy="50%"
            outerRadius={100}
            fill="#8884d8"
            dataKey="value"
          >
            {data.map((entry, index) => (
              <Cell fill={COLORS[index % COLORS.length]} />
            ))}
          </Pie>
          {/* <Legend verticalAlign="top" height={36} />
          <Line name="pv of pages" dataKey="pv" stroke="#8884d8" />
          <Line name="uv of pages" dataKey="uv" stroke="#82ca9d" /> */}
          <Legend verticalAlign="top" height={125} />
          {data.map((entry, index) => (
            <Line
              name={data.name}
              dataKey={data.value}
              stroke={COLORS[index % COLORS.length]}
            />
          ))}
        </PieChart>
      </ResponsiveContainer>
    </Widget>
  );
};

export default valueHistory;
