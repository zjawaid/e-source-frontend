import React from "react";
import {Card, Checkbox, Col, Row, Button} from "antd";

function onChange(checkedValues) {
  console.log('checked = ', checkedValues);
}


const UseWithGrid = () => {
  return (
    // <Card className="gx-card" title="Use With Grid" style={{width:"100%"}}>
      <Checkbox.Group style={{width: '100%'}} onChange={onChange}>
        <Row>
          <Col span={8}><Checkbox className="gx-mb-3" value="A">Terminal Access
</Checkbox></Col>
          <Col span={8}><Checkbox className="gx-mb-3" value="B">Allow terminal distribution</Checkbox></Col>
          
          <Col span={8}><Button icon="setting">Configure</Button></Col>
          
          
        </Row>
      </Checkbox.Group>
    // </Card>
  );
};

export default UseWithGrid;
