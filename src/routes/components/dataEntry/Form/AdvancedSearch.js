import React, { Component } from "react";
import { Card, Col, Form, Icon, Input, Row, Checkbox, Select } from "antd";

const FormItem = Form.Item;

class AdvancedSearch extends Component {
  state = {
    expand: false
  };

  handleSearch = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      console.log("Received values of form: ", values, err);
    });
  };

  handleReset = () => {
    this.props.form.resetFields();
  };

  toggle = () => {
    const { expand } = this.state;
    this.setState({ expand: !expand });
  };

  // To generate mock Form.Item
  getFields(j, k) {
    const count = this.state.expand ? 10 : 6;
    const { getFieldDecorator } = this.props.form;
    const children = [];
    const fieldNames = [
      "Agency Name",
      "Agency Adress",
      "Agency Phone",
      "Contract with",
      "Contract Code",
      "Assigned Limit"
    ];
    for (let i = j; i < k; i++) {
      const divider = null;

      children.push(
        <Col
          lg={8}
          md={8}
          sm={12}
          xs={24}
          key={i}
          style={{ display: i < count ? "block" : "none" }}
        >
          <div className="gx-form-row0">
            <FormItem>
              <Input
                placeholder={fieldNames[`${i}`]}
                style={{ width: "300px" }}
              />
            </FormItem>
          </div>
        </Col>
      );
    }
    return children;
  }

  render() {
    return (
      <Card className="gx-card" title="Details">
        <Form className="ant-advanced-search-form" onSubmit={this.handleSearch}>
          <Row>
            {this.getFields(0, 3)}

            {this.getFields(3, 6)}

            <Col span={24} className="gx-text-right">
              <span>
                <Select
                  defaultValue="currency"
                  style={{ width: "10%", margin: "1%" }}
                >
                  <Select.Option value="USD">USD</Select.Option>
                  <Select.Option value="INR">INR</Select.Option>
                  <Select.Option value="PKR">PKR</Select.Option>
                </Select>
              </span>
              <Checkbox>Allow Distribution</Checkbox>
              <Checkbox>Allow Conversion</Checkbox>
            </Col>
          </Row>
        </Form>
      </Card>
    );
  }
}

const WrappedAdvancedSearch = Form.create()(AdvancedSearch);

export default WrappedAdvancedSearch;
