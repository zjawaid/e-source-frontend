import React from "react";
import { Col, Row, PageHeader, Button } from "antd";
import Auxiliary from "util/Auxiliary";
import AvailableCalendars from "components/Manage/Calendar/AvailableCalendars";

const Calendars = () => {
  return (
    <Auxiliary>
      <Row>
        <PageHeader title="Calendar" />,
        <Col span={24}>
          <AvailableCalendars />
        </Col>
      </Row>
    </Auxiliary>
  );
};

export default Calendars;
