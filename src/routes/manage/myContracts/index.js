import React from "react";
import { Tabs, Icon } from "antd";
import Auxiliary from "util/Auxiliary";
import ProfileHeader from "components/Manage/myContracts/details/Header";
import Details from "components/Manage/myContracts/details/index";
import TemporaryCrappings from "components/Manage/myContracts/temporaryCrappings/index";
import Receipt from "components/Manage/myContracts/Receipt/index";
import ReceiptReview from "components/Manage/myContracts/receiptReview/index";

const MyContracts = () => {
  const { TabPane } = Tabs;
  return (
    <Auxiliary>
      {
        <Tabs defaultActiveKey="1">
          <TabPane tab={<span className="gx-link">Details</span>} key="1">
            <Details />
          </TabPane>
          <TabPane
            tab={<span className="gx-link">Temporary Crapping</span>}
            key="2"
          >
            <TemporaryCrappings />
          </TabPane>
          <TabPane tab={<span className="gx-link">Receipt</span>} key="3">
            <Receipt />
          </TabPane>
          <TabPane
            tab={<span className="gx-link">Receipt Review</span>}
            key="4"
          >
            <ReceiptReview />
          </TabPane>
        </Tabs>
      }
    </Auxiliary>
  );
};

export default MyContracts;
