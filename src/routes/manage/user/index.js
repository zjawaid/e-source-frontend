import React from "react";
import { Col, Row, PageHeader, Button } from "antd";
import Auxiliary from "util/Auxiliary";
import User from "components/Manage/user/UserInfo";

const UserAcccounts = () => {
  return (
    <Auxiliary>
      <Row>
        <PageHeader title="User" />,
        <Col span={24}>
          <User />
        </Col>
      </Row>
    </Auxiliary>
  );
};

export default UserAcccounts;
