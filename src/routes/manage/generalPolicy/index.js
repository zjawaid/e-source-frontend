import React from "react";
import { Col, Row, PageHeader, Button } from "antd";
import Auxiliary from "util/Auxiliary";
import Summary from "components/Manage/generalPolicy/SettingsSummary";
import TicketModificationSetting from "components/Manage/generalPolicy/TicketModificationSettings";
import DiscountSetting from "components/Manage/generalPolicy/ChargeSettings";
// import AdvancedSearch from "components/Manage/contract/AdvancedSearch";
// import ContractType from "components/Manage/contract/ControlledCheckbox";
// import SelectAirport from "components/Manage/contract/SelectMultiple";
// import Ticketing from "components/Manage/contract/CheckboxGroups";

const GeneralPolicy = () => {
  return (
    <Auxiliary>
      <PageHeader title="Charge Rule Setting" />
      <Row>
        <Col span={24}>
          <Summary />
        </Col>

        <Col span={24}>
          <DiscountSetting />
        </Col>

        <Col span={24}>
          <TicketModificationSetting />
        </Col>
        {/* <Col span={24}>
    //       <AdvancedSearch />
    //     </Col>
    //     <Col span={24}>
    //       <ContractType />
    //     </Col>
    //     <Col span={24}>
    //       <SelectAirport />
    //     </Col>
    //     <Col span={24}>
    //       <Ticketing />
    //     </Col> */}

        {/* <Col xl={6} lg={12} md={12} sm={12} xs={24}>
    //       <Productivity/>
    //     </Col>
    //     <Col xl={6} lg={12} md={12} sm={12} xs={24}>
    //       <SocialMedia/>
    //     </Col>
    //     <Col xl={6} lg={12} md={12} sm={12} xs={24}>
    //       <RoadMap/>
    //     </Col>
    //     <Col xl={8} lg={12} md={12} sm={12} xs={24}>
    //       <Newseletter/>
    //     </Col>
    //     <Col xl={8} lg={12} md={12} sm={12} xs={24}>
    //       <NewPhotos/>
    //     </Col>
    //     <Col xl={8} lg={12} md={12} sm={12} xs={24}>
    //       <FlyingBird/>
    //     </Col>

    //     <Col xl={4} lg={12} md={12} sm={12} xs={24}>
    //       <DryFruit/>
    //     </Col>

    //     <Col xl={4} lg={8} md={8} sm={12} xs={24}>
    //       <AayurvedaCard/>
    //     </Col>

    //     <Col xl={4} lg={8} md={8} sm={12} xs={24}>
    //       <ToolTheDay/>
    //     </Col>

    //     <Col xl={4} lg={8} md={8} sm={12} xs={24}>
    //       <Row>
    //         <Col xl={12} lg={12} md={12} sm={12} xs={12}>
    //           <IconCard icon="noodles"/>
    //         </Col>
    //         <Col xl={12} lg={12} md={12} sm={12} xs={12}>
    //           <IconCard icon="donut"/>
    //         </Col>
    //       </Row>
    //       <SmartHomeCard/>
    //     </Col>

    //     <Col xl={4} lg={12} md={12} sm={12} xs={24}>
    //       <PhotosCard/>
    //       <UnreadMessagesCard/>
    //     </Col>

    //     <Col xl={4} lg={12} md={12} sm={24} xs={24}>
    //       <WorkStatusCard/>
    //       <Row>
    //         <Col xl={12} lg={12} md={12} sm={12} xs={12}>
    //           <IconCard color="gx-bg-orange gx-icon-white" icon="burger"/>
    //         </Col>
    //         <Col xl={12} lg={12} md={12} sm={12} xs={12}>
    //           <IconCard color="gx-bg-primary gx-icon-white" icon="pizza"/>
    //         </Col>
    //       </Row>
    //     </Col>

    //     <Col xl={6} lg={12} md={12} sm={12} xs={24}>
    //       <UserCard/>
    //     </Col>
    //     <Col xl={6} lg={12} md={12} sm={12} xs={24}>
    //       <IncreamentCard/>
    //     </Col>
    //     <Col xl={6} lg={12} md={12} sm={12} xs={24}>
    //       <CampaignCard/>
    //     </Col>
    //     <Col xl={6} lg={12} md={12} sm={12} xs={24}>
    //       <BuildingCard/>
    //     </Col>

    //     <Col xl={12} lg={24} md={24} sm={24} xs={24}>
    //       <GreenStepCard/>
    //     </Col>
    //     <Col xl={12} lg={24} md={24} sm={24} xs={24}>
    //       <FriendshipCard/>
    //     </Col> */}
      </Row>
    </Auxiliary>
  );
};

export default GeneralPolicy;
