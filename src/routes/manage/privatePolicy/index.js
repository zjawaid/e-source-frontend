import React from "react";
import { Col, Row, PageHeader, Button } from "antd";
import Auxiliary from "util/Auxiliary";
import AddPolicy from "components/Manage/generalPolicy/SettingsSummary";
import TicketModificationSetting from "components/Manage/generalPolicy/TicketModificationSettings";
import DiscountSetting from "components/Manage/generalPolicy/ChargeSettings";
// import AdvancedSearch from "components/Manage/contract/AdvancedSearch";
// import ContractType from "components/Manage/contract/ControlledCheckbox";
// import SelectAirport from "components/Manage/contract/SelectMultiple";
// import Ticketing from "components/Manage/contract/CheckboxGroups";

const PrivatePolicy = () => {
  return (
    <Auxiliary>
      <PageHeader title="Private Policy" />
      <Row>
        <Col span={24}>
          <AddPolicy />
        </Col>

        <Col span={24}>
          <DiscountSetting />
        </Col>

        <Col span={24}>
          <TicketModificationSetting />
        </Col>
      </Row>
    </Auxiliary>
  );
};

export default PrivatePolicy;
