import React from "react";
import { Col, Row, PageHeader, Button } from "antd";
import Auxiliary from "util/Auxiliary";
import Sectors from "components/Manage/Sector/Sectors";

const UserAcccounts = () => {
  return (
    <Auxiliary>
      <Row>
        <PageHeader title="Sectors" />,
        <Col span={24}>
          <Sectors />
        </Col>
      </Row>
    </Auxiliary>
  );
};

export default UserAcccounts;
