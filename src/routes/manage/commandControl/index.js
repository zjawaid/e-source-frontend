import React from "react";
import { Col, Row, PageHeader, Button } from "antd";
import Auxiliary from "util/Auxiliary";

import CommandList from "../../../components/Manage/CommandControl/CommandList";

const CommandControl = () => {
  return (
    <Auxiliary>
      <Row>
        <PageHeader title="Command Control" />,
        <Col span={24}>
          <CommandList />
        </Col>
      </Row>
    </Auxiliary>
  );
};

export default CommandControl;
