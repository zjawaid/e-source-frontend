import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import asyncComponent from "util/asyncComponent";

const Manage = ({ match }) => (
  <Switch>
    <Redirect exact from={`${match.url}/`} to={`${match.url}/contract`} />
    <Route
      path={`${match.url}/contract`}
      component={asyncComponent(() => import("./contract/index"))}
    />
    <Route
      path={`${match.url}/generalPolicy`}
      component={asyncComponent(() => import("./generalPolicy/index"))}
    />
    <Route
      path={`${match.url}/user`}
      component={asyncComponent(() => import("./user/index"))}
    />
    <Route
      path={`${match.url}/myContracts`}
      component={asyncComponent(() => import("./myContracts/index"))}
    />
    <Route
      path={`${match.url}/calendars`}
      component={asyncComponent(() => import("./calendar/index"))}
    />

    <Route
      path={`${match.url}/commandControl`}
      component={asyncComponent(() => import("./commandControl/index"))}
    />
    <Route
      path={`${match.url}/privatePolicy`}
      component={asyncComponent(() => import("./privatePolicy/index"))}
    />
    <Route
      path={`${match.url}/sectors`}
      component={asyncComponent(() => import("./Sector/index"))}
    />
  </Switch>
);

export default Manage;
