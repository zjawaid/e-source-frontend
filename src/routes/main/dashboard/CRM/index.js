import React from "react";
import { Col, Row } from "antd";
import { Area, AreaChart, ResponsiveContainer, Tooltip } from "recharts";
import {
  citiesData,
  propertiesData,
  queriesData,
  visitsData
} from "../../Metrics/data";
import TaskList from "components/dashboard/CRM/TaskList";
import SiteVisit from "components/dashboard/CRM/SiteVisit";
import RecentActivity from "components/dashboard/CRM/RecentActivity";
import TicketList from "components/dashboard/CRM/TicketList";
import TaskByStatus from "components/dashboard/CRM/TaskByStatus";
import WelComeCard from "components/dashboard/CRM/WelComeCard";
import Overview from "components/dashboard/CRM/Overview";
import SiteAudience from "components/dashboard/CRM/SiteAudience";
import Auxiliary from "util/Auxiliary";
import TotalRevenueCard from "components/dashboard/CRM/TotalRevenueCard";
import NewCustomers from "components/dashboard/CRM/NewCustomers";
import GrowthCard from "components/dashboard/CRM/GrowthCard";
import Widget from "components/Widget/index";
import CurrencyCalculator from "components/dashboard/Crypto/CurrencyCalculator";
import IconWithTextCard from "components/dashboard/CRM/IconWithTextCard";
import { recentActivity, taskList, trafficData } from "./data";
import ColoredChartCard from "components/dashboard/Listing/ChartCard";
import Sales from "components/dashboard/Crypto/BalanceHistory";

const CRM = () => {
  return (
    <Auxiliary>
      <Row>
        <Col xl={8} lg={24} md={8} sm={24} xs={24}>
          <ColoredChartCard
            chartProperties={{
              title: "TOTAL TICKET SALES",
              prize: "PKR 26,873",
              icon: "stats",
              bgColor: "cyan",
              styleName: "up",
              desc: "This week",
              percent: "03%"
            }}
            children={
              <ResponsiveContainer width="100%" height={75}>
                <AreaChart
                  data={propertiesData}
                  margin={{ top: 0, right: 0, left: 0, bottom: 0 }}
                >
                  <Tooltip />
                  <Area
                    dataKey="properties"
                    strokeWidth={0}
                    stackId="2"
                    stroke="#092453"
                    fill="#092453"
                    fillOpacity={1}
                  />
                </AreaChart>
              </ResponsiveContainer>
            }
          ></ColoredChartCard>
          {/* <TotalRevenueCard /> */}
          <Row>
            <Col xl={12} lg={12} md={6} sm={12} xs={12}>
              <IconWithTextCard
                cardColor="cyan"
                icon="diamond"
                title="00"
                subTitle="Today"
              />
            </Col>
            <Col xl={12} lg={12} md={6} sm={12} xs={12}>
              <IconWithTextCard
                cardColor="cyan"
                icon="tasks"
                title="09"
                subTitle="Fortnightly"
              />
            </Col>
          </Row>
        </Col>
        <Col xl={8} lg={12} md={8} sm={24} xs={24}>
          <ColoredChartCard
            chartProperties={{
              title: "TOTAL REFUNDS",
              prize: "PKR 15,873",
              icon: "stats",
              bgColor: "orange",
              styleName: "up",
              desc: "This week",
              percent: "03%"
            }}
            children={
              <ResponsiveContainer width="100%" height={75}>
                <AreaChart
                  data={citiesData}
                  margin={{ top: 0, right: 0, left: 0, bottom: 0 }}
                >
                  <Tooltip />
                  <Area
                    dataKey="cities"
                    strokeWidth={0}
                    stackId="2"
                    stroke="#092453"
                    fill="#092453"
                    fillOpacity={1}
                  />
                </AreaChart>
              </ResponsiveContainer>
            }
          ></ColoredChartCard>
          {/* <NewCustomers /> */}
          <Row>
            <Col xl={12} lg={12} md={6} sm={12} xs={12}>
              <IconWithTextCard
                cardColor="orange"
                icon="diamond"
                title="09"
                subTitle="Today"
              />
            </Col>
            <Col xl={12} lg={12} md={6} sm={12} xs={12}>
              <IconWithTextCard
                cardColor="orange"
                icon="tasks"
                title="687"
                subTitle="Fortnightly"
              />
            </Col>
          </Row>
        </Col>

        <Col xl={8} lg={12} md={8} sm={24} xs={24}>
          <ColoredChartCard
            chartProperties={{
              title: "TOTAL VOID TICKET AMOUNTS",
              prize: "PKR 16,873",
              icon: "stats",
              bgColor: "red",
              styleName: "up",
              desc: "This week",
              percent: "03%"
            }}
            children={
              <ResponsiveContainer width="100%" height={75}>
                <AreaChart
                  data={visitsData}
                  margin={{ top: 0, right: 0, left: 0, bottom: 0 }}
                >
                  <Tooltip />
                  <Area
                    dataKey="visit"
                    strokeWidth={0}
                    stackId="2"
                    stroke="#092453"
                    fill="#092453"
                    fillOpacity={1}
                  />
                </AreaChart>
              </ResponsiveContainer>
            }
          ></ColoredChartCard>
          {/* <GrowthCard trafficData={trafficData} /> */}
          <Row>
            <Col xl={12} lg={12} md={6} sm={12} xs={12}>
              <IconWithTextCard
                cardColor="red"
                icon="diamond"
                title="09"
                subTitle="Today"
              />
            </Col>
            <Col xl={12} lg={12} md={6} sm={12} xs={12}>
              <IconWithTextCard
                cardColor="red"
                icon="tasks"
                title="687"
                subTitle="Fortnightly"
              />
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <div className="gx-card">
            <div className="gx-card-body">
              <Row>
                <Col xl={6} lg={12} md={12} sm={12} xs={24}>
                  <WelComeCard />
                </Col>

                <Col
                  xl={6}
                  lg={20}
                  md={12}
                  sm={12}
                  xs={24}
                  className="gx-audi-col"
                >
                  <SiteAudience />
                </Col>

                <Col
                  xl={12}
                  lg={24}
                  md={24}
                  sm={24}
                  xs={24}
                  className="gx-visit-col"
                >
                  <SiteVisit />
                </Col>
              </Row>
            </div>
          </div>
        </Col>

        <Col span={24}>
          <Overview />
        </Col>
        <Col xl={12} lg={24} md={12} sm={24} xs={24}>
          <Sales title="Sales by Airlines"></Sales>
        </Col>
        <Col xl={12} lg={24} md={12} sm={24} xs={24}>
          <Sales title="Sales by Contracts"></Sales>
        </Col>

        {/* <Col xl={8} lg={24} md={24} sm={24} xs={24} className="gx-order-sm-2">
          <Widget>
            <RecentActivity recentList={recentActivity} shape="circle" />
          </Widget>
          <CurrencyCalculator />
        </Col> */}

        {/* <Col xl={16} lg={24} md={24} sm={24} xs={24} className="gx-order-sm-1">
          <Row>
            <Col xl={6} lg={6} md={6} sm={12} xs={12}>
              <IconWithTextCard
                cardColor="cyan"
                icon="diamond"
                title="09"
                subTitle="Projects"
              />
            </Col>
            <Col xl={6} lg={6} md={6} sm={12} xs={12}>
              <IconWithTextCard
                cardColor="orange"
                icon="tasks"
                title="687"
                subTitle="Tasks"
              />
            </Col>
            <Col xl={6} lg={6} md={6} sm={12} xs={12}>
              <IconWithTextCard
                cardColor="teal"
                icon="team"
                title="04"
                subTitle="Teams"
              />
            </Col>
            <Col xl={6} lg={6} md={6} sm={12} xs={12}>
              <IconWithTextCard
                cardColor="red"
                icon="files"
                title="09"
                subTitle="Files"
              />
            </Col>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <TaskList taskList={taskList} />
            </Col>
            <Col xl={16} lg={16} md={16} sm={24} xs={24}>
              <TicketList />
            </Col>
            <Col xl={8} lg={8} md={8} sm={24} xs={24}>
              <TaskByStatus />
            </Col>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <Overview />
            </Col>
          </Row>
        </Col> */}
      </Row>
    </Auxiliary>
  );
};

export default CRM;
