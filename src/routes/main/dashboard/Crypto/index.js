import React from "react";
import { Col, Row, Card } from "antd";

import {
  Area,
  AreaChart,
  Line,
  LineChart,
  ResponsiveContainer,
  Tooltip
} from "recharts";
import { increamentData, lineData } from "../../Metrics/data";
import ChartCard from "components/dashboard/Crypto/ChartCard";

import Auxiliary from "util/Auxiliary";
import Portfolio from "components/dashboard/Crypto/Portfolio";
import BalanceHistory from "components/dashboard/Crypto/BalanceHistory";
import SendMoney from "components/dashboard/Crypto/SendMoney";
import RewardCard from "components/dashboard/Crypto/RewardCard";
import CurrencyCalculator from "components/dashboard/Crypto/CurrencyCalculator";
import CryptoNews from "components/dashboard/Crypto/CryptoNews";
import DownloadMobileApps from "components/dashboard/Crypto/DownloadMobileApps";
import OrderHistory from "components/dashboard/Crypto/OrderHistory";

import ReadOnly from "../../../components/dataEntry/Rate/ReadOnly";

const gridStyle = {
  width: "50%",
  textAlign: "center",
  marginTop: "10px"
};
const Crypto = () => {
  return (
    <Auxiliary>
      <Row>
        <Col xl={8} lg={16} md={12} sm={12} xs={24}>
          <ChartCard
            prize="Tickets Issued per day"
            // title="23"
            // icon="bitcoin"
            children={
              <Card bordered={false}>
                {" "}
                <Card.Grid style={gridStyle}>
                  0<br />
                  Total Today
                </Card.Grid>
                <Card.Grid style={gridStyle}>
                  0<br />
                  Fortnightly
                </Card.Grid>
              </Card>
            }
            desc="Dec 12, 2019 9:47:18 AM"
          />
        </Col>
        <Col xl={8} lg={16} md={12} sm={12} xs={24}>
          <ChartCard
            prize="Refund Per Day"
            // title="23"
            // icon="bitcoin"
            children={
              <Card bordered={false}>
                {" "}
                <Card.Grid style={gridStyle}>
                  0<br />
                  Total Today
                </Card.Grid>
                <Card.Grid style={gridStyle}>
                  0<br />
                  Fortnightly
                </Card.Grid>
              </Card>
            }
            desc="Dec 12, 2019 9:47:18 AM"
          />
        </Col>
        <Col xl={8} lg={16} md={12} sm={12} xs={24}>
          <ChartCard
            prize="Void Tickets Per Day"
            // title="23"
            // icon="bitcoin"
            children={
              <Card bordered={false}>
                {" "}
                <Card.Grid style={gridStyle}>
                  0<br />
                  Total Today
                </Card.Grid>
                <Card.Grid style={gridStyle}>
                  0<br />
                  Fortnightly
                </Card.Grid>
              </Card>
            }
            desc="Dec 12, 2019 9:47:18 AM"
          />
        </Col>
        {/* <Col xl={6} lg={12} md={12} sm={12} xs={24}>
          <ChartCard prize="$849" title="47" icon="litcoin"
                     children={<ResponsiveContainer width="100%" height={75}>

                       <LineChart data={lineData}
                                  margin={{top: 5, right: 5, left: 5, bottom: 5}}>
                         <Tooltip/>
                         <Line dataKey="price" stroke="#038FDE" dot={{stroke: '#FEA931', strokeWidth: 2}}/>
                       </LineChart>
                     </ResponsiveContainer>}
                     styleName="down" desc="Litecoin Price"/>
        </Col> */}
        <Col xl={12} lg={24} md={12} sm={24} xs={24}>
          <BalanceHistory title="Sales by Airline" />
        </Col>
        <Col xl={12} lg={24} md={12} sm={24} xs={24}>
          <BalanceHistory title="Sales by Contract" />
        </Col>

        {/* <Col xl={9} lg={24} md={24} sm={24} xs={24}>
          <SendMoney />
        </Col>
        <Col xl={6} lg={12} md={12} sm={24} xs={24}>
          <RewardCard />
        </Col>
        <Col xl={9} lg={12} md={12} sm={24} xs={24}>
          <CurrencyCalculator />
        </Col>

        <Col xl={15} lg={24} md={24} sm={24} xs={24}>
          <CryptoNews />
        </Col>
        <Col xl={9} lg={24} md={24} sm={24} xs={24}>
          <DownloadMobileApps />
          <OrderHistory />
        </Col> */}
      </Row>
    </Auxiliary>
  );
};

export default Crypto;
