import React, { Component } from "react";
import MissedTicketReport from "../../../components/Reports/MissedTicketReport";
class MissedTicketReportPage extends Component {
  render() {
    return (
      <div>
        <MissedTicketReport />
      </div>
    );
  }
}

export default MissedTicketReportPage;
