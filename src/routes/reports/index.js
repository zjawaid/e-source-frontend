import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import asyncComponent from "util/asyncComponent";
import refundReportsPage from "./RefundReports/index";
import childRefundReportPage from "./ChildRefundReports/index";
import saleReportsPage from "./SaleReports/index";
import childSaleReportsPage from "./ChildSaleReport/index";
import voidReportsPage from "./VoidReports/index";
import ledgerReportsPage from "./LedgerReport/index";
import MissedTicketReportPage from "./MissedTicketReport/index";

const Reports = ({ match }) => (
  <Switch>
    <Route path={`${match.url}/refundReports`} component={refundReportsPage} />
    <Route
      path={`${match.url}/childRefundReports`}
      component={childRefundReportPage}
    />
    <Route path={`${match.url}/saleReports`} component={saleReportsPage} />
    <Route
      path={`${match.url}/childSaleReports`}
      component={childSaleReportsPage}
    />
    <Route path={`${match.url}/ledgerReports`} component={ledgerReportsPage} />
    <Route path={`${match.url}/voidReports`} component={voidReportsPage} />
    <Route
      path={`${match.url}/missedTicketReport`}
      component={MissedTicketReportPage}
    />

    {/* <Route path={`${match.url}/algolia`} component={asyncComponent(() => import('../algolia'))} /> */}
  </Switch>
);

export default Reports;
