import React from "react";
import { Col, Row } from "antd";
import { Area, AreaChart, ResponsiveContainer, Tooltip } from "recharts";
import { citiesData, propertiesData, visitsData } from "../main/Metrics/data";
import SiteVisit from "components/MainDashboard/SiteVisit";
import WelComeCard from "components/MainDashboard/WelComeCard";
import Overview from "components/MainDashboard/Overview";
import SiteAudience from "components/MainDashboard/SiteAudience";
import Auxiliary from "util/Auxiliary";
import IconWithTextCard from "components/MainDashboard/IconWithTextCard";
import ColoredChartCard from "components/MainDashboard/ChartCard";
import Sales from "components/MainDashboard/BalanceHistory";

const MainDashboard = () => {
  return (
    <Auxiliary>
      <Row>
        <Col xl={8} lg={24} md={8} sm={24} xs={24}>
          <ColoredChartCard
            chartProperties={{
              title: "TOTAL TICKET SALES",
              prize: "PKR 26,873",
              icon: "stats",
              bgColor: "cyan",
              styleName: "up",
              desc: "This week",
              percent: "03%"
            }}
            children={
              <ResponsiveContainer width="100%" height={75}>
                <AreaChart
                  data={propertiesData}
                  margin={{ top: 0, right: 0, left: 0, bottom: 0 }}
                >
                  <Tooltip />
                  <Area
                    dataKey="properties"
                    strokeWidth={0}
                    stackId="2"
                    stroke="#092453"
                    fill="#092453"
                    fillOpacity={1}
                  />
                </AreaChart>
              </ResponsiveContainer>
            }
          ></ColoredChartCard>
          {/* <TotalRevenueCard /> */}
          <Row>
            <Col xl={12} lg={12} md={6} sm={12} xs={12}>
              <IconWithTextCard
                cardColor="cyan"
                icon="diamond"
                title="00"
                subTitle="Today"
              />
            </Col>
            <Col xl={12} lg={12} md={6} sm={12} xs={12}>
              <IconWithTextCard
                cardColor="cyan"
                icon="tasks"
                title="09"
                subTitle="Fortnightly"
              />
            </Col>
          </Row>
        </Col>
        <Col xl={8} lg={12} md={8} sm={24} xs={24}>
          <ColoredChartCard
            chartProperties={{
              title: "TOTAL REFUNDS",
              prize: "PKR 15,873",
              icon: "stats",
              bgColor: "orange",
              styleName: "up",
              desc: "This week",
              percent: "03%"
            }}
            children={
              <ResponsiveContainer width="100%" height={75}>
                <AreaChart
                  data={citiesData}
                  margin={{ top: 0, right: 0, left: 0, bottom: 0 }}
                >
                  <Tooltip />
                  <Area
                    dataKey="cities"
                    strokeWidth={0}
                    stackId="2"
                    stroke="#092453"
                    fill="#092453"
                    fillOpacity={1}
                  />
                </AreaChart>
              </ResponsiveContainer>
            }
          ></ColoredChartCard>
          {/* <NewCustomers /> */}
          <Row>
            <Col xl={12} lg={12} md={6} sm={12} xs={12}>
              <IconWithTextCard
                cardColor="orange"
                icon="diamond"
                title="09"
                subTitle="Today"
              />
            </Col>
            <Col xl={12} lg={12} md={6} sm={12} xs={12}>
              <IconWithTextCard
                cardColor="orange"
                icon="tasks"
                title="687"
                subTitle="Fortnightly"
              />
            </Col>
          </Row>
        </Col>

        <Col xl={8} lg={12} md={8} sm={24} xs={24}>
          <ColoredChartCard
            chartProperties={{
              title: "TOTAL VOID TICKET AMOUNTS",
              prize: "PKR 16,873",
              icon: "stats",
              bgColor: "red",
              styleName: "up",
              desc: "This week",
              percent: "03%"
            }}
            children={
              <ResponsiveContainer width="100%" height={75}>
                <AreaChart
                  data={visitsData}
                  margin={{ top: 0, right: 0, left: 0, bottom: 0 }}
                >
                  <Tooltip />
                  <Area
                    dataKey="visit"
                    strokeWidth={0}
                    stackId="2"
                    stroke="#092453"
                    fill="#092453"
                    fillOpacity={1}
                  />
                </AreaChart>
              </ResponsiveContainer>
            }
          ></ColoredChartCard>
          {/* <GrowthCard trafficData={trafficData} /> */}
          <Row>
            <Col xl={12} lg={12} md={6} sm={12} xs={12}>
              <IconWithTextCard
                cardColor="red"
                icon="diamond"
                title="09"
                subTitle="Today"
              />
            </Col>
            <Col xl={12} lg={12} md={6} sm={12} xs={12}>
              <IconWithTextCard
                cardColor="red"
                icon="tasks"
                title="687"
                subTitle="Fortnightly"
              />
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <div className="gx-card">
            <div className="gx-card-body">
              <Row>
                <Col xl={6} lg={12} md={12} sm={12} xs={24}>
                  <WelComeCard />
                </Col>

                <Col
                  xl={6}
                  lg={20}
                  md={12}
                  sm={12}
                  xs={24}
                  className="gx-audi-col"
                >
                  <SiteAudience />
                </Col>

                <Col
                  xl={12}
                  lg={24}
                  md={24}
                  sm={24}
                  xs={24}
                  className="gx-visit-col"
                >
                  <SiteVisit />
                </Col>
              </Row>
            </div>
          </div>
        </Col>

        <Col span={24}>
          <Overview />
        </Col>
        <Col xl={12} lg={24} md={12} sm={24} xs={24}>
          <Sales title="Sales by Airlines"></Sales>
        </Col>
        <Col xl={12} lg={24} md={12} sm={24} xs={24}>
          <Sales title="Sales by Contracts"></Sales>
        </Col>
      </Row>
    </Auxiliary>
  );
};

export default MainDashboard;
