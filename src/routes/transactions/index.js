import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import asyncComponent from "util/asyncComponent";
import bookFlightPage from "./BookFlight/index";
import IssueTicketPage from "./IssueTicket/index";
import ManualReservationPage from "./ManualReservation/index";
import VoudTicketPage from "./VoidTicket/index";
import ticketManagementPage from "./TicketManagement/index";
import myPnrPage from "./MyPnr/index";
import exchangeTicketPage from "./ExchangeTicket/index";
import revalidateTicketPage from "./RevalidateTicket/index";
import lccBookFlightPage from "./LccBookFlight/LccBookFlight";
import invoicePursuingPage from "./InvoicePursuing/invoicPursuing";

const Transactions = ({ match }) => (
  <Switch>
    <Route path={`${match.url}/bookFlights`} component={bookFlightPage} />
    <Route path={`${match.url}/issueTicket`} component={IssueTicketPage} />
    <Route
      path={`${match.url}/manualReservation`}
      component={ManualReservationPage}
    />
    <Route path={`${match.url}/voidTicket`} component={VoudTicketPage} />
    <Route
      path={`${match.url}/ticketManagement`}
      component={ticketManagementPage}
    />
    <Route path={`${match.url}/myPnrs`} component={myPnrPage} />
    <Route
      path={`${match.url}/exchangeTicket`}
      component={exchangeTicketPage}
    />
    <Route
      path={`${match.url}/revalidateTicket`}
      component={revalidateTicketPage}
    />
    <Route path={`${match.url}/lccBookFlight`} component={lccBookFlightPage} />
    <Route
      path={`${match.url}/invoicePursuing`}
      component={invoicePursuingPage}
    />
    {/* <Route path={`${match.url}/algolia`} component={asyncComponent(() => import('../algolia'))} /> */}
  </Switch>
);

export default Transactions;
