import React, { Component } from "react";
import MyPnrComponent from "components/Transactions/MyPnr";

class myPnrPage extends Component {
  render() {
    return (
      <div>
        <MyPnrComponent />
      </div>
    );
  }
}

export default myPnrPage;
