import React, { Component } from "react";
import ManualReservationComponent from "../../../components/Transactions/ManualReservation";
class ManualReservationPage extends Component {
  render() {
    return (
      <div>
        <ManualReservationComponent />
      </div>
    );
  }
}

export default ManualReservationPage;
