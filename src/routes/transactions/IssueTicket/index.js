import React, { Component } from "react";
import IssueTicketComponent from "../../../components/Transactions/IssueTicket";
class IssueTicketPage extends Component {
  render() {
    return (
      <div>
        <IssueTicketComponent />
      </div>
    );
  }
}

export default IssueTicketPage;
